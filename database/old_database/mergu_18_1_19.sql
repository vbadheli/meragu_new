-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2019 at 06:39 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mergu`
--

-- --------------------------------------------------------

--
-- Table structure for table `advances`
--

CREATE TABLE `advances` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advances`
--

INSERT INTO `advances` (`id`, `employee_id`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 100, 0, '2018-12-11 08:40:50', '2018-12-11 08:40:50'),
(2, 3, 100, 0, '2018-12-11 08:40:56', '2018-12-11 08:40:56'),
(3, 4, 100, 0, '2018-12-22 09:37:04', '2018-12-20 03:56:29'),
(4, 5, 0, 0, '2018-12-22 09:37:02', '2018-12-11 08:41:23'),
(5, 9, 500, 0, '2018-12-22 09:36:55', '2018-12-20 03:54:12'),
(6, 9, 500, 0, '2018-12-22 09:36:59', '2018-12-20 03:54:40'),
(8, 9, 5000, 0, '2018-12-20 04:02:44', '2018-12-20 04:02:44');

-- --------------------------------------------------------

--
-- Table structure for table `cuttingdetails`
--

CREATE TABLE `cuttingdetails` (
  `id` int(11) NOT NULL,
  `cutting_id` int(11) DEFAULT NULL,
  `stockdetail_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `avg` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttingdetails`
--

INSERT INTO `cuttingdetails` (`id`, `cutting_id`, `stockdetail_id`, `size_id`, `count`, `avg`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, 10, 1.24, '2019-01-03 13:34:46', '2019-01-03 13:34:46'),
(2, 1, 3, 2, 10, 1.24, '2019-01-03 13:34:46', '2019-01-03 13:34:46'),
(3, 1, 4, 2, 10, 1.24, '2019-01-03 13:34:46', '2019-01-03 13:34:46'),
(4, 1, 2, 3, 5, 1.29, '2019-01-03 13:34:46', '2019-01-03 13:34:46'),
(5, 1, 3, 3, 5, 1.29, '2019-01-03 13:34:46', '2019-01-03 13:34:46'),
(6, 1, 4, 3, 5, 1.29, '2019-01-03 13:34:46', '2019-01-03 13:34:46'),
(7, 2, 22, 2, 10, 1.24, '2019-01-03 13:39:41', '2019-01-03 13:39:41'),
(8, 2, 23, 2, 50, 1.24, '2019-01-03 13:39:41', '2019-01-03 13:39:41'),
(9, 2, 22, 3, 20, 1.29, '2019-01-03 13:39:41', '2019-01-03 13:39:41'),
(10, 2, 23, 3, 10, 1.29, '2019-01-03 13:39:41', '2019-01-03 13:39:41'),
(11, 3, 20, 2, 10, 1.24, '2019-01-03 13:43:47', '2019-01-03 13:43:47'),
(12, 3, 21, 2, 20, 1.24, '2019-01-03 13:43:47', '2019-01-03 13:43:47'),
(13, 3, 20, 3, 50, 1.29, '2019-01-03 13:43:47', '2019-01-03 13:43:47'),
(14, 3, 21, 3, 30, 1.29, '2019-01-03 13:43:47', '2019-01-03 13:43:47'),
(15, 4, 32, 2, 10, 1.24, '2019-01-03 13:46:00', '2019-01-03 13:46:00'),
(16, 4, 33, 2, 50, 1.24, '2019-01-03 13:46:00', '2019-01-03 13:46:00'),
(17, 4, 32, 3, 20, 1.29, '2019-01-03 13:46:00', '2019-01-03 13:46:00'),
(18, 4, 33, 3, 60, 1.29, '2019-01-03 13:46:00', '2019-01-03 13:46:00'),
(19, 5, 5, 2, 10, 1.24, '2019-01-03 13:53:35', '2019-01-03 13:53:35'),
(20, 5, 6, 2, 10, 1.24, '2019-01-03 13:53:35', '2019-01-03 13:53:35'),
(21, 5, 5, 3, 2, 1.29, '2019-01-03 13:53:35', '2019-01-03 13:53:35'),
(22, 5, 6, 3, 2, 1.29, '2019-01-03 13:53:35', '2019-01-03 13:53:35'),
(23, 6, 38, 2, 5, 1.24, '2019-01-03 13:54:38', '2019-01-03 13:54:38'),
(24, 6, 39, 2, 5, 1.24, '2019-01-03 13:54:38', '2019-01-03 13:54:38'),
(25, 6, 38, 3, 5, 1.29, '2019-01-03 13:54:38', '2019-01-03 13:54:38'),
(26, 6, 39, 3, 6, 1.29, '2019-01-03 13:54:38', '2019-01-03 13:54:38'),
(27, 7, 30, 2, 10, 1.24, '2019-01-03 14:03:17', '2019-01-03 14:03:17'),
(28, 7, 31, 2, 60, 1.24, '2019-01-03 14:03:17', '2019-01-03 14:03:17'),
(29, 7, 30, 3, 20, 1.29, '2019-01-03 14:03:17', '2019-01-03 14:03:17'),
(30, 7, 31, 3, 30, 1.29, '2019-01-03 14:03:17', '2019-01-03 14:03:17'),
(31, 8, 14, 2, 10, 1.24, '2019-01-03 14:09:23', '2019-01-03 14:09:23'),
(32, 8, 15, 2, 10, 1.24, '2019-01-03 14:09:23', '2019-01-03 14:09:23'),
(33, 8, 14, 3, 20, 1.29, '2019-01-03 14:09:23', '2019-01-03 14:09:23'),
(34, 8, 15, 3, 20, 1.29, '2019-01-03 14:09:23', '2019-01-03 14:09:23'),
(35, 9, 16, 2, 10, 1.24, '2019-01-03 14:10:00', '2019-01-03 14:10:00'),
(36, 9, 17, 2, 5, 1.24, '2019-01-03 14:10:00', '2019-01-03 14:10:00'),
(37, 9, 16, 3, 20, 1.29, '2019-01-03 14:10:00', '2019-01-03 14:10:00'),
(38, 9, 17, 3, 6, 1.29, '2019-01-03 14:10:00', '2019-01-03 14:10:00'),
(39, 11, 1, 2, 10, 1.24, '2019-01-04 12:38:06', '2019-01-04 12:38:06'),
(40, 11, 1, 3, 5, 1.29, '2019-01-04 12:38:06', '2019-01-04 12:38:06');

-- --------------------------------------------------------

--
-- Table structure for table `cuttings`
--

CREATE TABLE `cuttings` (
  `id` int(11) NOT NULL,
  `party_id` int(11) DEFAULT NULL,
  `stock_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttings`
--

INSERT INTO `cuttings` (`id`, `party_id`, `stock_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2019-01-03 08:04:46', '2019-01-03 08:04:46'),
(2, 1, 10, '2019-01-03 08:09:41', '2019-01-03 08:09:41'),
(3, 5, 9, '2019-01-03 08:13:47', '2019-01-03 08:13:47'),
(4, 6, 15, '2019-01-03 08:16:00', '2019-01-03 08:16:00'),
(5, 7, 3, '2019-01-03 08:23:35', '2019-01-03 08:23:35'),
(7, 7, 14, '2019-01-03 08:33:17', '2019-01-03 08:33:17'),
(8, 13, 6, '2019-01-03 08:39:23', '2019-01-03 08:39:23'),
(9, 13, 7, '2019-01-03 08:40:00', '2019-01-03 08:40:00'),
(11, 1, 1, '2019-01-04 07:08:06', '2019-01-04 07:08:06');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` varchar(100) NOT NULL,
  `category` enum('stiching','finishing','packaging','buttons') DEFAULT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `firstname`, `lastname`, `email`, `phone`, `address`, `category`, `active_status`, `created_at`, `updated_at`) VALUES
(1, 'testing', 'testing', 'test@gmail.com', '9999999999', 'Solapur', NULL, 1, '2018-11-21 10:34:23', '2018-11-21 05:04:23'),
(3, 'kaka', 'kaka', 'kaka@gmail.com', '7474747474', 'solapur', NULL, 1, '2018-11-21 05:06:32', '2018-11-21 05:06:32'),
(4, 'Pravin', 'Lagshetti', 'pravin@gmail.com', '8475858589', 'solapur', NULL, 1, '2018-11-22 05:28:33', '2018-11-22 05:28:33'),
(5, 'Amrut', 'Halnor', 'amrut@gmail.com', '8485895750', 'solapur', NULL, 1, '2018-12-08 02:44:55', '2018-12-08 02:44:55'),
(6, 'amrut', 'halnor', 'test@gmail.com', '9999999999', 'Solapur', NULL, 1, '2018-12-12 01:32:52', '2018-12-12 01:32:52'),
(7, 'vishal', 'adheli', 'vbadheli@gmail.com', '9999999999', 'Solapur', NULL, 1, '2018-12-12 01:34:11', '2018-12-12 01:34:11'),
(8, 'laxmi', 'test', 'testlaxmi@gmail.com', '9999999999', 'Solapur', NULL, 1, '2018-12-12 01:34:46', '2018-12-12 01:34:46'),
(9, 'Laxmikant', 'Urgonda', 'urgonda143@gmail.com', '8149686090', 'New Pachha Peth,Vijay Nagar,Solapur', NULL, 1, '2018-12-20 08:01:21', '2018-12-20 02:31:21'),
(10, 'Pandurang', 'Guja', NULL, '8478585858', 'solapur', 'finishing', 1, '2018-12-29 08:37:23', '2018-12-29 08:37:23'),
(11, 'jaganatha', 'Jakkan', 'ga@gmail.com', '9876543210', 'solapur', 'stiching', 1, '2019-01-05 11:50:53', '2019-01-05 06:20:53'),
(12, 'kiran', 'jampale', 'kiran@gmail.com', '8484848484', 'solapur', 'stiching', 1, '2018-12-31 01:07:47', '2018-12-31 01:07:47');

-- --------------------------------------------------------

--
-- Table structure for table `employee_payments`
--

CREATE TABLE `employee_payments` (
  `id` int(11) NOT NULL,
  `paymentsheet_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `total_pics` varchar(50) NOT NULL,
  `cal_amount` double NOT NULL,
  `esi` float NOT NULL,
  `pf` float NOT NULL,
  `totalamount` int(11) NOT NULL,
  `payable_amount` int(11) NOT NULL,
  `advance_cut` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `invoice_number` varchar(15) NOT NULL,
  `igst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `advance_payment` float DEFAULT NULL,
  `shipping` float DEFAULT NULL,
  `total` float NOT NULL,
  `remarks` text,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `party_id`, `invoice_number`, `igst`, `sgst`, `cgst`, `advance_payment`, `shipping`, `total`, `remarks`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, 1, '1002', 2, 2.5, 2.2, 0, 500, 5000, NULL, 0, '2018-12-19 06:12:33', '2018-12-19 11:42:33'),
(2, 5, '1003', NULL, NULL, NULL, NULL, NULL, 200, NULL, 0, '2018-12-21 06:40:36', '2018-12-21 12:10:36'),
(3, 5, '1424', NULL, NULL, NULL, NULL, NULL, 1500, NULL, 0, '2018-12-21 06:46:20', '2018-12-21 12:16:20'),
(4, 7, '550158', NULL, NULL, NULL, NULL, NULL, 5000, NULL, 0, '2018-12-21 06:51:23', '2018-12-21 12:21:23'),
(5, 5, '1100', NULL, NULL, NULL, NULL, NULL, 20000, NULL, 0, '2019-01-04 08:05:14', '2019-01-04 13:35:14');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_item_details`
--

CREATE TABLE `invoice_item_details` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `cuttingdetails_id` int(11) NOT NULL,
  `stockdetail_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `rate` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_item_details`
--

INSERT INTO `invoice_item_details` (`id`, `invoice_id`, `cuttingdetails_id`, `stockdetail_id`, `quantity`, `rate`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 15, 150, '2018-12-19 11:42:33', '2018-12-19 11:42:33'),
(2, 1, 4, 2, 15, 150, '2018-12-19 11:42:33', '2018-12-19 11:42:33'),
(3, 2, 1, 2, 10, 20, '2018-12-21 12:10:36', '2018-12-21 12:10:36'),
(4, 3, 1, 2, 10, 50, '2018-12-21 12:16:21', '2018-12-21 12:16:21'),
(5, 3, 7, 2, 20, 50, '2018-12-21 12:16:21', '2018-12-21 12:16:21'),
(6, 4, 4, 2, 10, 500, '2018-12-21 12:21:23', '2018-12-21 12:21:23'),
(7, 5, 40, 1, 100, 200, '2019-01-04 13:35:14', '2019-01-04 13:35:14');

-- --------------------------------------------------------

--
-- Table structure for table `parties`
--

CREATE TABLE `parties` (
  `id` int(11) NOT NULL,
  `firm_name` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `gst` varchar(15) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parties`
--

INSERT INTO `parties` (`id`, `firm_name`, `firstname`, `lastname`, `email`, `phone`, `address`, `gst`, `created_at`, `updated_at`, `active_status`) VALUES
(1, 'Nitesh Garment', 'Nitesh', 'Garment', 'nitesh@gmail.com', '8600914255', 'Solapur', 'G274515425', '2018-11-21 10:06:29', '2018-11-21 04:36:29', 1),
(5, 'Speed Garment', 'Amit', 'Bakale', 'speed@gmail.com', '9876543210', 'Solapur', 'G2715425266', '2018-11-21 04:27:40', '2018-11-21 04:27:40', 1),
(6, 'Mg Garments', 'Narendra', 'Mergu', 'meragu@gmail.com', '123456790', 'solapur', 'G27145635H4', '2018-12-20 07:54:12', '2018-12-20 02:24:12', 1),
(7, 'Indian Garment', 'test', 'Indian', 'indian@gmail.com', '8748585959', 'Solapur', 'G274584858H5', '2018-12-20 02:08:05', '2018-12-20 02:08:05', 1),
(13, 'Amrut Garment', 'Amrut', 'Halnor', 'amrut@gmail.com', '9876543210', 'Solapur', 'G12345678901234', '2018-12-29 07:43:38', '2018-12-29 07:43:38', 1),
(18, 'hina', 'Hina', 'Hina', 'hina@gmail.com', '9876541230', 'solapur', 'G27123456789012', '2018-12-29 08:18:29', '2018-12-29 08:18:29', 1),
(19, 'kiran Garment', 'Kiran', 'Balap', 'kiran@gmail.com', '8475859595', 'solapur', 'G27121234567890', '2019-01-04 06:38:15', '2019-01-04 06:38:15', 1),
(20, 'Gadagi Garment', 'Ganesh', 'Gadagi', 'ganeshgadagi@gmail.com', '1234567890', 'Solapur', 'G27015151526234', '2019-01-05 05:02:51', '2019-01-05 05:02:51', 1),
(21, 'Mahesh Garment', 'Mahesh', 'Mahindrakar', 'mahesh@gmail.com', '9876543210', 'solapur', 'G27141234567890', '2019-01-05 05:58:56', '2019-01-05 05:58:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `party_payments`
--

CREATE TABLE `party_payments` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `remarks` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `party_payments`
--

INSERT INTO `party_payments` (`id`, `invoice_id`, `amount`, `remarks`, `created_at`, `updated_at`) VALUES
(1, 1, 100, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '2018-12-19 12:48:05', '2018-12-19 12:48:05'),
(2, 1, 150, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '2018-12-19 12:50:50', '2018-12-19 12:50:50'),
(3, 1, 150, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '2018-12-19 12:51:09', '2018-12-19 12:51:09'),
(4, 1, 150, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '2018-12-19 12:51:30', '2018-12-19 12:51:30'),
(5, 1, 1000, 'this is a test', '2018-12-19 13:34:24', '2018-12-19 13:34:24'),
(6, 1, 4000, 'Advance Amount', '2018-12-21 12:04:50', '2018-12-21 12:04:50'),
(7, 2, 100, 'advance', '2018-12-21 12:10:59', '2018-12-21 12:10:59'),
(8, 2, 100, 'advance', '2018-12-21 12:12:19', '2018-12-21 12:12:19'),
(9, 2, 100, 'advance', '2018-12-21 12:14:51', '2018-12-21 12:14:51'),
(10, 3, 500, 'Remaing', '2018-12-21 12:17:10', '2018-12-21 12:17:10'),
(11, 3, 500, 'Remaing', '2018-12-21 12:17:25', '2018-12-21 12:17:25'),
(12, 3, 100, 'remaing', '2018-12-21 12:18:07', '2018-12-21 12:18:07'),
(13, 3, 100, 'remaing', '2018-12-21 12:19:13', '2018-12-21 12:19:13'),
(14, 4, 1000, 'remiangin', '2018-12-21 12:21:51', '2018-12-21 12:21:51');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `size` varchar(50) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size`, `active_status`, `created_at`, `updated_at`) VALUES
(2, '4', 1, '2018-11-23 00:42:15', '2018-11-23 00:42:15'),
(3, '6', 1, '2018-11-23 00:42:20', '2018-11-23 00:42:20'),
(4, '8', 1, '2018-11-23 00:42:24', '2018-11-23 00:42:24'),
(5, '10', 1, '2018-11-23 00:42:27', '2018-11-23 00:42:27'),
(6, '12', 1, '2018-11-24 06:52:34', '2018-11-24 06:52:34'),
(7, '14', 1, '2018-12-20 02:31:51', '2018-12-20 02:31:51'),
(8, '42', 1, '2019-01-05 04:46:22', '2019-01-05 04:46:22'),
(9, '40', 1, '2019-01-05 04:47:21', '2019-01-05 04:47:21'),
(10, '38', 1, '2019-01-05 04:58:36', '2019-01-05 04:58:36'),
(11, '36', 1, '2019-01-05 04:58:50', '2019-01-05 04:58:50');

-- --------------------------------------------------------

--
-- Table structure for table `stitching_assignments`
--

CREATE TABLE `stitching_assignments` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `cuttingdetail_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `duedate` datetime NOT NULL,
  `rate` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stitching_assignments`
--

INSERT INTO `stitching_assignments` (`id`, `employee_id`, `cuttingdetail_id`, `count`, `duedate`, `rate`, `created_at`, `updated_at`) VALUES
(1, 9, 16, 10, '2019-01-03 00:00:00', 15, '2019-01-03 09:32:10', '2019-01-03 04:02:10'),
(2, 9, 17, 20, '2019-01-03 00:00:00', 15, '2019-01-03 09:32:10', '2019-01-03 04:02:10'),
(3, 9, 18, 30, '2019-01-03 00:00:00', 15, '2019-01-03 09:32:10', '2019-01-03 04:02:10'),
(4, 4, 1, 10, '2019-01-03 00:00:00', 10, '2019-01-03 09:08:21', '2019-01-03 03:38:21'),
(5, 4, 4, 10, '2018-12-21 00:00:00', 10, '2018-12-21 04:34:49', '2018-12-21 04:34:49');

-- --------------------------------------------------------

--
-- Table structure for table `stitching_returns`
--

CREATE TABLE `stitching_returns` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `stitching_id` int(11) NOT NULL,
  `received_count` int(11) NOT NULL,
  `received_date` date NOT NULL,
  `paid_status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stitching_returns`
--

INSERT INTO `stitching_returns` (`id`, `employee_id`, `stitching_id`, `received_count`, `received_date`, `paid_status`, `created_at`, `updated_at`) VALUES
(1, 9, 1, 10, '2018-12-20', 0, '2019-01-01 07:39:14', '2018-12-28 07:45:46'),
(2, 9, 2, 20, '2018-12-20', 0, '2019-01-01 07:39:18', '2018-12-28 07:45:46'),
(3, 9, 3, 20, '2018-12-20', 0, '2019-01-04 13:17:27', '2018-12-28 07:45:46'),
(4, 4, 4, 10, '2018-12-21', 0, '2019-01-12 12:06:02', '2019-01-12 06:35:53'),
(5, 4, 5, 10, '2019-01-04', 0, '2019-01-12 12:06:04', '2019-01-12 06:35:53'),
(9, 9, 3, 10, '2019-01-04', 0, '2019-01-04 07:47:41', '2019-01-04 07:47:41');

-- --------------------------------------------------------

--
-- Table structure for table `stockdetails`
--

CREATE TABLE `stockdetails` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `length` double DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stockdetails`
--

INSERT INTO `stockdetails` (`id`, `stock_id`, `length`, `color`, `photo`, `created_at`, `updated_at`) VALUES
(1, 1, 200, 'OTTO', 'codeCenter-3.png', '2019-01-04 12:06:03', '2019-01-04 06:36:03'),
(2, 2, 100, 'redchecks', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(3, 2, 110, 'blue bird', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(4, 2, 120, 'grey hair', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(5, 3, 100, 'Magenta', 'FICA-DOCUMENTS (1).jpg', '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(6, 3, 2000, 'Krime', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(7, 3, 1000, 'Leman white', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(8, 4, 100, 'G', 'DigitalGlobe_WorldView2_50cm_8bit_Pansharpened_RGB_DRA_Rome_Italy_2009DEC10_8bits_sub_r_1.jpg', '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(9, 4, 2000, 'B', 'Infoterra_Terrasar-X_1_75_m_Radar_2007DEC15_Toronto_EEC-RE_8bits_sub_r_12.jpg', '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(10, 4, 900, 'C', 'GeoEye_GeoEye1_50cm_8bit_RGB_DRA_Mining_2009FEB14_8bits_sub_r_15.jpg', '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(11, 5, 100, 'N', 'Infoterra_Terrasar-X_1_75_m_Radar_2007DEC15_Toronto_EEC-RE_8bits_sub_r_12.jpg', '2019-01-04 08:42:13', '2019-01-04 03:12:13'),
(12, 5, 100, 'M', 'DigitalGlobe_WorldView2_50cm_8bit_Pansharpened_RGB_DRA_Rome_Italy_2009DEC10_8bits_sub_r_1.jpg', '2019-01-04 08:42:55', '2019-01-04 03:12:55'),
(13, 5, 500, 'L', 'GeoEye_GeoEye1_50cm_8bit_RGB_DRA_Mining_2009FEB14_8bits_sub_r_15.jpg', '2019-01-04 08:42:55', '2019-01-04 03:12:55'),
(14, 6, 100, 'mita', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(15, 6, 200, 'green', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(16, 7, 50, 'mita', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(17, 7, 100, 'jara', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(18, 8, 100, 'checks', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(19, 8, 100, 'kakal', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(20, 9, 120, 'jaja', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(21, 9, 100, 'hira', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(22, 10, 100, 'Pink', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(23, 10, 200, 'Green', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(24, 11, 100, 'leman', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(25, 11, 200, 'tannna', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(26, 12, 100, 'pink', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(27, 12, 200, 'on', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(28, 13, 100, 'Bi', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(29, 13, 200, 'Green Lemen', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(30, 14, 100, '200', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(31, 14, 500, 'golden', NULL, '2019-01-04 08:42:08', '0000-00-00 00:00:00'),
(32, 15, 100, 'silver', 'barcode (1).png', '2019-01-05 06:44:12', '2019-01-05 01:14:12'),
(33, 15, 200, 'Platinum', 'barcode.png', '2019-01-05 06:44:12', '2019-01-05 01:14:12'),
(34, 16, 100, 'L', NULL, '2019-01-04 11:25:53', '2019-01-03 18:30:00'),
(35, 16, 200, 'CH', NULL, '2019-01-04 11:24:42', '2019-01-04 11:12:42'),
(36, 17, 100, 'Lo', NULL, '2019-01-04 11:24:21', '2019-01-04 10:08:29'),
(37, 17, 400, 'Kill', NULL, '2019-01-04 11:24:26', '2019-01-04 11:11:41'),
(38, 18, 100, 'No', NULL, '2019-01-04 11:24:30', '2019-01-04 12:10:36'),
(39, 18, 200, 'Lo', NULL, '2019-01-04 11:24:35', '2019-01-04 13:16:42'),
(40, 27, 100, 'green', NULL, '2019-01-04 11:26:43', NULL),
(41, 1, 100, 'OPPO', 'FICA-DOCUMENTS.jpg', '2019-01-05 05:58:06', '2019-01-05 00:28:06'),
(42, 1, 200, 'TOOT', 'codeCenter-3.png', '2019-01-05 05:56:05', NULL),
(43, 1, 500, 'KOOL', 'packman-v2.gif', '2019-01-05 05:56:05', NULL),
(44, 1, 200, 'TOOT', 'codeCenter-3.png', '2019-01-05 05:56:05', NULL),
(45, 1, 500, 'KOOL', 'packman-v2.gif', '2019-01-05 05:56:05', NULL),
(46, 19, 100, 'gij', NULL, '2019-01-12 10:51:11', NULL),
(47, 20, 100, 'asdfasdf', NULL, '2019-01-14 13:06:04', NULL),
(48, 21, 100, 'sdafasdfasdffa', NULL, '2019-01-14 13:07:10', NULL),
(49, 22, 100, 'asdfsafdfafsdfsfa', NULL, '2019-01-14 13:13:27', NULL),
(50, 23, 100, 'Jijj', NULL, '2019-01-14 13:14:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `dno` text NOT NULL,
  `challan_no` varchar(50) DEFAULT NULL,
  `cloth_name` varchar(100) DEFAULT NULL,
  `panna` varchar(50) DEFAULT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `party_id`, `dno`, `challan_no`, `cloth_name`, `panna`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, 'rr1000', NULL, 'reymonds', '', '2018-12-19', '2018-12-19 06:03:50', '2018-12-19 06:03:50'),
(2, 1, 'rr1200', NULL, 'test', '', '2018-12-19', '2018-12-19 06:04:37', '2018-12-19 06:04:37'),
(3, 7, '282726', '123456', 'OTTO', '', '2018-12-20', '2018-12-20 02:34:09', '2018-12-20 02:34:09'),
(4, 6, '282423', '21425a', 'OTTO', '', '2018-12-21', '2018-12-21 04:55:06', '2018-12-21 04:55:06'),
(5, 1, '141414', '1414', 'Indian', '', '2018-12-21', '2018-12-21 06:08:25', '2018-12-21 06:08:25'),
(6, 13, 'G1212', 'G123654', 'amrut', '60', '2018-12-29', '2018-12-29 08:28:20', '2018-12-29 08:28:20'),
(7, 13, '154266', '124142', 'Kala', '72', '2018-12-29', '2018-12-29 08:31:57', '2018-12-29 08:31:57'),
(8, 18, 'K14252', 'G123456', 'Lucky', '60', '2018-12-29', '2018-12-29 08:34:19', '2018-12-29 08:34:19'),
(9, 5, 'G75848', 'G12345', 'jara', '72', '2018-12-29', '2018-12-29 08:46:00', '2018-12-29 08:46:00'),
(10, 1, 'S151G', 'SC152', 'Nothing', '60', '2019-01-03', '2019-01-03 04:58:08', '2019-01-03 04:58:08'),
(11, 18, 'K2625', 'K3636', NULL, '60', '2019-01-03', '2019-01-03 05:21:37', '2019-01-03 05:21:37'),
(12, 18, 'L1414', 'LL1212', 'NO', '60', '2019-01-03', '2019-01-03 05:22:40', '2019-01-03 05:22:40'),
(13, 13, 'K12K12', 'B151B', 'Kila', '72', '2019-01-03', '2019-01-03 05:51:15', '2019-01-03 05:51:15'),
(14, 7, '252525', '1245K', 'Maftalal', '60', '2019-01-03', '2019-01-03 05:52:19', '2019-01-03 05:52:19'),
(15, 6, 'J15J15', 'CH123', 'N', '60', '2019-01-03', '2019-01-03 06:07:21', '2019-01-03 06:07:21'),
(16, 6, 'Kin154', 'nil142', 'b', '60', '2019-01-03', '2019-01-03 06:07:59', '2019-01-03 06:07:59'),
(17, 6, '154GD', 'B1542', 'Ko', '60', '2019-01-03', '2019-01-03 06:08:46', '2019-01-03 06:08:46'),
(18, 7, 'K4747', 'Jir142', 'Ka', '60', '2019-01-03', '2019-01-03 06:09:37', '2019-01-03 06:09:37'),
(19, 20, 'gga12', 'ga40', 'gaga', '60', '2019-01-12', '2019-01-12 05:21:11', '2019-01-12 05:21:11'),
(20, 1, 'asdfsfsaf', 'asdfasfd', 'asdfasfdasf', '66', '2019-01-14', '2019-01-14 07:36:04', '2019-01-14 07:36:04'),
(21, 1, 'asdafsdsadf', 'asdfasdfasdf', 'asdfsdafdsaf', 'asdfsafasdf', '2019-01-14', '2019-01-14 07:37:10', '2019-01-14 07:37:10'),
(22, 1, 'atatata', 'adsafs', 'adsafdsaf', '60', '2019-01-14', '2019-01-14 07:43:27', '2019-01-14 07:43:27'),
(23, 20, 'K50', 'Ch1234a', 'Molla', '60', '2019-01-14', '2019-01-14 07:44:22', '2019-01-14 07:44:22');

-- --------------------------------------------------------

--
-- Table structure for table `stock_size_avgs`
--

CREATE TABLE `stock_size_avgs` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `avg` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_size_avgs`
--

INSERT INTO `stock_size_avgs` (`id`, `stock_id`, `size_id`, `avg`, `created_at`, `updated_at`) VALUES
(1, 2, 2, '1.24', '2019-01-03 13:34:46', '2019-01-03 13:34:46'),
(2, 2, 3, '1.29', '2019-01-03 13:34:46', '2019-01-03 13:34:46'),
(3, 10, 2, '1.24', '2019-01-03 13:39:42', '2019-01-03 13:39:42'),
(4, 10, 3, '1.29', '2019-01-03 13:39:42', '2019-01-03 13:39:42'),
(5, 15, 2, '1.24', '2019-01-03 13:46:00', '2019-01-03 13:46:00'),
(6, 15, 3, '1.29', '2019-01-03 13:46:00', '2019-01-03 13:46:00'),
(7, 3, 2, '1.24', '2019-01-03 13:53:35', '2019-01-03 13:53:35'),
(8, 3, 3, '1.29', '2019-01-03 13:53:35', '2019-01-03 13:53:35'),
(9, 18, 2, '1.24', '2019-01-03 13:54:38', '2019-01-03 13:54:38'),
(10, 18, 3, '1.29', '2019-01-03 13:54:38', '2019-01-03 13:54:38'),
(11, 14, 2, '1.24', '2019-01-03 14:03:17', '2019-01-03 14:03:17'),
(12, 14, 3, '1.29', '2019-01-03 14:03:17', '2019-01-03 14:03:17'),
(13, 6, 2, '1.24', '2019-01-03 14:09:24', '2019-01-03 14:09:24'),
(14, 6, 3, '1.29', '2019-01-03 14:09:24', '2019-01-03 14:09:24'),
(15, 7, 2, '1.24', '2019-01-03 14:10:00', '2019-01-03 14:10:00'),
(16, 7, 3, '1.29', '2019-01-03 14:10:01', '2019-01-03 14:10:01'),
(17, 1, 2, '1.24', '2019-01-04 12:38:06', '2019-01-04 12:38:06'),
(18, 1, 3, '1.29', '2019-01-04 12:38:06', '2019-01-04 12:38:06');

-- --------------------------------------------------------

--
-- Table structure for table `stock_uses`
--

CREATE TABLE `stock_uses` (
  `id` int(11) NOT NULL,
  `stockdetail_id` int(11) NOT NULL,
  `used_mtrs` double NOT NULL,
  `total_pcs` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_uses`
--

INSERT INTO `stock_uses` (`id`, `stockdetail_id`, `used_mtrs`, `total_pcs`, `created_at`, `updated_at`) VALUES
(1, 2, 19.05, 15, '2019-01-03 08:04:46', '2019-01-03 08:04:46'),
(2, 3, 19.05, 15, '2019-01-03 08:04:46', '2019-01-03 08:04:46'),
(3, 4, 19.05, 15, '2019-01-03 08:04:46', '2019-01-03 08:04:46'),
(4, 22, 38.1, 30, '2019-01-03 08:09:41', '2019-01-03 08:09:41'),
(5, 23, 76.2, 60, '2019-01-03 08:09:41', '2019-01-03 08:09:41'),
(6, 20, 76.2, 60, '2019-01-03 08:13:47', '2019-01-03 08:13:47'),
(7, 21, 63.5, 50, '2019-01-03 08:13:47', '2019-01-03 08:13:47'),
(8, 32, 38.1, 30, '2019-01-03 08:16:00', '2019-01-03 08:16:00'),
(9, 33, 139.7, 110, '2019-01-03 08:16:00', '2019-01-03 08:16:00'),
(10, 5, 15.24, 12, '2019-01-03 08:23:35', '2019-01-03 08:23:35'),
(11, 6, 15.24, 12, '2019-01-03 08:23:35', '2019-01-03 08:23:35'),
(12, 38, 12.7, 10, '2019-01-03 08:24:38', '2019-01-03 08:24:38'),
(13, 39, 13.97, 11, '2019-01-03 08:24:38', '2019-01-03 08:24:38'),
(14, 30, 38.1, 30, '2019-01-03 08:33:17', '2019-01-03 08:33:17'),
(15, 31, 114.3, 90, '2019-01-03 08:33:17', '2019-01-03 08:33:17'),
(16, 14, 38.1, 30, '2019-01-03 08:39:23', '2019-01-03 08:39:23'),
(17, 15, 38.1, 30, '2019-01-03 08:39:23', '2019-01-03 08:39:23'),
(18, 16, 38.1, 30, '2019-01-03 08:40:00', '2019-01-03 08:40:00'),
(19, 17, 13.97, 11, '2019-01-03 08:40:00', '2019-01-03 08:40:00'),
(20, 1, 19.05, 15, '2019-01-04 07:08:06', '2019-01-04 07:08:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User table to store all user details , permission';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `mobile_number`, `last_login`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'administrator', '9999999999', '2019-01-19 00:04:13', 1, '2PuHNPU5xoyz23XR2QYWmKI97Xf2d8TqtXooVmEfyZjsu8rYltd4SjXhCTSl', '2018-11-22 18:30:00', '2019-01-19 00:04:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advances`
--
ALTER TABLE `advances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuttings`
--
ALTER TABLE `cuttings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_payments`
--
ALTER TABLE `employee_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `party_id` (`party_id`);

--
-- Indexes for table `invoice_item_details`
--
ALTER TABLE `invoice_item_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_id` (`invoice_id`),
  ADD KEY `cuttingdetails_id` (`cuttingdetails_id`);

--
-- Indexes for table `parties`
--
ALTER TABLE `parties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `party_payments`
--
ALTER TABLE `party_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stitching_assignments`
--
ALTER TABLE `stitching_assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stitching_returns`
--
ALTER TABLE `stitching_returns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockdetails`
--
ALTER TABLE `stockdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_size_avgs`
--
ALTER TABLE `stock_size_avgs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_id` (`stock_id`),
  ADD KEY `size_id` (`size_id`);

--
-- Indexes for table `stock_uses`
--
ALTER TABLE `stock_uses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advances`
--
ALTER TABLE `advances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `cuttings`
--
ALTER TABLE `cuttings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `employee_payments`
--
ALTER TABLE `employee_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `invoice_item_details`
--
ALTER TABLE `invoice_item_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `parties`
--
ALTER TABLE `parties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `party_payments`
--
ALTER TABLE `party_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `stitching_assignments`
--
ALTER TABLE `stitching_assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stitching_returns`
--
ALTER TABLE `stitching_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `stockdetails`
--
ALTER TABLE `stockdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `stock_size_avgs`
--
ALTER TABLE `stock_size_avgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `stock_uses`
--
ALTER TABLE `stock_uses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
