-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 18, 2019 at 10:37 PM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mglara`
--

-- --------------------------------------------------------

--
-- Table structure for table `advances`
--

CREATE TABLE `advances` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cuttingdetails`
--

CREATE TABLE `cuttingdetails` (
  `id` int(11) NOT NULL,
  `cutting_id` int(11) DEFAULT NULL,
  `stockdetail_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `avg` double DEFAULT NULL,
  `fashion_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttingdetails`
--

INSERT INTO `cuttingdetails` (`id`, `cutting_id`, `stockdetail_id`, `size_id`, `count`, `avg`, `fashion_id`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 4, 80, 0.55, 1, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(2, 1, 4, 5, 350, 0.63, 1, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(3, 1, 4, 6, 630, 0.7, 1, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(4, 1, 4, 7, 700, 0.73, 1, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(5, 1, 4, 8, 550, 0.78, 1, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(6, 1, 4, 9, 350, 0.85, 1, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(7, 1, 4, 10, 160, 0.9, 1, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(8, 1, 4, 11, 80, 0.95, 1, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(9, 1, 4, 12, 9, 1, 1, '2019-01-18 13:20:12', '2019-01-18 13:20:12');

-- --------------------------------------------------------

--
-- Table structure for table `cuttings`
--

CREATE TABLE `cuttings` (
  `id` int(11) NOT NULL,
  `party_id` int(11) DEFAULT NULL,
  `stock_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttings`
--

INSERT INTO `cuttings` (`id`, `party_id`, `stock_id`, `created_at`, `updated_at`) VALUES
(1, 4, 7, '2019-01-18 20:20:12', '2019-01-18 20:20:12');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` varchar(100) NOT NULL,
  `category` enum('stiching','finishing','packaging','buttons') DEFAULT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `firstname`, `lastname`, `email`, `phone`, `address`, `category`, `active_status`, `created_at`, `updated_at`) VALUES
(1, 'prvin', 'kamurti', 'acdds@gmail.com', '9422664455', 'Solapur', 'stiching', 1, '2019-01-03 17:33:25', '2019-01-03 17:33:25'),
(2, 'niranjan', 'madgundi', 'abc@gmail.com', '9422645420', 'Solapur', 'stiching', 1, '2019-01-03 17:39:35', '2019-01-03 17:39:35'),
(3, 'DINESH', 'BODA', 'abc@gmail.com', '8421862671', 'Solapur', 'stiching', 1, '2019-01-03 17:40:40', '2019-01-03 17:40:40'),
(4, 'SANTOSH', 'GAJUL', 'abc@gmail.com', '8421862671', 'Solapur', 'stiching', 1, '2019-01-03 17:41:46', '2019-01-03 17:41:46'),
(5, 'SANJAY', 'BODA', 'abc@gmail.com', '8421862671', 'Solapur', 'stiching', 1, '2019-01-03 17:42:18', '2019-01-03 17:42:18'),
(6, 'ANJALI', 'ARKAL', 'abc@gmail.com', '8421862671', 'solapur', 'stiching', 1, '2019-01-03 17:43:15', '2019-01-03 17:43:15'),
(7, 'RAVI', 'SAI', 'abc@gmail.com', '8421862671', 'Solapur', 'finishing', 1, '2019-01-03 17:43:55', '2019-01-03 17:43:55'),
(8, 'SANTOSH', 'SHERLA', 'abc@gmail.com', '8421862671', 'Solapur', 'finishing', 1, '2019-01-03 17:44:29', '2019-01-03 17:44:29'),
(9, 'Mohan', 'chawan', NULL, '1234567890', 'solapur', 'stiching', 1, '2019-01-18 18:47:38', '2019-01-18 18:47:38'),
(10, 'Ramesh', 'Mitta', NULL, '9373937078', 'Solapur', 'finishing', 1, '2019-01-18 18:48:25', '2019-01-18 18:48:25'),
(11, 'Rohan', 'Boddul', NULL, '1234567890', 'Solapur', 'stiching', 1, '2019-01-18 18:49:01', '2019-01-18 18:49:01'),
(12, 'Santosh', 'Sherla', NULL, '1234567890', 'Solapur', 'finishing', 1, '2019-01-18 18:49:27', '2019-01-18 18:49:27'),
(13, 'prabhakar', 'Putta', NULL, '1234567890', 'Solapur', 'stiching', 1, '2019-01-18 18:54:38', '2019-01-18 18:54:38'),
(14, 'balaji', 'ganpa', NULL, '1234567890', 'Solapur', 'stiching', 1, '2019-01-18 18:55:12', '2019-01-18 18:55:12'),
(15, 'Shriniwas', 'Sherla', NULL, '1234567890', 'Solapur', 'stiching', 1, '2019-01-18 18:55:41', '2019-01-18 18:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `employee_payments`
--

CREATE TABLE `employee_payments` (
  `id` int(11) NOT NULL,
  `paymentsheet_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `total_pics` varchar(50) NOT NULL,
  `cal_amount` double NOT NULL,
  `esi` float NOT NULL,
  `pf` float NOT NULL,
  `totalamount` int(11) NOT NULL,
  `payable_amount` int(11) NOT NULL,
  `advance_cut` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_payments`
--

INSERT INTO `employee_payments` (`id`, `paymentsheet_id`, `employee_id`, `total_pics`, `cal_amount`, `esi`, `pf`, `totalamount`, `payable_amount`, `advance_cut`, `status`, `created_at`, `updated_at`) VALUES
(1, 829435, 3, '50 Pices', 700, 40, 0, 660, 700, -40, 1, '2019-01-18 21:05:30', '2019-01-18 21:05:30');

-- --------------------------------------------------------

--
-- Table structure for table `fashion`
--

CREATE TABLE `fashion` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fashion`
--

INSERT INTO `fashion` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Shirt-GH', NULL, NULL),
(2, 'Shirt-GF', NULL, NULL),
(3, 'Shirt-BH', NULL, NULL),
(4, 'Shirt-BF', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `invoice_number` varchar(15) NOT NULL,
  `igst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `advance_payment` float DEFAULT NULL,
  `shipping` float DEFAULT NULL,
  `total` float NOT NULL,
  `remarks` text,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_item_details`
--

CREATE TABLE `invoice_item_details` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `cuttingdetails_id` int(11) NOT NULL,
  `stockdetail_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `rate` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `parties`
--

CREATE TABLE `parties` (
  `id` int(11) NOT NULL,
  `firm_name` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` varchar(100) NOT NULL,
  `gst` varchar(15) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parties`
--

INSERT INTO `parties` (`id`, `firm_name`, `firstname`, `lastname`, `email`, `phone`, `address`, `gst`, `created_at`, `updated_at`, `active_status`) VALUES
(4, 'Malani Garments', 'Shailesh', 'Malani', NULL, '9422645420', 'Solapur', NULL, '2019-01-18 18:56:53', '2019-01-18 18:56:53', 1),
(5, 'Skylon Shirts', 'Vijaya', 'kolhapure', NULL, '1234567890', 'Solapur', NULL, '2019-01-18 18:57:34', '2019-01-18 18:57:34', 1),
(6, 'RR Bankapure', 'Rohan', 'Bankapure', NULL, '1234567890', 'Solapur', NULL, '2019-01-18 18:58:05', '2019-01-18 18:58:05', 1),
(7, 'united corp', 'gurjyot', 'narang', NULL, '1234567890', 'Ahmednagar', NULL, '2019-01-18 18:58:52', '2019-01-18 18:58:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `party_payments`
--

CREATE TABLE `party_payments` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `remarks` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `size` varchar(50) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size`, `active_status`, `created_at`, `updated_at`) VALUES
(1, '16', 1, '2019-01-03 17:26:11', '2019-01-03 17:26:11'),
(2, '18', 1, '2019-01-03 17:26:20', '2019-01-03 17:26:20'),
(3, '20', 1, '2019-01-03 17:26:24', '2019-01-03 17:26:24'),
(4, '22', 1, '2019-01-03 17:26:31', '2019-01-03 17:26:31'),
(5, '24', 1, '2019-01-03 10:26:44', '2019-01-03 17:26:44'),
(6, '26', 1, '2019-01-03 17:26:53', '2019-01-03 17:26:53'),
(7, '28', 1, '2019-01-03 17:27:01', '2019-01-03 17:27:01'),
(8, '30', 1, '2019-01-03 17:27:08', '2019-01-03 17:27:08'),
(9, '32', 1, '2019-01-03 17:27:12', '2019-01-03 17:27:12'),
(10, '34', 1, '2019-01-03 17:27:16', '2019-01-03 17:27:16'),
(11, '36', 1, '2019-01-03 17:27:19', '2019-01-03 17:27:19'),
(12, '38', 1, '2019-01-03 17:27:22', '2019-01-03 17:27:22'),
(13, '40', 1, '2019-01-03 17:27:26', '2019-01-03 17:27:26'),
(14, '42', 1, '2019-01-03 17:27:29', '2019-01-03 17:27:29'),
(15, '44', 1, '2019-01-03 17:27:33', '2019-01-03 17:27:33'),
(16, '46', 1, '2019-01-03 17:27:37', '2019-01-03 17:27:37'),
(17, '48', 1, '2019-01-03 17:27:44', '2019-01-03 17:27:44');

-- --------------------------------------------------------

--
-- Table structure for table `stitching_assignments`
--

CREATE TABLE `stitching_assignments` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `cuttingdetail_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `duedate` datetime NOT NULL,
  `rate` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stitching_assignments`
--

INSERT INTO `stitching_assignments` (`id`, `employee_id`, `cuttingdetail_id`, `count`, `duedate`, `rate`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 80, '2019-01-18 00:00:00', 18, '2019-01-18 20:22:28', '2019-01-18 20:22:28'),
(2, 3, 2, 50, '2019-01-18 00:00:00', 14, '2019-01-18 20:22:51', '2019-01-18 20:22:51'),
(3, 4, 2, 50, '2019-01-18 00:00:00', 14, '2019-01-18 20:23:21', '2019-01-18 20:23:21'),
(4, 4, 2, 50, '2019-01-18 00:00:00', 14, '2019-01-18 20:24:32', '2019-01-18 20:24:32'),
(5, 15, 2, 50, '2019-01-18 00:00:00', 14, '2019-01-18 20:25:26', '2019-01-18 20:25:26'),
(6, 14, 2, 50, '2019-01-18 00:00:00', 14, '2019-01-18 20:26:55', '2019-01-18 20:26:55'),
(7, 13, 2, 49, '2019-01-18 00:00:00', 14, '2019-01-18 20:27:16', '2019-01-18 20:27:16'),
(8, 12, 2, 51, '2019-01-18 00:00:00', 14, '2019-01-18 20:27:39', '2019-01-18 20:27:39'),
(9, 11, 3, 130, '2019-01-18 00:00:00', 18, '2019-01-18 20:28:04', '2019-01-18 20:28:04'),
(10, 1, 3, 100, '2019-01-18 00:00:00', 18, '2019-01-18 20:29:07', '2019-01-18 20:29:07'),
(11, 1, 4, 100, '2019-01-18 00:00:00', 18, '2019-01-18 20:29:07', '2019-01-18 20:29:07'),
(12, 1, 5, 50, '2019-01-18 00:00:00', 18, '2019-01-18 20:29:07', '2019-01-18 20:29:07'),
(13, 1, 6, 50, '2019-01-18 00:00:00', 18, '2019-01-18 20:29:07', '2019-01-18 20:29:07'),
(14, 1, 7, 160, '2019-01-18 00:00:00', 18, '2019-01-18 20:29:07', '2019-01-18 20:29:07'),
(15, 2, 3, 400, '2019-01-18 00:00:00', 15, '2019-01-18 20:29:48', '2019-01-18 20:29:48'),
(16, 2, 4, 50, '2019-01-18 00:00:00', 15, '2019-01-18 20:29:48', '2019-01-18 20:29:48'),
(17, 2, 5, 80, '2019-01-18 00:00:00', 15, '2019-01-18 20:29:48', '2019-01-18 20:29:48'),
(18, 2, 8, 80, '2019-01-18 00:00:00', 15, '2019-01-18 20:29:48', '2019-01-18 20:29:48'),
(19, 2, 9, 9, '2019-01-18 00:00:00', 15, '2019-01-18 20:29:48', '2019-01-18 20:29:48'),
(20, 6, 4, 50, '2019-01-18 00:00:00', 14, '2019-01-18 20:30:32', '2019-01-18 20:30:32'),
(21, 6, 5, 20, '2019-01-18 00:00:00', 14, '2019-01-18 20:30:32', '2019-01-18 20:30:32');

-- --------------------------------------------------------

--
-- Table structure for table `stitching_returns`
--

CREATE TABLE `stitching_returns` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `stitching_id` int(11) NOT NULL,
  `received_count` int(11) NOT NULL,
  `received_date` date NOT NULL,
  `paid_status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stitching_returns`
--

INSERT INTO `stitching_returns` (`id`, `employee_id`, `stitching_id`, `received_count`, `received_date`, `paid_status`, `created_at`, `updated_at`) VALUES
(1, 3, 2, 50, '2019-01-18', 1, '2019-01-18 14:05:30', '2019-01-18 21:05:30'),
(2, 1, 10, 100, '2019-01-18', 0, '2019-01-18 21:08:08', '2019-01-18 21:08:08'),
(3, 1, 11, 50, '2019-01-18', 0, '2019-01-18 21:08:08', '2019-01-18 21:08:08'),
(4, 1, 12, 40, '2019-01-18', 0, '2019-01-18 21:08:08', '2019-01-18 21:08:08'),
(5, 1, 13, 40, '2019-01-18', 0, '2019-01-18 21:08:08', '2019-01-18 21:08:08'),
(6, 1, 14, 100, '2019-01-18', 0, '2019-01-18 21:08:08', '2019-01-18 21:08:08');

-- --------------------------------------------------------

--
-- Table structure for table `stockdetails`
--

CREATE TABLE `stockdetails` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `length` double DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stockdetails`
--

INSERT INTO `stockdetails` (`id`, `stock_id`, `length`, `color`, `photo`, `created_at`, `updated_at`) VALUES
(4, 7, 2909, 'White-GH', NULL, NULL, '2019-01-18 13:35:16'),
(5, 7, 3000, 'White-BH', NULL, NULL, NULL),
(6, 7, 4000, 'White-GF', NULL, NULL, NULL),
(7, 7, 4000, 'White-BF', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `dno` text NOT NULL,
  `challan_no` varchar(50) DEFAULT NULL,
  `cloth_name` varchar(100) DEFAULT NULL,
  `panna` varchar(10) DEFAULT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `party_id`, `dno`, `challan_no`, `cloth_name`, `panna`, `date`, `created_at`, `updated_at`) VALUES
(7, 4, 'Mafatlal-White', '1', 'Mafatlal', '58', '2018-12-22', '2019-01-18 12:17:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stock_fashion`
--

CREATE TABLE `stock_fashion` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `fashion_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_fashion`
--

INSERT INTO `stock_fashion` (`id`, `stock_id`, `fashion_id`, `created_at`, `updated_at`) VALUES
(1, 7, 1, NULL, NULL),
(2, 7, 2, NULL, NULL),
(3, 7, 3, NULL, NULL),
(4, 7, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_size_avgs`
--

CREATE TABLE `stock_size_avgs` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `avg` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_size_avgs`
--

INSERT INTO `stock_size_avgs` (`id`, `stock_id`, `size_id`, `avg`, `created_at`, `updated_at`) VALUES
(1, 7, 4, 0.55, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(2, 7, 5, 0.63, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(3, 7, 6, 0.7, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(4, 7, 7, 0.73, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(5, 7, 8, 0.78, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(6, 7, 9, 0.85, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(7, 7, 10, 0.9, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(8, 7, 11, 0.95, '2019-01-18 13:20:12', '2019-01-18 13:20:12'),
(9, 7, 12, 1, '2019-01-18 13:20:12', '2019-01-18 13:20:12');

-- --------------------------------------------------------

--
-- Table structure for table `stock_uses`
--

CREATE TABLE `stock_uses` (
  `id` int(11) NOT NULL,
  `stockdetail_id` int(11) NOT NULL,
  `used_mtrs` double NOT NULL,
  `total_pcs` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_uses`
--

INSERT INTO `stock_uses` (`id`, `stockdetail_id`, `used_mtrs`, `total_pcs`, `created_at`, `updated_at`) VALUES
(1, 4, 2909, 2909, '2019-01-18 20:20:12', '2019-01-18 20:20:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User table to store all user details , permission';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `mobile_number`, `last_login`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mgadmin', 'admin', 'administrator', '9999999999', '2019-01-19 12:23:52', 1, 'KVLL1dOjtbBBFohTN4MRughwxjMcfioczTa3JKpu1vvI3f1Pq6JLqH932Lbr', '2018-11-22 18:30:00', '2019-01-19 12:23:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advances`
--
ALTER TABLE `advances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fashion_id` (`fashion_id`);

--
-- Indexes for table `cuttings`
--
ALTER TABLE `cuttings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_payments`
--
ALTER TABLE `employee_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fashion`
--
ALTER TABLE `fashion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `party_id` (`party_id`);

--
-- Indexes for table `invoice_item_details`
--
ALTER TABLE `invoice_item_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_id` (`invoice_id`),
  ADD KEY `cuttingdetails_id` (`cuttingdetails_id`);

--
-- Indexes for table `parties`
--
ALTER TABLE `parties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `party_payments`
--
ALTER TABLE `party_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stitching_assignments`
--
ALTER TABLE `stitching_assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stitching_returns`
--
ALTER TABLE `stitching_returns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockdetails`
--
ALTER TABLE `stockdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_fashion`
--
ALTER TABLE `stock_fashion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_id` (`stock_id`),
  ADD KEY `fashion_id` (`fashion_id`);

--
-- Indexes for table `stock_size_avgs`
--
ALTER TABLE `stock_size_avgs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_id` (`stock_id`),
  ADD KEY `size_id` (`size_id`);

--
-- Indexes for table `stock_uses`
--
ALTER TABLE `stock_uses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advances`
--
ALTER TABLE `advances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cuttings`
--
ALTER TABLE `cuttings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `employee_payments`
--
ALTER TABLE `employee_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fashion`
--
ALTER TABLE `fashion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_item_details`
--
ALTER TABLE `invoice_item_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `parties`
--
ALTER TABLE `parties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `party_payments`
--
ALTER TABLE `party_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `stitching_assignments`
--
ALTER TABLE `stitching_assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `stitching_returns`
--
ALTER TABLE `stitching_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stockdetails`
--
ALTER TABLE `stockdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stock_fashion`
--
ALTER TABLE `stock_fashion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stock_size_avgs`
--
ALTER TABLE `stock_size_avgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `stock_uses`
--
ALTER TABLE `stock_uses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
