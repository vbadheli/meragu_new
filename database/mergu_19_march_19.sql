-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2019 at 01:36 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mergu`
--

-- --------------------------------------------------------

--
-- Table structure for table `advances`
--

CREATE TABLE `advances` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advances`
--

INSERT INTO `advances` (`id`, `employee_id`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 200, 0, '2019-03-14 06:52:36', '2019-03-14 06:52:36'),
(2, 1, 500, 0, '2019-03-18 03:39:43', '2019-03-18 03:39:43');

-- --------------------------------------------------------

--
-- Table structure for table `cuttingdetails`
--

CREATE TABLE `cuttingdetails` (
  `id` int(11) NOT NULL,
  `cutting_id` int(11) DEFAULT NULL,
  `stockdetail_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `avg` double DEFAULT NULL,
  `fashion_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttingdetails`
--

INSERT INTO `cuttingdetails` (`id`, `cutting_id`, `stockdetail_id`, `size_id`, `count`, `avg`, `fashion_id`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 1, 10, 1.24, NULL, '2019-03-14 10:10:05', '2019-03-14 10:10:05'),
(2, 1, 5, 2, 10, 1.29, NULL, '2019-03-14 10:10:05', '2019-03-14 10:10:05'),
(3, 1, 5, 3, 10, 1.36, NULL, '2019-03-14 10:10:05', '2019-03-14 10:10:05'),
(4, 1, 5, 4, 10, 1.42, NULL, '2019-03-14 10:10:05', '2019-03-14 10:10:05'),
(5, 2, 1, 1, 10, 1.24, NULL, '2019-03-14 12:05:18', '2019-03-14 12:05:18'),
(6, 2, 2, 1, 10, 1.24, NULL, '2019-03-14 12:05:18', '2019-03-14 12:05:18'),
(7, 2, 1, 2, 20, 1.29, NULL, '2019-03-14 12:05:18', '2019-03-14 12:05:18'),
(8, 2, 2, 2, 20, 1.29, NULL, '2019-03-14 12:05:18', '2019-03-14 12:05:18');

-- --------------------------------------------------------

--
-- Table structure for table `cuttings`
--

CREATE TABLE `cuttings` (
  `id` int(11) NOT NULL,
  `party_id` int(11) DEFAULT NULL,
  `stock_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttings`
--

INSERT INTO `cuttings` (`id`, `party_id`, `stock_id`, `created_at`, `updated_at`) VALUES
(1, 4, 11, '2019-03-14 04:40:05', '2019-03-14 04:40:05'),
(2, 5, 3, '2019-03-14 06:35:18', '2019-03-14 06:35:18');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` varchar(100) NOT NULL,
  `category` enum('stiching','finishing','packaging','buttons') DEFAULT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `firstname`, `lastname`, `email`, `phone`, `address`, `category`, `active_status`, `created_at`, `updated_at`) VALUES
(1, 'prvin', 'kamurti', 'acdds@gmail.com', '9422664455', 'Solapur', 'stiching', 1, '2019-01-03 17:33:25', '2019-01-03 17:33:25'),
(2, 'niranjan', 'madgundi', 'abc@gmail.com', '9422645420', 'Solapur', 'stiching', 1, '2019-01-03 17:39:35', '2019-01-03 17:39:35'),
(3, 'DINESH', 'BODA', 'abc@gmail.com', '8421862671', 'Solapur', 'stiching', 1, '2019-01-03 17:40:40', '2019-01-03 17:40:40'),
(4, 'SANTOSH', 'GAJUL', 'abc@gmail.com', '8421862671', 'Solapur', 'stiching', 1, '2019-01-03 17:41:46', '2019-01-03 17:41:46'),
(5, 'SANJAY', 'BODA', 'abc@gmail.com', '8421862671', 'Solapur', 'stiching', 1, '2019-01-03 17:42:18', '2019-01-03 17:42:18'),
(6, 'ANJALI', 'ARKAL', 'abc@gmail.com', '8421862671', 'solapur', 'stiching', 1, '2019-01-03 17:43:15', '2019-01-03 17:43:15'),
(7, 'RAVI', 'SAI', 'abc@gmail.com', '8421862671', 'Solapur', 'finishing', 1, '2019-01-03 17:43:55', '2019-01-03 17:43:55'),
(8, 'SANTOSH', 'SHERLA', 'abc@gmail.com', '8421862671', 'Solapur', 'finishing', 1, '2019-01-03 17:44:29', '2019-01-03 17:44:29'),
(9, 'Mohan', 'chawan', NULL, '1234567890', 'solapur', 'stiching', 1, '2019-01-18 18:47:38', '2019-01-18 18:47:38'),
(10, 'Ramesh', 'Mitta', NULL, '9373937078', 'Solapur', 'finishing', 1, '2019-01-18 18:48:25', '2019-01-18 18:48:25'),
(11, 'Rohan', 'Boddul', NULL, '1234567890', 'Solapur', 'stiching', 1, '2019-01-18 18:49:01', '2019-01-18 18:49:01'),
(12, 'Santosh', 'Sherla', NULL, '1234567890', 'Solapur', 'finishing', 1, '2019-01-18 18:49:27', '2019-01-18 18:49:27'),
(13, 'prabhakar', 'Putta', NULL, '1234567890', 'Solapur', 'stiching', 1, '2019-01-18 18:54:38', '2019-01-18 18:54:38'),
(14, 'balaji', 'ganpa', NULL, '1234567890', 'Solapur', 'stiching', 1, '2019-01-18 18:55:12', '2019-01-18 18:55:12'),
(15, 'Shriniwas', 'Sherla', NULL, '1234567890', 'Solapur', 'stiching', 1, '2019-01-18 18:55:41', '2019-01-18 18:55:41'),
(16, 'shrikant', 'sheral', 'shrikant@gmail.com', '9595660990', 'solapur', 'stiching', 1, '2019-01-19 04:40:29', '2019-01-19 04:40:29');

-- --------------------------------------------------------

--
-- Table structure for table `employee_payments`
--

CREATE TABLE `employee_payments` (
  `id` int(11) NOT NULL,
  `paymentsheet_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `total_pics` varchar(50) NOT NULL,
  `cal_amount` double NOT NULL,
  `esi` float NOT NULL,
  `pf` float NOT NULL,
  `totalamount` int(11) NOT NULL,
  `payable_amount` int(11) NOT NULL,
  `advance_cut` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_payments`
--

INSERT INTO `employee_payments` (`id`, `paymentsheet_id`, `employee_id`, `total_pics`, `cal_amount`, `esi`, `pf`, `totalamount`, `payable_amount`, `advance_cut`, `status`, `created_at`, `updated_at`) VALUES
(1, 883232, 8, '35 Pices', 1015, 100, 100, 815, 800, 15, 1, '2019-03-14 06:41:49', '2019-03-14 06:41:49'),
(2, 485584, 12, '25 Pices', 725, 10, 100, 615, 515, 100, 1, '2019-03-14 06:53:55', '2019-03-14 06:53:55'),
(3, 357886, 1, '40 Pices', 1000, 0, 0, 1000, 1000, NULL, 1, '2019-03-18 09:19:48', '2019-03-14 06:55:03'),
(4, 606955, 8, '15 Pices', 435, 0, 0, 435, 400, 35, 1, '2019-03-18 07:52:46', '2019-03-18 07:52:46');

-- --------------------------------------------------------

--
-- Table structure for table `fashions`
--

CREATE TABLE `fashions` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fashions`
--

INSERT INTO `fashions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(7, 'White-GF', '2019-01-19 06:41:02', '2019-01-19 06:41:02'),
(3, 'Shirt-BH.', NULL, '2019-01-19 06:38:17'),
(4, 'Shirt-BF', NULL, NULL),
(5, 'data', '2019-01-19 06:04:18', '2019-01-19 06:04:18'),
(6, 'White-BH', '2019-01-19 06:39:47', '2019-01-19 06:39:47'),
(8, 'White-BF', '2019-01-19 06:41:19', '2019-01-19 06:41:19'),
(9, 'Green-Bg', '2019-01-19 06:55:10', '2019-01-19 06:55:10');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `invoice_number` varchar(15) NOT NULL,
  `igst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `advance_payment` float DEFAULT NULL,
  `shipping` float DEFAULT NULL,
  `total` float NOT NULL,
  `remarks` text,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `party_id`, `invoice_number`, `igst`, `sgst`, `cgst`, `advance_payment`, `shipping`, `total`, `remarks`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, 4, '15326', 2.5, 2.5, 2.5, 200, 300, 8700, NULL, 0, '2019-03-16 07:07:21', '2019-03-16 12:37:21');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_item_details`
--

CREATE TABLE `invoice_item_details` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `cuttingdetails_id` int(11) NOT NULL,
  `stockdetail_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `rate` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_item_details`
--

INSERT INTO `invoice_item_details` (`id`, `invoice_id`, `cuttingdetails_id`, `stockdetail_id`, `quantity`, `rate`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 1, 200, 20, '2019-03-16 12:37:21', '2019-03-16 12:37:21'),
(2, 1, 7, 1, 200, 20, '2019-03-16 12:37:21', '2019-03-16 12:37:21');

-- --------------------------------------------------------

--
-- Table structure for table `parties`
--

CREATE TABLE `parties` (
  `id` int(11) NOT NULL,
  `firm_name` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` varchar(100) NOT NULL,
  `gst` varchar(15) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parties`
--

INSERT INTO `parties` (`id`, `firm_name`, `firstname`, `lastname`, `email`, `phone`, `address`, `gst`, `created_at`, `updated_at`, `active_status`) VALUES
(4, 'Malani Garments', 'Shailesh', 'Malani', NULL, '9422645420', 'Solapur', NULL, '2019-01-18 18:56:53', '2019-01-18 18:56:53', 1),
(5, 'Skylon Shirts', 'Vijaya', 'kolhapure', NULL, '1234567890', 'Solapur', NULL, '2019-01-18 18:57:34', '2019-01-18 18:57:34', 1),
(6, 'RR Bankapure', 'Rohan', 'Bankapure', NULL, '1234567890', 'Solapur', NULL, '2019-01-18 18:58:05', '2019-01-18 18:58:05', 1),
(7, 'united corp', 'gurjyot', 'narang', NULL, '1234567890', 'Ahmednagar', NULL, '2019-01-18 18:58:52', '2019-01-18 18:58:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `party_payments`
--

CREATE TABLE `party_payments` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `remarks` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `size` varchar(50) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size`, `active_status`, `created_at`, `updated_at`) VALUES
(1, '16', 1, '2019-01-03 17:26:11', '2019-01-03 17:26:11'),
(2, '18', 1, '2019-01-03 17:26:20', '2019-01-03 17:26:20'),
(3, '20', 1, '2019-01-03 17:26:24', '2019-01-03 17:26:24'),
(4, '22', 1, '2019-01-03 17:26:31', '2019-01-03 17:26:31'),
(5, '24', 1, '2019-01-03 10:26:44', '2019-01-03 17:26:44'),
(6, '26', 1, '2019-01-03 17:26:53', '2019-01-03 17:26:53'),
(7, '28', 1, '2019-01-03 17:27:01', '2019-01-03 17:27:01'),
(8, '30', 1, '2019-01-03 17:27:08', '2019-01-03 17:27:08'),
(9, '32', 1, '2019-01-03 17:27:12', '2019-01-03 17:27:12'),
(10, '34', 1, '2019-01-03 17:27:16', '2019-01-03 17:27:16'),
(11, '36', 1, '2019-01-03 17:27:19', '2019-01-03 17:27:19'),
(12, '38', 1, '2019-01-03 17:27:22', '2019-01-03 17:27:22'),
(13, '40', 1, '2019-01-03 17:27:26', '2019-01-03 17:27:26'),
(14, '42', 1, '2019-01-03 17:27:29', '2019-01-03 17:27:29'),
(15, '44', 1, '2019-01-03 17:27:33', '2019-01-03 17:27:33'),
(16, '46', 1, '2019-01-03 17:27:37', '2019-01-03 17:27:37'),
(17, '48', 1, '2019-01-03 17:27:44', '2019-01-03 17:27:44');

-- --------------------------------------------------------

--
-- Table structure for table `stitching_assignments`
--

CREATE TABLE `stitching_assignments` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `cuttingdetail_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `duedate` datetime NOT NULL,
  `rate` float NOT NULL,
  `fashion_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stitching_assignments`
--

INSERT INTO `stitching_assignments` (`id`, `employee_id`, `cuttingdetail_id`, `count`, `duedate`, `rate`, `fashion_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, '2019-03-14 00:00:00', 25, 7, '2019-03-14 04:41:09', '2019-03-14 04:41:09'),
(2, 1, 2, 10, '2019-03-14 00:00:00', 25, 7, '2019-03-14 04:41:09', '2019-03-14 04:41:09'),
(3, 1, 3, 10, '2019-03-14 00:00:00', 25, 7, '2019-03-14 04:41:09', '2019-03-14 04:41:09'),
(4, 1, 4, 10, '2019-03-14 00:00:00', 25, 7, '2019-03-14 04:41:09', '2019-03-14 04:41:09'),
(5, 12, 5, 10, '2019-03-14 00:00:00', 29, 7, '2019-03-14 06:35:57', '2019-03-14 06:35:57'),
(6, 12, 7, 5, '2019-03-14 00:00:00', 29, 7, '2019-03-14 06:35:57', '2019-03-14 06:35:57'),
(7, 12, 6, 5, '2019-03-14 00:00:00', 29, 7, '2019-03-14 06:35:58', '2019-03-14 06:35:58'),
(8, 12, 8, 5, '2019-03-14 00:00:00', 29, 7, '2019-03-14 06:35:58', '2019-03-14 06:35:58'),
(9, 8, 7, 15, '2019-03-14 00:00:00', 29, 7, '2019-03-14 06:36:37', '2019-03-14 06:36:37'),
(10, 8, 6, 5, '2019-03-14 00:00:00', 29, 7, '2019-03-14 06:36:37', '2019-03-14 06:36:37'),
(11, 8, 8, 15, '2019-03-14 00:00:00', 29, 7, '2019-03-14 06:36:37', '2019-03-14 06:36:37');

-- --------------------------------------------------------

--
-- Table structure for table `stitching_returns`
--

CREATE TABLE `stitching_returns` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `stitching_id` int(11) NOT NULL,
  `received_count` int(11) NOT NULL,
  `received_date` date NOT NULL,
  `paid_status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stitching_returns`
--

INSERT INTO `stitching_returns` (`id`, `employee_id`, `stitching_id`, `received_count`, `received_date`, `paid_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, '2019-03-14', 1, '2019-03-14 12:25:03', '2019-03-14 06:55:03'),
(2, 1, 2, 10, '2019-03-14', 1, '2019-03-14 12:25:03', '2019-03-14 06:55:03'),
(3, 1, 3, 10, '2019-03-14', 1, '2019-03-18 09:28:11', '2019-03-14 06:55:03'),
(4, 1, 4, 10, '2019-03-14', 1, '2019-03-14 12:25:03', '2019-03-14 06:55:03'),
(5, 8, 9, 15, '2019-03-14', 1, '2019-03-14 12:11:49', '2019-03-14 06:41:49'),
(6, 8, 10, 5, '2019-03-14', 1, '2019-03-14 12:11:49', '2019-03-14 06:41:49'),
(7, 8, 11, 15, '2019-03-14', 1, '2019-03-18 13:22:46', '2019-03-18 07:52:46'),
(8, 12, 5, 10, '2019-03-14', 0, '2019-03-18 13:19:51', '2019-03-14 06:53:55'),
(9, 12, 6, 5, '2019-03-14', 1, '2019-03-14 12:23:55', '2019-03-14 06:53:55'),
(10, 12, 7, 5, '2019-03-14', 1, '2019-03-14 12:23:55', '2019-03-14 06:53:55'),
(11, 12, 8, 5, '2019-03-14', 1, '2019-03-14 12:23:55', '2019-03-14 06:53:55');

-- --------------------------------------------------------

--
-- Table structure for table `stockdetails`
--

CREATE TABLE `stockdetails` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `length` double DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stockdetails`
--

INSERT INTO `stockdetails` (`id`, `stock_id`, `length`, `color`, `photo`, `created_at`, `updated_at`) VALUES
(1, 3, 100, 'red', 'F:\\xampp\\tmp\\phpC017.tmp', NULL, NULL),
(2, 3, 120, 'blue', 'F:\\xampp\\tmp\\phpC018.tmp', NULL, NULL),
(3, 7, 111, 'sasd', 'codeCenter-3.png', NULL, NULL),
(4, 10, 400, 'black', NULL, NULL, NULL),
(5, 11, 100, 'Golden', 'paystack-opengraph.jpg', NULL, '2019-03-19 11:54:19'),
(6, 12, 200, 'black', NULL, NULL, NULL),
(7, 13, 200, 'black', NULL, NULL, NULL),
(8, 13, 300, 'jilebi', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `dno` text NOT NULL,
  `challan_no` varchar(50) DEFAULT NULL,
  `cloth_name` varchar(100) DEFAULT NULL,
  `panna` varchar(10) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL COMMENT 'sample image',
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `party_id`, `dno`, `challan_no`, `cloth_name`, `panna`, `image`, `date`, `created_at`, `updated_at`) VALUES
(3, 5, '123', NULL, NULL, '60', 'paystack-opengraph.jpg', '2019-03-13', '2019-03-13 06:13:40', '2019-03-13 06:13:40'),
(7, 5, '456', '777', '888', '60', 'Slide2.JPG', '2019-03-13', '2019-03-13 06:43:51', '2019-03-13 06:43:51'),
(10, 5, '666', '888', '123', '60', NULL, '2019-03-13', '2019-03-13 06:46:46', '2019-03-13 06:46:46'),
(11, 4, '2727', 'CH1215', 'Atlance', '72', 'paystack-opengraph.jpg', '2019-03-14', '2019-03-14 04:24:40', '2019-03-14 04:24:40'),
(12, 6, '1585', 'K5869', NULL, '90', 'user_small.png', '2019-03-19', '2019-03-19 06:22:42', '2019-03-19 06:22:42'),
(13, 6, '1585', 'K5869', NULL, '90', 'user_small.png', '2019-03-19', '2019-03-19 06:22:58', '2019-03-19 06:22:58');

-- --------------------------------------------------------

--
-- Table structure for table `stock_fashions`
--

CREATE TABLE `stock_fashions` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `fashion_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_fashions`
--

INSERT INTO `stock_fashions` (`id`, `stock_id`, `fashion_id`, `created_at`, `updated_at`) VALUES
(1, 1, 7, '2019-03-13 11:41:30', '2019-03-13 11:41:30'),
(2, 3, 7, '2019-03-13 11:43:40', '2019-03-13 11:43:40'),
(3, 4, 4, '2019-03-13 12:11:47', '2019-03-13 12:11:47'),
(4, 5, 4, '2019-03-13 12:13:06', '2019-03-13 12:13:06'),
(5, 6, 4, '2019-03-13 12:13:33', '2019-03-13 12:13:33'),
(6, 7, 4, '2019-03-13 12:13:51', '2019-03-13 12:13:51'),
(7, 8, 7, '2019-03-13 12:15:47', '2019-03-13 12:15:47'),
(8, 9, 7, '2019-03-13 12:16:14', '2019-03-13 12:16:14'),
(9, 10, 7, '2019-03-13 12:16:46', '2019-03-13 12:16:46'),
(10, 11, 7, '2019-03-14 09:54:40', '2019-03-14 09:54:40'),
(11, 11, 3, '2019-03-14 09:54:40', '2019-03-14 09:54:40'),
(12, 11, 4, '2019-03-14 09:54:40', '2019-03-14 09:54:40'),
(13, 12, 7, '2019-03-19 11:52:42', '2019-03-19 11:52:42'),
(14, 12, 3, '2019-03-19 11:52:42', '2019-03-19 11:52:42'),
(15, 12, 4, '2019-03-19 11:52:42', '2019-03-19 11:52:42'),
(16, 13, 7, '2019-03-19 11:52:58', '2019-03-19 11:52:58'),
(17, 13, 3, '2019-03-19 11:52:58', '2019-03-19 11:52:58'),
(18, 13, 4, '2019-03-19 11:52:58', '2019-03-19 11:52:58');

-- --------------------------------------------------------

--
-- Table structure for table `stock_size_avgs`
--

CREATE TABLE `stock_size_avgs` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `avg` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_size_avgs`
--

INSERT INTO `stock_size_avgs` (`id`, `stock_id`, `size_id`, `avg`, `created_at`, `updated_at`) VALUES
(1, 11, 1, 1.24, '2019-03-14 10:10:06', '2019-03-14 10:10:06'),
(2, 11, 2, 1.29, '2019-03-14 10:10:06', '2019-03-14 10:10:06'),
(3, 11, 3, 1.36, '2019-03-14 10:10:06', '2019-03-14 10:10:06'),
(4, 11, 4, 1.42, '2019-03-14 10:10:06', '2019-03-14 10:10:06'),
(5, 3, 1, 1.24, '2019-03-14 12:05:18', '2019-03-14 12:05:18'),
(6, 3, 2, 1.29, '2019-03-14 12:05:18', '2019-03-14 12:05:18');

-- --------------------------------------------------------

--
-- Table structure for table `stock_uses`
--

CREATE TABLE `stock_uses` (
  `id` int(11) NOT NULL,
  `stockdetail_id` int(11) NOT NULL,
  `used_mtrs` double NOT NULL,
  `total_pcs` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_uses`
--

INSERT INTO `stock_uses` (`id`, `stockdetail_id`, `used_mtrs`, `total_pcs`, `created_at`, `updated_at`) VALUES
(1, 5, 53.2, 40, '2019-03-14 04:40:05', '2019-03-14 04:40:05'),
(2, 1, 38.1, 30, '2019-03-14 06:35:18', '2019-03-14 06:35:18'),
(3, 2, 38.1, 30, '2019-03-14 06:35:18', '2019-03-14 06:35:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User table to store all user details , permission';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `mobile_number`, `last_login`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'administrator', '9999999999', '2019-03-19 04:50:04', 1, 'HLetqqrzMfN7X1MvoGCNqQyT9UY9fa7jX9bnZkc0DzyR0wOIDoWeo2C0pTvu', '2018-11-22 18:30:00', '2019-03-19 04:50:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advances`
--
ALTER TABLE `advances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fashion_id` (`fashion_id`);

--
-- Indexes for table `cuttings`
--
ALTER TABLE `cuttings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_payments`
--
ALTER TABLE `employee_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fashions`
--
ALTER TABLE `fashions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `party_id` (`party_id`);

--
-- Indexes for table `invoice_item_details`
--
ALTER TABLE `invoice_item_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_id` (`invoice_id`),
  ADD KEY `cuttingdetails_id` (`cuttingdetails_id`);

--
-- Indexes for table `parties`
--
ALTER TABLE `parties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `party_payments`
--
ALTER TABLE `party_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stitching_assignments`
--
ALTER TABLE `stitching_assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stitching_returns`
--
ALTER TABLE `stitching_returns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockdetails`
--
ALTER TABLE `stockdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_fashions`
--
ALTER TABLE `stock_fashions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_id` (`stock_id`),
  ADD KEY `fashion_id` (`fashion_id`);

--
-- Indexes for table `stock_size_avgs`
--
ALTER TABLE `stock_size_avgs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_id` (`stock_id`),
  ADD KEY `size_id` (`size_id`);

--
-- Indexes for table `stock_uses`
--
ALTER TABLE `stock_uses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advances`
--
ALTER TABLE `advances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cuttings`
--
ALTER TABLE `cuttings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `employee_payments`
--
ALTER TABLE `employee_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fashions`
--
ALTER TABLE `fashions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invoice_item_details`
--
ALTER TABLE `invoice_item_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `parties`
--
ALTER TABLE `parties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `party_payments`
--
ALTER TABLE `party_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `stitching_assignments`
--
ALTER TABLE `stitching_assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `stitching_returns`
--
ALTER TABLE `stitching_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `stockdetails`
--
ALTER TABLE `stockdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `stock_fashions`
--
ALTER TABLE `stock_fashions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `stock_size_avgs`
--
ALTER TABLE `stock_size_avgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stock_uses`
--
ALTER TABLE `stock_uses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
