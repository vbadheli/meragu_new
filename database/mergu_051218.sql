-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2018 at 03:52 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mergu`
--

-- --------------------------------------------------------

--
-- Table structure for table `cuttingdetails`
--

CREATE TABLE `cuttingdetails` (
  `id` int(11) NOT NULL,
  `cutting_id` int(11) DEFAULT NULL,
  `stockdetail_id` int(11) NOT NULL,
  `size_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `avg` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttingdetails`
--

INSERT INTO `cuttingdetails` (`id`, `cutting_id`, `stockdetail_id`, `size_id`, `count`, `avg`, `created_at`, `updated_at`) VALUES
(1, 1, 9, 5, 11, NULL, '2018-12-05 14:51:03', '2018-12-05 14:51:03'),
(2, 1, 9, 2, 33, NULL, '2018-12-05 14:51:03', '2018-12-05 14:51:03'),
(3, 1, 10, 5, 22, NULL, '2018-12-05 14:51:03', '2018-12-05 14:51:03'),
(4, 1, 10, 2, 44, NULL, '2018-12-05 14:51:03', '2018-12-05 14:51:03');

-- --------------------------------------------------------

--
-- Table structure for table `cuttings`
--

CREATE TABLE `cuttings` (
  `id` int(11) NOT NULL,
  `party_id` int(11) DEFAULT NULL,
  `dno` varchar(15) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttings`
--

INSERT INTO `cuttings` (`id`, `party_id`, `dno`, `created_at`, `updated_at`) VALUES
(1, 5, '3', '2018-12-05 09:21:03', '2018-12-05 09:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(100) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `firstname`, `lastname`, `email`, `phone`, `address`, `active_status`, `created_at`, `updated_at`) VALUES
(1, 'testing', 'testing', 'test@gmail.com', '9999999999', 'Solapur', 1, '2018-11-21 10:34:23', '2018-11-21 05:04:23'),
(3, 'kaka', 'kaka', 'kaka@gmail.com', '7474747474', 'solapur', 1, '2018-11-21 05:06:32', '2018-11-21 05:06:32'),
(4, 'Pravin', 'Lagshetti', 'pravin@gmail.com', '8475858589', 'solapur', 1, '2018-11-22 05:28:33', '2018-11-22 05:28:33');

-- --------------------------------------------------------

--
-- Table structure for table `parties`
--

CREATE TABLE `parties` (
  `id` int(11) NOT NULL,
  `firm_name` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `gst` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parties`
--

INSERT INTO `parties` (`id`, `firm_name`, `firstname`, `lastname`, `email`, `phone`, `address`, `gst`, `created_at`, `updated_at`, `active_status`) VALUES
(1, 'Nitesh Garment', 'Nitesh', 'Garment', 'nitesh@gmail.com', '8600914255', 'Solapur', 'G274515425', '2018-11-21 10:06:29', '2018-11-21 04:36:29', 1),
(5, 'Speed Garment', 'Amit', 'Bakale', 'speed@gmail.com', '9876543210', 'Solapur', 'G2715425266', '2018-11-21 04:27:40', '2018-11-21 04:27:40', 1),
(6, 'Kamala Garment', 'Kamala', 'Nagi', 'kamala@gmail.com', '748758747', 'solapur', 'G1452456366', '2018-11-27 04:18:18', '2018-11-27 04:18:18', 1),
(7, 'Indian Garment', 'indian', 'garment', 'indian@gmail.com', '8475969696', 'solapur', 'G2745875858', '2018-11-27 04:30:45', '2018-11-27 04:30:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `size` varchar(50) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size`, `active_status`, `created_at`, `updated_at`) VALUES
(2, '4', 1, '2018-11-23 00:42:15', '2018-11-23 00:42:15'),
(3, '6', 1, '2018-11-23 00:42:20', '2018-11-23 00:42:20'),
(4, '8', 1, '2018-11-23 00:42:24', '2018-11-23 00:42:24'),
(5, '10', 1, '2018-11-23 00:42:27', '2018-11-23 00:42:27'),
(6, '12', 1, '2018-11-24 06:52:34', '2018-11-24 06:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `stockdetails`
--

CREATE TABLE `stockdetails` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `length` double DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stockdetails`
--

INSERT INTO `stockdetails` (`id`, `stock_id`, `length`, `color`, `photo`) VALUES
(1, 1, 8, 'red', '41wZ0gukeaL.jpg'),
(2, 1, 10, 'green', '4.jpg'),
(3, 1, 12, 'checks', '814LKB4f1PL._UL1500_.jpg'),
(4, 1, 14, 'model', '81whybZi5LL._UL1500_.jpg'),
(5, 1, 15, 'round', '97b5ca9f0283b091581acb477b0ffedb.jpg'),
(6, 2, 8, 'green', '1134182_final_520x520_1535371817._CB485600720_SY260_.jpg'),
(7, 2, 10, 'pink', 'apple-iphone-xs-max-mt522hn-a-original-imaf97f6hmng8ufu.jpeg'),
(8, 2, 12, 'magenta', 'android.jpg'),
(9, 3, 12, 'lemon', 'xWeb_Design_a_Development.png.pagespeed.ic_.nJQ2R2M6Jw.jpg'),
(10, 3, 10, 'green', '1134182_final_520x520_1535371817._CB485600720_SY260_.jpg'),
(11, 4, 50, 'red', NULL),
(12, 5, 50, 'lemon', 'packman-v2.gif'),
(13, 5, 40, 'black', 'codeCenter-3.png'),
(14, 6, 1, 'kala', NULL),
(15, 7, 1.5, 'Green', 'packman-v2.gif'),
(16, 7, 2.00002, 'need', NULL),
(17, 7, 1.50004, 'nagative', NULL),
(18, 7, 12.3, '2Milinak', 'codeCenter-3.png'),
(19, 8, 10, 'orange', 'Airbus_Pleiades_50cm_8bit_RGB_Yogyakarta.jpg'),
(20, 8, 20, 'pink', 'DigitalGlobe_WorldView2_50cm_8bit_Pansharpened_RGB_DRA_Rome_Italy_2009DEC10_8bits_sub_r_1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `dno` text NOT NULL,
  `challan_no` varchar(50) DEFAULT NULL,
  `cloth_name` varchar(100) DEFAULT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `party_id`, `dno`, `challan_no`, `cloth_name`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, '1234', '154525', '', '2018-11-22', '2018-11-24 08:44:42', '2018-11-22 03:04:03'),
(2, 1, '54', '5455', '', '2018-11-22', '2018-11-24 08:44:39', '2018-11-22 03:05:05'),
(3, 5, '6969', '4554', '1244', '2018-11-22', '2018-11-24 07:44:48', '2018-11-22 04:36:35'),
(4, 5, '1001', '1001', '1', '2018-11-23', '2018-11-24 07:44:52', '2018-11-23 08:10:45'),
(5, 1, '1234', '12345', 'Maftalal', '2018-11-24', '2018-11-24 06:09:38', '2018-11-24 06:09:38'),
(6, 1, '54848', '66666', NULL, '2018-11-24', '2018-11-27 11:17:43', '2018-11-24 07:07:07'),
(7, 5, 'G7848G12LK', 'K12546987548MA12CK', 'BMC', '2018-11-24', '2018-11-24 07:11:05', '2018-11-24 07:11:05'),
(8, 6, '1515', '12123HER', 'Thane', '2018-11-27', '2018-11-27 04:40:53', '2018-11-27 04:40:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User table to store all user details , permission';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `mobile_number`, `last_login`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'administrator', '9999999999', '2018-12-05 05:27:07', 1, 'mdyVLcbV4JerjOQrvFFELlf9cHzaLWsEqaDbV3LhH5KLyr58H6lFM0lygOZt', '2018-11-22 18:30:00', '2018-12-05 05:27:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuttings`
--
ALTER TABLE `cuttings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parties`
--
ALTER TABLE `parties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockdetails`
--
ALTER TABLE `stockdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cuttings`
--
ALTER TABLE `cuttings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `parties`
--
ALTER TABLE `parties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stockdetails`
--
ALTER TABLE `stockdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
