-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 10, 2018 at 10:49 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mergu`
--

-- --------------------------------------------------------

--
-- Table structure for table `cuttingdetails`
--

CREATE TABLE `cuttingdetails` (
  `id` int(11) NOT NULL,
  `cutting_id` int(11) DEFAULT NULL,
  `stockdetail_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `avg` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttingdetails`
--

INSERT INTO `cuttingdetails` (`id`, `cutting_id`, `stockdetail_id`, `size_id`, `count`, `avg`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 10, 1.25, '2018-12-08 04:56:56', '2018-12-08 04:56:56'),
(2, 1, 2, 2, 30, 1.25, '2018-12-08 04:56:56', '2018-12-08 04:56:56'),
(3, 1, 3, 2, 50, 1.25, '2018-12-08 04:56:56', '2018-12-08 04:56:56'),
(4, 1, 4, 2, 70, 1.25, '2018-12-08 04:56:56', '2018-12-08 04:56:56'),
(5, 1, 5, 2, 90, 1.25, '2018-12-08 04:56:56', '2018-12-08 04:56:56'),
(6, 1, 1, 3, 20, 1.35, '2018-12-08 04:56:56', '2018-12-08 04:56:56'),
(7, 1, 2, 3, 40, 1.35, '2018-12-08 04:56:56', '2018-12-08 04:56:56'),
(8, 1, 3, 3, 60, 1.35, '2018-12-08 04:56:56', '2018-12-08 04:56:56'),
(9, 1, 4, 3, 80, 1.35, '2018-12-08 04:56:56', '2018-12-08 04:56:56'),
(10, 1, 5, 3, 100, 1.35, '2018-12-08 04:56:56', '2018-12-08 04:56:56'),
(11, 2, 6, 2, 5, 1.23, '2018-12-08 10:12:32', '2018-12-08 10:12:32'),
(12, 2, 7, 2, 10, 1.23, '2018-12-08 10:12:33', '2018-12-08 10:12:33'),
(13, 2, 8, 2, 10, 1.23, '2018-12-08 10:12:33', '2018-12-08 10:12:33'),
(14, 2, 6, 3, 5, 1.29, '2018-12-08 10:12:33', '2018-12-08 10:12:33'),
(15, 2, 7, 3, 10, 1.29, '2018-12-08 10:12:33', '2018-12-08 10:12:33'),
(16, 2, 8, 3, 10, 1.29, '2018-12-08 10:12:33', '2018-12-08 10:12:33');

-- --------------------------------------------------------

--
-- Table structure for table `cuttings`
--

CREATE TABLE `cuttings` (
  `id` int(11) NOT NULL,
  `party_id` int(11) DEFAULT NULL,
  `dno` varchar(15) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttings`
--

INSERT INTO `cuttings` (`id`, `party_id`, `dno`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '2018-12-07 23:26:56', '2018-12-07 23:26:56'),
(2, 1, '2', '2018-12-08 04:42:32', '2018-12-08 04:42:32');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(100) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `firstname`, `lastname`, `email`, `phone`, `address`, `active_status`, `created_at`, `updated_at`) VALUES
(1, 'testing', 'testing', 'test@gmail.com', '9999999999', 'Solapur', 1, '2018-11-21 10:34:23', '2018-11-21 05:04:23'),
(3, 'kaka', 'kaka', 'kaka@gmail.com', '7474747474', 'solapur', 1, '2018-11-21 05:06:32', '2018-11-21 05:06:32'),
(4, 'Pravin', 'Lagshetti', 'pravin@gmail.com', '8475858589', 'solapur', 1, '2018-11-22 05:28:33', '2018-11-22 05:28:33'),
(5, 'Amrut', 'Halnor', 'amrut@gmail.com', '8485895750', 'solapur', 1, '2018-12-08 02:44:55', '2018-12-08 02:44:55');

-- --------------------------------------------------------

--
-- Table structure for table `parties`
--

CREATE TABLE `parties` (
  `id` int(11) NOT NULL,
  `firm_name` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `gst` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parties`
--

INSERT INTO `parties` (`id`, `firm_name`, `firstname`, `lastname`, `email`, `phone`, `address`, `gst`, `created_at`, `updated_at`, `active_status`) VALUES
(1, 'Nitesh Garment', 'Nitesh', 'Garment', 'nitesh@gmail.com', '8600914255', 'Solapur', 'G274515425', '2018-11-21 10:06:29', '2018-11-21 04:36:29', 1),
(5, 'Speed Garment', 'Amit', 'Bakale', 'speed@gmail.com', '9876543210', 'Solapur', 'G2715425266', '2018-11-21 04:27:40', '2018-11-21 04:27:40', 1),
(6, 'Mg Garments', 'Narendra', 'Mergu', NULL, '123456790', 's', '12', '2018-12-06 04:28:46', '2018-12-06 04:28:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `size` varchar(50) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size`, `active_status`, `created_at`, `updated_at`) VALUES
(2, '4', 1, '2018-11-23 00:42:15', '2018-11-23 00:42:15'),
(3, '6', 1, '2018-11-23 00:42:20', '2018-11-23 00:42:20'),
(4, '8', 1, '2018-11-23 00:42:24', '2018-11-23 00:42:24'),
(5, '10', 1, '2018-11-23 00:42:27', '2018-11-23 00:42:27'),
(6, '12', 1, '2018-11-24 06:52:34', '2018-11-24 06:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `stitching_assignments`
--

CREATE TABLE `stitching_assignments` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `cuttingdetail_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `duedate` datetime NOT NULL,
  `rate` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stitching_assignments`
--

INSERT INTO `stitching_assignments` (`id`, `employee_id`, `cuttingdetail_id`, `count`, `duedate`, `rate`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 5, '2018-12-14 00:00:00', 10, '2018-12-08 05:42:24', '0000-00-00 00:00:00'),
(2, 1, 2, 10, '2018-12-15 00:00:00', 15, '2018-12-08 00:35:15', '2018-12-08 00:35:15'),
(3, 1, 3, 10, '2018-12-15 00:00:00', 15, '2018-12-08 00:35:15', '2018-12-08 00:35:15'),
(4, 1, 4, 10, '2018-12-15 00:00:00', 15, '2018-12-08 00:35:15', '2018-12-08 00:35:15'),
(5, 4, 1, 2, '2018-12-08 00:00:00', 12, '2018-12-08 02:03:45', '2018-12-08 02:03:45'),
(6, 4, 2, 2, '2018-12-08 00:00:00', 12, '2018-12-08 02:03:45', '2018-12-08 02:03:45'),
(7, 4, 3, 10, '2018-12-08 00:00:00', 12, '2018-12-08 02:03:45', '2018-12-08 02:03:45'),
(8, 4, 4, 10, '2018-12-08 00:00:00', 12, '2018-12-08 02:03:45', '2018-12-08 02:03:45'),
(9, 4, 5, 10, '2018-12-08 00:00:00', 12, '2018-12-08 02:03:45', '2018-12-08 02:03:45'),
(10, 1, 12, 5, '2018-12-08 00:00:00', 10, '2018-12-08 04:43:05', '2018-12-08 04:43:05'),
(11, 1, 13, 5, '2018-12-08 00:00:00', 10, '2018-12-08 04:43:06', '2018-12-08 04:43:06'),
(12, 1, 1, 3, '2018-12-10 00:00:00', 15, '2018-12-09 23:03:51', '2018-12-09 23:03:51'),
(13, 1, 3, 15, '2018-12-10 00:00:00', 15, '2018-12-09 23:03:51', '2018-12-09 23:03:51');

-- --------------------------------------------------------

--
-- Table structure for table `stitching_returns`
--

CREATE TABLE `stitching_returns` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `stitching_id` int(11) NOT NULL,
  `received_count` int(11) NOT NULL,
  `received_date` datetime NOT NULL,
  `paid_status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stitching_returns`
--

INSERT INTO `stitching_returns` (`id`, `employee_id`, `stitching_id`, `received_count`, `received_date`, `paid_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2018-12-08 00:00:00', 0, '2018-12-08 01:52:16', '2018-12-08 01:52:16'),
(2, 1, 2, 1, '2018-12-08 00:00:00', 0, '2018-12-08 01:52:16', '2018-12-08 01:52:16'),
(3, 1, 3, 1, '2018-12-08 00:00:00', 0, '2018-12-08 01:52:16', '2018-12-08 01:52:16'),
(4, 1, 4, 1, '2018-12-08 00:00:00', 0, '2018-12-08 01:52:16', '2018-12-08 01:52:16'),
(5, 4, 7, 2, '2018-12-08 00:00:00', 0, '2018-12-08 02:04:19', '2018-12-08 02:04:19'),
(6, 4, 8, 2, '2018-12-08 00:00:00', 0, '2018-12-08 02:04:19', '2018-12-08 02:04:19'),
(7, 4, 9, 2, '2018-12-08 00:00:00', 0, '2018-12-08 02:04:19', '2018-12-08 02:04:19'),
(8, 1, 7, 2, '2018-12-08 00:00:00', 0, '2018-12-08 08:30:34', '2018-12-08 08:30:34'),
(9, 1, 8, 1, '2018-12-08 00:00:00', 0, '2018-12-08 08:30:34', '2018-12-08 08:30:34'),
(10, 4, 5, 5, '2018-12-08 00:00:00', 0, '2018-12-08 08:31:23', '2018-12-08 08:31:23'),
(11, 1, 1, 5, '2018-12-10 00:00:00', 0, '2018-12-09 23:06:36', '2018-12-09 23:06:36'),
(12, 1, 1, 3, '2018-12-10 00:00:00', 0, '2018-12-09 23:06:36', '2018-12-09 23:06:36'),
(13, 1, 1, 2, '2018-12-10 00:00:00', 0, '2018-12-10 03:58:01', '2018-12-10 03:58:01'),
(14, 1, 2, 5, '2018-12-10 00:00:00', 0, '2018-12-10 04:14:35', '2018-12-10 04:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `stockdetails`
--

CREATE TABLE `stockdetails` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `length` double DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stockdetails`
--

INSERT INTO `stockdetails` (`id`, `stock_id`, `length`, `color`, `photo`) VALUES
(1, 1, 8, 'red', '41wZ0gukeaL.jpg'),
(2, 1, 10, 'green', '4.jpg'),
(3, 1, 12, 'checks', '814LKB4f1PL._UL1500_.jpg'),
(4, 1, 14, 'model', '81whybZi5LL._UL1500_.jpg'),
(5, 1, 15, 'round', '97b5ca9f0283b091581acb477b0ffedb.jpg'),
(6, 2, 8, 'green', '1134182_final_520x520_1535371817._CB485600720_SY260_.jpg'),
(7, 2, 10, 'pink', 'apple-iphone-xs-max-mt522hn-a-original-imaf97f6hmng8ufu.jpeg'),
(8, 2, 12, 'magenta', 'android.jpg'),
(9, 3, 12, 'lemon', 'xWeb_Design_a_Development.png.pagespeed.ic_.nJQ2R2M6Jw.jpg'),
(10, 3, 10, 'green', '1134182_final_520x520_1535371817._CB485600720_SY260_.jpg'),
(11, 4, 50, 'red', NULL),
(12, 5, 50, 'lemon', 'packman-v2.gif'),
(13, 5, 40, 'black', 'codeCenter-3.png'),
(14, 6, 1.01, 'kala', NULL),
(15, 7, 1.5, 'Green', 'packman-v2.gif'),
(16, 7, 2.00002, 'need', NULL),
(17, 7, 1.50004, 'nagative', NULL),
(18, 7, 12.3, '2Milinak', 'codeCenter-3.png'),
(19, 8, 100, 'red', NULL),
(20, 8, 100, 'blue', NULL),
(21, 8, 150, 'white', NULL),
(22, 9, 100, 'rr', NULL),
(23, 10, 50, 'Light green', NULL),
(24, 10, 200, 'Japnees', NULL),
(25, 10, 100, 'Checks', NULL),
(26, 10, 100, 'think', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `dno` text NOT NULL,
  `challan_no` varchar(50) DEFAULT NULL,
  `cloth_name` varchar(100) DEFAULT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `party_id`, `dno`, `challan_no`, `cloth_name`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, '1234', '154525', '', '2018-11-22', '2018-11-24 08:44:42', '2018-11-22 03:04:03'),
(2, 1, '54', '5455', '', '2018-11-22', '2018-11-24 08:44:39', '2018-11-22 03:05:05'),
(3, 5, '6969', '4554', '1244', '2018-11-22', '2018-11-24 07:44:48', '2018-11-22 04:36:35'),
(4, 5, '1001', '1001', '1', '2018-11-23', '2018-11-24 07:44:52', '2018-11-23 08:10:45'),
(5, 1, '1234', '12345', 'Maftalal', '2018-11-24', '2018-11-24 06:09:38', '2018-11-24 06:09:38'),
(6, 1, '200', 'asldfsakjfdksajfdkasjfkldjkfds10', NULL, '2018-11-24', '2018-12-08 07:37:56', '2018-11-24 07:07:07'),
(7, 5, '40', 'K12546987548MA12CK', 'BMC', '2018-11-24', '2018-12-08 07:38:21', '2018-11-24 07:11:05'),
(8, 6, '1100', NULL, 'remond', '2018-12-06', '2018-12-06 04:29:41', '2018-12-06 04:29:41'),
(9, 6, '1090', NULL, 'test', '2018-12-07', '2018-12-06 22:35:35', '2018-12-06 22:35:35'),
(10, 5, 'C142ZA', 'VXA1252', 'Batiya', '2018-12-08', '2018-12-08 02:35:07', '2018-12-08 02:35:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User table to store all user details , permission';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `mobile_number`, `last_login`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'administrator', '9999999999', '2018-12-09 22:34:05', 1, 'eFXAPdsSvRBFfHycJKuNZvqwvjPgoVPNMs2IHZkjvgp352xobyWrz6P7012H', '2018-11-22 18:30:00', '2018-12-09 22:34:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuttings`
--
ALTER TABLE `cuttings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parties`
--
ALTER TABLE `parties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stitching_assignments`
--
ALTER TABLE `stitching_assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stitching_returns`
--
ALTER TABLE `stitching_returns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockdetails`
--
ALTER TABLE `stockdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cuttings`
--
ALTER TABLE `cuttings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `parties`
--
ALTER TABLE `parties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stitching_assignments`
--
ALTER TABLE `stitching_assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `stitching_returns`
--
ALTER TABLE `stitching_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `stockdetails`
--
ALTER TABLE `stockdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
