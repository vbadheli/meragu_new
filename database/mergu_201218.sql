-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 20, 2018 at 08:18 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mergu`
--

-- --------------------------------------------------------

--
-- Table structure for table `advances`
--

CREATE TABLE `advances` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advances`
--

INSERT INTO `advances` (`id`, `employee_id`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 100, 0, '2018-12-11 08:40:50', '2018-12-11 08:40:50'),
(2, 3, 100, 0, '2018-12-11 08:40:56', '2018-12-11 08:40:56'),
(3, 4, 100, 0, '2018-12-11 08:41:03', '2018-12-11 08:41:03'),
(4, 5, 0, 1, '2018-12-11 14:11:23', '2018-12-11 08:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `cuttingdetails`
--

CREATE TABLE `cuttingdetails` (
  `id` int(11) NOT NULL,
  `cutting_id` int(11) DEFAULT NULL,
  `stockdetail_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `avg` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttingdetails`
--

INSERT INTO `cuttingdetails` (`id`, `cutting_id`, `stockdetail_id`, `size_id`, `count`, `avg`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, 10, 1.24, '2018-12-19 11:35:46', '2018-12-19 11:35:46'),
(2, 1, 3, 2, 10, 1.24, '2018-12-19 11:35:46', '2018-12-19 11:35:46'),
(3, 1, 4, 2, 15, 1.24, '2018-12-19 11:35:46', '2018-12-19 11:35:46'),
(4, 1, 2, 3, 10, 1.24, '2018-12-19 11:35:46', '2018-12-19 11:35:46'),
(5, 1, 3, 3, 18, 1.24, '2018-12-19 11:35:46', '2018-12-19 11:35:46'),
(6, 1, 4, 3, 15, 1.24, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(7, 1, 2, 4, 10, 1.24, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(8, 1, 3, 4, 20, 1.24, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(9, 1, 4, 4, 15, 1.24, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(10, 1, 2, 5, 10, 1.25, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(11, 1, 3, 5, 20, 1.25, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(12, 1, 4, 5, 15, 1.25, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(13, 1, 2, 6, 40, 1.25, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(14, 1, 3, 6, 20, 1.25, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(15, 1, 4, 6, 15, 1.25, '2018-12-19 11:35:47', '2018-12-19 11:35:47');

-- --------------------------------------------------------

--
-- Table structure for table `cuttings`
--

CREATE TABLE `cuttings` (
  `id` int(11) NOT NULL,
  `party_id` int(11) DEFAULT NULL,
  `stock_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuttings`
--

INSERT INTO `cuttings` (`id`, `party_id`, `stock_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2018-12-19 06:05:46', '2018-12-19 06:05:46');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(100) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `firstname`, `lastname`, `email`, `phone`, `address`, `active_status`, `created_at`, `updated_at`) VALUES
(1, 'testing', 'testing', 'test@gmail.com', '9999999999', 'Solapur', 1, '2018-11-21 10:34:23', '2018-11-21 05:04:23'),
(3, 'kaka', 'kaka', 'kaka@gmail.com', '7474747474', 'solapur', 1, '2018-11-21 05:06:32', '2018-11-21 05:06:32'),
(4, 'Pravin', 'Lagshetti', 'pravin@gmail.com', '8475858589', 'solapur', 1, '2018-11-22 05:28:33', '2018-11-22 05:28:33'),
(5, 'Amrut', 'Halnor', 'amrut@gmail.com', '8485895750', 'solapur', 1, '2018-12-08 02:44:55', '2018-12-08 02:44:55'),
(6, 'amrut', 'halnor', 'test@gmail.com', '9999999999', 'Solapur', 1, '2018-12-12 01:32:52', '2018-12-12 01:32:52'),
(7, 'vishal', 'adheli', 'vbadheli@gmail.com', '9999999999', 'Solapur', 1, '2018-12-12 01:34:11', '2018-12-12 01:34:11'),
(8, 'laxmi', 'test', 'testlaxmi@gmail.com', '9999999999', 'Solapur', 1, '2018-12-12 01:34:46', '2018-12-12 01:34:46');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `invoice_number` varchar(15) NOT NULL,
  `igst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `advance_payment` float DEFAULT NULL,
  `shipping` float DEFAULT NULL,
  `total` float NOT NULL,
  `remarks` text,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `party_id`, `invoice_number`, `igst`, `sgst`, `cgst`, `advance_payment`, `shipping`, `total`, `remarks`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, 1, '1002', 2, 2.5, 2.2, 0, 500, 5000, NULL, 0, '2018-12-19 06:12:33', '2018-12-19 11:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_item_details`
--

CREATE TABLE `invoice_item_details` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `cuttingdetails_id` int(11) NOT NULL,
  `stockdetail_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `rate` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_item_details`
--

INSERT INTO `invoice_item_details` (`id`, `invoice_id`, `cuttingdetails_id`, `stockdetail_id`, `quantity`, `rate`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 15, 150, '2018-12-19 11:42:33', '2018-12-19 11:42:33'),
(2, 1, 4, 2, 15, 150, '2018-12-19 11:42:33', '2018-12-19 11:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `parties`
--

CREATE TABLE `parties` (
  `id` int(11) NOT NULL,
  `firm_name` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `gst` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parties`
--

INSERT INTO `parties` (`id`, `firm_name`, `firstname`, `lastname`, `email`, `phone`, `address`, `gst`, `created_at`, `updated_at`, `active_status`) VALUES
(1, 'Nitesh Garment', 'Nitesh', 'Garment', 'nitesh@gmail.com', '8600914255', 'Solapur', 'G274515425', '2018-11-21 10:06:29', '2018-11-21 04:36:29', 1),
(5, 'Speed Garment', 'Amit', 'Bakale', 'speed@gmail.com', '9876543210', 'Solapur', 'G2715425266', '2018-11-21 04:27:40', '2018-11-21 04:27:40', 1),
(6, 'Mg Garments', 'Narendra', 'Mergu', NULL, '123456790', 's', '12', '2018-12-06 04:28:46', '2018-12-06 04:28:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `party_payments`
--

CREATE TABLE `party_payments` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `remarks` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `party_payments`
--

INSERT INTO `party_payments` (`id`, `invoice_id`, `amount`, `remarks`, `created_at`, `updated_at`) VALUES
(1, 1, 100, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '2018-12-19 12:48:05', '2018-12-19 12:48:05'),
(2, 1, 150, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '2018-12-19 12:50:50', '2018-12-19 12:50:50'),
(3, 1, 150, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '2018-12-19 12:51:09', '2018-12-19 12:51:09'),
(4, 1, 150, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '2018-12-19 12:51:30', '2018-12-19 12:51:30'),
(5, 1, 1000, 'this is a test', '2018-12-19 13:34:24', '2018-12-19 13:34:24');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `size` varchar(50) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size`, `active_status`, `created_at`, `updated_at`) VALUES
(2, '4', 1, '2018-11-23 00:42:15', '2018-11-23 00:42:15'),
(3, '6', 1, '2018-11-23 00:42:20', '2018-11-23 00:42:20'),
(4, '8', 1, '2018-11-23 00:42:24', '2018-11-23 00:42:24'),
(5, '10', 1, '2018-11-23 00:42:27', '2018-11-23 00:42:27'),
(6, '12', 1, '2018-11-24 06:52:34', '2018-11-24 06:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `stitching_assignments`
--

CREATE TABLE `stitching_assignments` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `cuttingdetail_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `duedate` datetime NOT NULL,
  `rate` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stitching_assignments`
--

INSERT INTO `stitching_assignments` (`id`, `employee_id`, `cuttingdetail_id`, `count`, `duedate`, `rate`, `created_at`, `updated_at`) VALUES
(1, 7, 1, 10, '2018-12-19 00:00:00', 25, '2018-12-19 06:07:14', '2018-12-19 06:07:14'),
(2, 7, 4, 10, '2018-12-19 00:00:00', 25, '2018-12-19 06:07:14', '2018-12-19 06:07:14'),
(3, 7, 7, 10, '2018-12-19 00:00:00', 25, '2018-12-19 06:07:14', '2018-12-19 06:07:14'),
(4, 7, 10, 10, '2018-12-19 00:00:00', 25, '2018-12-19 06:07:14', '2018-12-19 06:07:14'),
(5, 7, 13, 10, '2018-12-19 00:00:00', 25, '2018-12-19 06:07:15', '2018-12-19 06:07:15'),
(6, 7, 2, 10, '2018-12-19 00:00:00', 25, '2018-12-19 06:07:15', '2018-12-19 06:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `stitching_returns`
--

CREATE TABLE `stitching_returns` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `stitching_id` int(11) NOT NULL,
  `received_count` int(11) NOT NULL,
  `received_date` datetime NOT NULL,
  `paid_status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stockdetails`
--

CREATE TABLE `stockdetails` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `length` double DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stockdetails`
--

INSERT INTO `stockdetails` (`id`, `stock_id`, `length`, `color`, `photo`) VALUES
(1, 1, NULL, NULL, NULL),
(2, 2, 100, 'redchecks', NULL),
(3, 2, 110, 'blue bird', NULL),
(4, 2, 120, 'grey hair', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `dno` text NOT NULL,
  `challan_no` varchar(50) DEFAULT NULL,
  `cloth_name` varchar(100) DEFAULT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `party_id`, `dno`, `challan_no`, `cloth_name`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, 'rr1000', NULL, 'reymonds', '2018-12-19', '2018-12-19 06:03:50', '2018-12-19 06:03:50'),
(2, 1, 'rr1200', NULL, 'test', '2018-12-19', '2018-12-19 06:04:37', '2018-12-19 06:04:37');

-- --------------------------------------------------------

--
-- Table structure for table `stock_size_avgs`
--

CREATE TABLE `stock_size_avgs` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `avg` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_size_avgs`
--

INSERT INTO `stock_size_avgs` (`id`, `stock_id`, `size_id`, `avg`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1.24, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(2, 2, 3, 1.24, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(3, 2, 4, 1.24, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(4, 2, 5, 1.25, '2018-12-19 11:35:47', '2018-12-19 11:35:47'),
(5, 2, 6, 1.25, '2018-12-19 11:35:47', '2018-12-19 11:35:47');

-- --------------------------------------------------------

--
-- Table structure for table `stock_uses`
--

CREATE TABLE `stock_uses` (
  `id` int(11) NOT NULL,
  `stockdetail_id` int(11) NOT NULL,
  `used_mtrs` double NOT NULL,
  `total_pcs` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_uses`
--

INSERT INTO `stock_uses` (`id`, `stockdetail_id`, `used_mtrs`, `total_pcs`, `created_at`, `updated_at`) VALUES
(1, 2, 99.2, 80, '2018-12-19 06:05:47', '2018-12-19 06:05:47'),
(2, 3, 109.12, 88, '2018-12-19 06:05:47', '2018-12-19 06:05:47'),
(3, 4, 93, 75, '2018-12-19 06:05:47', '2018-12-19 06:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User table to store all user details , permission';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `mobile_number`, `last_login`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'administrator', '9999999999', '2018-12-19 23:10:30', 1, 'ZyRqOBpwpHOPUte6TVPqdHNIyzFhiDDhiIGliCZL6nglrsemobeQQ8cMU8Gq', '2018-11-22 18:30:00', '2018-12-19 23:10:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advances`
--
ALTER TABLE `advances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuttings`
--
ALTER TABLE `cuttings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `party_id` (`party_id`);

--
-- Indexes for table `invoice_item_details`
--
ALTER TABLE `invoice_item_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_id` (`invoice_id`),
  ADD KEY `cuttingdetails_id` (`cuttingdetails_id`);

--
-- Indexes for table `parties`
--
ALTER TABLE `parties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `party_payments`
--
ALTER TABLE `party_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stitching_assignments`
--
ALTER TABLE `stitching_assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stitching_returns`
--
ALTER TABLE `stitching_returns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockdetails`
--
ALTER TABLE `stockdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_size_avgs`
--
ALTER TABLE `stock_size_avgs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_id` (`stock_id`),
  ADD KEY `size_id` (`size_id`);

--
-- Indexes for table `stock_uses`
--
ALTER TABLE `stock_uses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advances`
--
ALTER TABLE `advances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cuttingdetails`
--
ALTER TABLE `cuttingdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `cuttings`
--
ALTER TABLE `cuttings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invoice_item_details`
--
ALTER TABLE `invoice_item_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `parties`
--
ALTER TABLE `parties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `party_payments`
--
ALTER TABLE `party_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stitching_assignments`
--
ALTER TABLE `stitching_assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stitching_returns`
--
ALTER TABLE `stitching_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stockdetails`
--
ALTER TABLE `stockdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stock_size_avgs`
--
ALTER TABLE `stock_size_avgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stock_uses`
--
ALTER TABLE `stock_uses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
