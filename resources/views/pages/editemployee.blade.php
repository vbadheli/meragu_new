@extends('layout.app')
@section('content')
@if(isset($data))
<div class="row">	
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Edit Employee</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/employee/update',$data->id)}}" method="POST">
                     	@csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                {{ session('success') }}
                              </div>
                           @endif
                        <fieldset>                           
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="firstname">First Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="firstname" value="{{$data->firstname}}" name="firstname" placeholder="Enter Your First Name" required="">
                                 @if ($errors->any() && $errors->has('firstname'))
                                 <span class="alert alert-danger">{{$errors->first('firstname')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="lastname">Last Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="lastname" name="lastname" value="{{$data->lastname}}" placeholder="Enter Your Last Name" required="">
                                 @if ($errors->any() && $errors->has('lastname'))
                                 <span class="alert alert-danger">{{$errors->first('lastname')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->            
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="category">Category</label>
                              <div class="controls">
                                 <select class="span4" id="category" name="category">
                                    <option>Select category</option>
                                    <option @if($data->category == 'cutting') {{"selected"}} @endif>Cutting</option>
                                    <option @if($data->category == 'stiching') {{"selected"}} @endif>Stiching</option>
                                    <option @if($data->category == 'finishing') {{"selected"}} @endif>Finishing</option>
                                    <option @if($data->category == 'packing') {{"selected"}} @endif>Packing</option>
                                    <option @if($data->category == 'helper') {{"selected"}} @endif>Helper</option>
                                 </select>
                                 @if ($errors->any() && $errors->has('category'))
                                 <span class="alert alert-danger">{{$errors->first('category')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->            
                           </div>
                           <div id="daily">
                              <div class="control-group">
                                 <label class="control-label" for="daily_payment">Daily Payment</label>
                                 <div class="controls">
                                    <input type="text" class="span4" id="daily_payment" name="daily_payment" placeholder="Enter Daily Payment" value="{{ old ('daily_payment') }}">
                                    @if ($errors->any() && $errors->has('daily_payment'))
                                    <span class="alert alert-danger">{{$errors->first('daily_payment')}}</span>
                                    @endif
                                 </div>
                                 <!-- /controls -->            
                              </div>                              
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="email">Email Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="email" name="email"  value="{{$data->email}}" placeholder="Enter Valid Email Address">
                                 @if ($errors->any() && $errors->has('email'))
                                 <span class="alert alert-danger">{{$errors->first('email')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="phone">Phone</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="phone" name="phone" value="{{$data->phone}}" placeholder="Enter phone Number" required="">
                                 @if ($errors->any() && $errors->has('phone'))
                                 <span class="alert alert-danger">{{$errors->first('phone')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="address">Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="address" name="address" value="{{$data->address}}" placeholder="Enter Address" required="">
                                 @if ($errors->any() && $errors->has('address'))
                                 <span class="alert alert-danger">{{$errors->first('address')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-success">Update</button> 
                              <a href="{{url('employee')}}" class="btn btn-default btn-sm">Cancel</a>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>            
           
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
@endif
@endsection