@extends('layout.app')
@section('content')
<div class="row"> 
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add Cutting Section</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/cutting/store')}}" method="POST">
                        @csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                {{ session('success') }}
                              </div>
                           @endif                          
                        <fieldset>
                           <div class="control-group span6">
                              <label class="control-label" for="firm_name">Select Party/Firm</label>
                              <div class="controls">
                                 <select class="span4" id="party_id" name="party_id" autofocus="true" onChange="getState(this.value);" required="true">
                                    <option value="" selected="true" disabled="true">Select Party/Firm Name</option>
                                    @foreach($partydata as $data)
                                       <option value="{{$data->id}}">{{$data->firm_name}}</option>                                    
                                    @endforeach
                                 </select>                                 
                              </div>
                              <!-- /controls -->            
                           </div>                           
                           <div class="control-group">
                              <label class="control-label" for="dno">Design No:</label>
                              <div class="controls">
                                 <select class="span4" id="dno" name="dno" autofocus="true" required="true" onChange="getColor(this.value);">
                                    <option value='' selected='true' disabled='true'>Select Design No.</option>
                                 </select> 
                                 <!-- <input type="text" class="span4" id="dno" name="dno" placeholder="Enter Your Design Number" required=""> -->
                              </div>
                              <!-- /controls -->            
                           </div>                          
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save</button> 
                              <button class="btn btn-default" type="reset">Reset</button>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                         <div class="span11">
                           <table class="table table-bordered">
                               <thead>
                                 <tr>
                                   <th style="text-align:center">Size Cutting</th>
                                   <th style="text-align:center">M</th>
                                   <th style="text-align:center">L</th>
                                   <th style="text-align:center">XL</th>
                                   <th style="text-align:center">2XL</th>
                                   <th style="text-align:center">3XL</th>
                                   <th style="text-align:center">Total (pcs.)</th>
                                 </tr>
                               </thead>                            
                               <tbody>
                                 <tr>
                                   <td style="text-align:center"><input type="text" readonly="true" name="sizename[]" class="span3 sizename" value="c1" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="M[]" class="span3 M" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="L[]" class="span3 L" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="XL[]" class="form-control span3 XL" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="TWOXL[]" class="form-control span3 TWOXL" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="THREEXL[]" class="form-control span3 THREEXL" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text"  readonly="" name="totalpic[]"  id="totalpic" class="form-control span3 totalpic" style="width: 45px;"></td>
                                 </tr>
                                 <tr>
                                   <td style="text-align:center"><input type="text" readonly="true" name="sizename[]" class="span3 sizename" value="c2" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="M[]" class="span3 M" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="L[]" class="span3 L" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="XL[]" class="form-control span3 XL" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="TWOXL[]" class="form-control span3 TWOXL" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="THREEXL[]" class="form-control span3 THREEXL" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" readonly="" name="totalpic[]"  id="totalpic" class="form-control span3 totalpic" style="width: 45px;"></td>
                                 </tr>
                                 <tr>
                                   <td style="text-align:center"><input type="text" readonly="true" name="sizename[]" class="span3 sizename" value="c3" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="M[]" class="span3 M" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="L[]" class="span3 L" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="XL[]" class="form-control span3 XL" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="TWOXL[]" class="form-control span3 TWOXL" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" name="THREEXL[]" class="form-control span3 THREEXL" style="width: 45px;"></td>
                                   <td style="text-align:center"><input type="text" readonly="" name="totalpic[]"  id="totalpic" class="form-control span3 totalpic" style="width: 45px;"></td>
                                 </tr>                                                    
                              </tbody>
                              <tfoot>
                                 <tr>
                                    <td style="text-align:center">AVG</td>
                                    <td style="text-align:center"><!-- <input type="text" name="Mavg[]" id="Mavg"  class="form-control span3 Mavg" style="width: 45px;"> --></td>
                                    <td style="text-align:center"><!-- <input type="text" name="Lavg[]" id="Lavg"  class="form-control span3 Lavg" style="width: 45px;"> --></td>
                                    <td style="text-align:center"><!-- <input type="text" name="XLavg[]" id="XLavg" class="form-control span3 XLavg" style="width: 45px;"> --></td>
                                    <td style="text-align:center"><!-- <input type="text" name="2XLavg[]" id="2XLavg" class="form-control span3 2XLavg" style="width: 45px;"> --></td>
                                    <td style="text-align:center"><!-- <input type="text" name="3XLavg[]" id="3XLavg" class="form-control span3 3XLavg" style="width: 45px;"> --></td>
                                    <td style="text-align:center"><!-- <input type="text" name="totalpicavg[]" id="totalpicavg" class="form-control span3 totalpicavg" style="width: 45px;"> --></td>
                                 </tr>
                                 <tr>
                                    <td style="text-align:center">Total</td>
                                    <td style="text-align:center" id="totalM"></td>
                                    <td style="text-align:center" id="totalL"></td>
                                    <td style="text-align:center" id="totalXL"></td>
                                    <td style="text-align:center" id="totalTWOXL"></td>
                                    <td style="text-align:center" id="totalTHREEXL"></td>
                                    <td style="text-align:center" id="Sumoftotal" class="Sumoftotal"></td>
                                 </tr>
                              </tfoot>
                           </table>
                        </div>
                     </form>
                  </div>                  
               </div>
            </div>   
           
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

function getState(val) {
  $.ajax({
  type: "GET",
  url: "view/"+val,
  success: function(data){
  //alert(data);
    $("#dno").html(data);
  }
  });
}
function getColor(val) {
  $.ajax({
  type: "GET",
  url: "viewstockdetails/"+val,
  success: function(data){
    alert(data);
    //$("#dno").html(data);
  }
  });
}
   $(document).on('keydown keyup',function(){
      var totalsum=0;
      $('.M').each(function(){
         var inputVal=$(this).val();
         if($.isNumeric(inputVal)){
            totalsum+=parseFloat(inputVal);
         }
      });
      $("#totalM").text(totalsum);
   });
   $(document).on('keydown keyup',function(){
      var totalsum=0;
      $('.L').each(function(){
         var inputVal=$(this).val();
         if($.isNumeric(inputVal)){
            totalsum+=parseFloat(inputVal);
         }
      });
      $("#totalL").text(totalsum);
   });
   $(document).on('keydown keyup',function(){
      var totalsum=0;
      $('.XL').each(function(){
         var inputVal=$(this).val();
         if($.isNumeric(inputVal)){
            totalsum+=parseFloat(inputVal);
         }
      });
      $("#totalXL").text(totalsum);
   });
   $(document).on('keydown keyup',function(){
      var totalsum=0;
      $('.TWOXL').each(function(){
         var inputVal=$(this).val();
         if($.isNumeric(inputVal)){
            totalsum+=parseFloat(inputVal);
         }
      });
      $("#totalTWOXL").text(totalsum);
   });
   $(document).on('keydown keyup',function(){
      var totalsum=0;
      $('.THREEXL').each(function(){
         var inputVal=$(this).val();
         if($.isNumeric(inputVal)){
            totalsum+=parseFloat(inputVal);
         }
      });
      $("#totalTHREEXL").text(totalsum);
   });
   $(document).on('keydown keyup',function(){
      var grandtotal=0;
      $('.totalpic').each(function(){
         var inputVal=$(this).val();
         if($.isNumeric(inputVal)){
            grandtotal+=parseFloat(inputVal);
         }
      });
      $("#Sumoftotal").text(grandtotal);
   });

   // $('tbody').delegate('.M,.L,.XL,.TWOXL,.THREEXL','keyup',function(){
   //    var tr=$(this).parent().parent();
   //    var total=0;
   //    var M=tr.find('.M').val();
   //    var L=tr.find('.L').val();
   //    var XL=tr.find('.XL').val();
   //    var TWOXL=tr.find('.TWOXL').val();
   //    var THREEXL=tr.find('.THREEXL').val();
   //    total=M+L+XL+TWOXL+THREEXL;
   //    tr.find('.totalpic').val(total);
   // });


   
</script>
@endsection