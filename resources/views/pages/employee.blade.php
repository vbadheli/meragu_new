@extends('layout.app')
@section('content')
<div class="row">	
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add New Employees</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/employee/store')}}" method="POST">
                     	@csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                {{ session('success') }}
                              </div>
                           @endif
                           @if(session('danger'))
                              <div class="alert alert-danger">
                                {{ session('danger') }}
                              </div>
                           @endif
                        <fieldset>                           
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="firstname">First Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="firstname" name="firstname" placeholder="Enter Your First Name" value="{{ old ('firstname') }}" required="" autofocus="">
                                 @if ($errors->any() && $errors->has('firstname'))
                                 <span class="alert alert-danger">{{$errors->first('firstname')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="lastname">Last Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="lastname" name="lastname" placeholder="Enter Your Last Name" value="{{ old ('lastname') }}" required="">
                                 @if ($errors->any() && $errors->has('lastname'))
                                 <span class="alert alert-danger">{{$errors->first('lastname')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="category">Category</label>
                              <div class="controls">
                                 <select class="span4" id="category" name="category" required="true">
                                    <option value="" selected="selected" disabled="true">Select category</option>
                                    <option value="cutting">Cutting</option>
                                    <option value="stiching">Stiching</option>
                                    <option value="finishing">Finishing</option>
                                    <option value="packing">Packing</option>
                                    <option value="helper">Helper</option>
                                 </select>
                                 @if ($errors->any() && $errors->has('category'))
                                 <span class="alert alert-danger">{{$errors->first('category')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->            
                           </div>
                           <div id="daily">
                              <div class="control-group">
                                 <label class="control-label" for="daily_payment">Daily Payment</label>
                                 <div class="controls">
                                    <input type="text" class="span4" id="daily_payment" name="daily_payment" placeholder="Enter Daily Payment" value="{{ old ('daily_payment') }}">
                                    @if ($errors->any() && $errors->has('daily_payment'))
                                    <span class="alert alert-danger">{{$errors->first('daily_payment')}}</span>
                                    @endif
                                 </div>
                                 <!-- /controls -->				
                              </div>                              
                           </div>
                           <div class="control-group">
                                 <label class="control-label" for="email">Email</label>
                                 <div class="controls">
                                    <input type="email" class="span4" id="email" name="email" placeholder="Enter Email ID" value="{{ old ('email') }}">
                                    @if ($errors->any() && $errors->has('email'))
                                    <span class="alert alert-danger">{{$errors->first('email')}}</span>
                                    @endif
                                 </div>
                                 <!-- /controls -->            
                              </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="phone">Phone</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="phone" name="phone" placeholder="Enter phone Number" value="{{ old ('phone') }}" required="">
                                 @if ($errors->any() && $errors->has('phone'))
                                 <span class="alert alert-danger">{{$errors->first('phone')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="address">Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="address" name="address" placeholder="Enter Address" value="{{ old ('address') }}" required="">
                                 @if ($errors->any() && $errors->has('address'))
                                 <span class="alert alert-danger">{{$errors->first('address')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save</button> 
                              <button class="btn btn-default" type="reset" id="reset">Reset</button>
                           </div>                           
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>    
            <div class="span11">
               <div class="widget-header">
                  <h3>Show Records</h3>
               </div>
               <!-- /widget-header -->
               <div class="widget-content">
                  <div class="card-body">
               <table class="table table-bordered" id="example">
                  <thead>
                     <tr>
                        <th>Sr.</th>                        
                        <th>Name</th>
                        <!-- <th>Email</th> -->
                        <th>Phone No.</th>
                        <!-- <th>Address</th> -->
                        <th>Department</th>
                        <!-- <th>Created At</th> -->
                        <th>Actions</th>
                     </tr>
                  </thead>
                  <?php  $SrNo = 1; ?>
                  <tbody>
                     @foreach($Employee as $data)                   
                     <tr>
                        <td>{{$SrNo++}}</td>
                        <td>{{$data->firstname}}  {{$data->lastname}}</td>
                        <!-- <td>{{$data->email}}</td> -->
                        <td>{{$data->phone}}</td>
                        <!-- <td>{{$data->address}}</td> -->
                        <td>{{$data->category}}</td>
                        <!-- <td>{{$data->created_at->format('d/m/Y H:i:s')}}</td>                          -->
                        <td>
                           <a href="{{url('employee/edit', $data['id'])}}" class="btn btn-warning btn-sm"><i class="icon-pencil"></i></a>
                           <form action="{{url('employee/destroy', $data['id'])}}" method="post">
                              @csrf
                              <input name="_method" type="hidden" value="DELETE">
                              <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')" type="submit"><i class="icon-remove"></i></button>
                           </form>
                        </td>                         
                     </tr>
                     @endforeach
                  </tbody>
               </table>             
            </div>

               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
    $('[data-toggle="tooltip"]').tooltip();
    $("#btnsave").click(function () {
     $("#btnsave").toggle();
  });
    $("#reset").click(function () {
     $("#btnsave").show();
  });
});
$(document).ready(function(){
   $('#daily').hide();
    $('#category').change(function() {
      if($('#category').val()=="helper")
      {
         $('#daily').show();
      }else
      {
         $('#daily').hide();
      }
    });
});
</script>
@endsection