@extends('layout.app')
@section('content')
<div class="row">	
   <div class="col-6">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add New Party</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/store')}}" method="POST">
                     	@csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                {{ session('success') }}
                              </div>
                           @endif
                           @if(session('danger'))
                              <div class="alert alert-danger">
                                {{ session('danger') }}
                              </div>
                           @endif
                           
                        <fieldset>
                           <div class="control-group">
                              <label class="control-label" for="firm_name">Firm name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="firm_name" placeholder="Enter Your firm_name" name="firm_name" required="true" autofocus="ture" value="{{old('firm_name')}}">
                                 @if ($errors->any() && $errors->has('firm_name'))
                                 <span class="alert alert-danger">{{$errors->first('firm_name')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="gst_no">GST NO.</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="gst" placeholder="Enter Your GST Number" name="gst" value="{{old('gst')}}">
                                 @if ($errors->any() && $errors->has('gst'))
                                 <span class="alert alert-danger">{{$errors->first('gst')}}</span>
                                 @endif
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="firstname">First Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="firstname" name="firstname" placeholder="Enter Your First Name" required="" value="{{old('firstname')}}">
                                 @if ($errors->any() && $errors->has('firstname'))
                                 <span class="alert alert-danger">{{$errors->first('firstname')}}</span>
                                 @endif
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="lastname">Last Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="lastname" name="lastname" placeholder="Enter Your Last Name" required="" value="{{old('lastname')}}">
                                 @if ($errors->any() && $errors->has('lastname'))
                                 <span class="alert alert-danger">{{$errors->first('lastname')}}</span>
                                 @endif
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="email">Email Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="email" name="email" placeholder="Enter Valid Email Address" value="{{old('email')}}">
                                 @if ($errors->any() && $errors->has('email'))
                                 <span class="alert alert-danger">{{$errors->first('email')}}</span>
                                 @endif
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="phone">Phone</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="phone" name="phone" placeholder="Enter phone Number" value="{{old('phone')}}" required="">
                                 @if ($errors->any() && $errors->has('phone'))
                                 <span class="alert alert-danger">{{$errors->first('phone')}}</span>
                                 @endif
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="address">Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="address" name="address" placeholder="Enter Address" value="{{old('address')}}" required="">
                                 @if ($errors->any() && $errors->has('address'))
                                 <span class="alert alert-danger">{{$errors->first('address')}}</span>
                                 @endif
                              </div>
                           </div>
                           <div class="">
                              <button type="submit" class="btn btn-primary" id="btnsave">Save</button>
                              <button class="btn btn-default" type="reset" id="reset">Reset</button>
                           </div>                            
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>    
         </div>
      </div>
   </div>

   <div class="col-6">
      <div class="widget-header">
         <h3>All Party</h3>
      </div>
      <!-- /widget-header -->
      <div class="widget-content">
         <div class="card-body">
      <table class="table table-bordered" id="example">
         <thead>
            <tr>
               <th>Sr.</th>                        
               <th>Firm Name</th>
               <th>Name</th>
               <th>Phone No.</th>
               <!-- <th>Email</th>
               <th>Address</th>
               <th>GST No.</th>
               <th>Created At</th> -->
               <th colspan="3">Actions</th>
               <!-- <th>Delete</th> -->
            </tr>
         </thead>
         <?php  $SrNo = 1; ?>
         <tbody>
            @foreach($partydata as $data)                   
            <tr>
               <td>{{$SrNo++}}</td>
               <!-- <td><a href="{{url('edit', $data['id'])}}" class="btn btn-link">{{$data->firm_name}}</a></td> -->
               <td>{{$data->firm_name}}</td>
               <td>{{$data->firstname}}  {{$data->lastname}}</td>
               <td>{{$data->phone}}</td>
               <!-- <td>{{$data->email}}</td>
               <td>{{$data->address}}</td>
               <td>{{$data->gst}}</td>
               <td>{{$data->created_at->format('d/m/Y H:i:s')}}</td> -->
               <td></td>
               <!-- <td>
                  <a href="{{route('invoice-home')}}?party_id={{$data['id']}}">View Invoices</a><br>
               </td>
               <td>
                  <a href="{{route('stockusage')}}?party={{$data['id']}}">View Stock</a><br>
               </td>
               <td>
                  <a href="{{route('payment-history',['id'=>$data['id']])}}">View Payment History</a></td>
               <td>
                  <!__<a href="{{url('edit', $data['id'])}}" class="btn btn-warning btn-sm"><i class="icon-pencil"></i></a>__>
                  <form action="{{url('destroy', $data['id'])}}" method="post">
                     @csrf
                     <input name="_method" type="hidden" value="DELETE">
                     <button class="btn btn-danger btn-sm"  onclick="return confirm('Are you sure?')" type="submit"><i class="icon-remove"></i></button>
                     </form>
               </td> -->
            </tr>
            @endforeach
         </tbody>
      </table>             
   </div>
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>

$(document).ready(function() {
   //  $('#example').DataTable( {
   //      dom: 'Bfrtip',
   //      buttons: [
   //          'copy', 'csv', 'excel', 'pdf', 'print'
   //      ]
   //  } );
  //   $('[data-toggle="tooltip"]').tooltip();
  //   $("#btnsave").click(function () {
  //    $("#btnsave").toggle();
  // });
  //   $("#reset").click(function () {
  //    $("#btnsave").show();
  // });
});
</script>


@endsection

