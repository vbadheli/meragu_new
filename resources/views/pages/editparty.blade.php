@extends('layout.app')
@section('content')
@if(isset($editdata))
<div class="row">	
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Edit Party</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/update',$editdata->id)}}" method="POST">
                     	@csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                {{ session('success') }}
                              </div>
                           @endif
                        <fieldset>
                           <div class="control-group">
                              <label class="control-label" for="firm_name">Firm name</label>
                              <div class="controls">
                                 <input type="text" class="span4" value="{{$editdata->firm_name}}" id="firm_name" placeholder="Enter Your firm_name" name="firm_name" required="true" autofocus="ture">
                                 @if ($errors->any() && $errors->has('firm_name'))
                                 <span class="alert alert-danger">{{$errors->first('firm_name')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="gst_no">GST NO.</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="gst" value="{{$editdata->gst}}" placeholder="Enter Your GST Number" name="gst">
                                 @if ($errors->any() && $errors->has('firm_name'))
                                 <span class="alert alert-danger">{{$errors->first('firm_name')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="firstname">First Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="firstname" value="{{$editdata->firstname}}" name="firstname" placeholder="Enter Your First Name" required="true">
                                 @if ($errors->any() && $errors->has('firstname'))
                                 <span class="alert alert-danger">{{$errors->first('firstname')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="lastname">Last Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="lastname" name="lastname" value="{{$editdata->lastname}}" placeholder="Enter Your Last Name" required="true">
                                 @if ($errors->any() && $errors->has('lastname'))
                                 <span class="alert alert-danger">{{$errors->first('lastname')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="email">Email Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="email" name="email"  value="{{$editdata->email}}" placeholder="Enter Valid Email Address">
                                 @if ($errors->any() && $errors->has('email'))
                                 <span class="alert alert-danger">{{$errors->first('email')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="phone">Phone</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="phone" name="phone" value="{{$editdata->phone}}" placeholder="Enter phone Number" required="true">
                                 @if ($errors->any() && $errors->has('phone'))
                                 <span class="alert alert-danger">{{$errors->first('phone')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="address">Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="address" name="address" value="{{$editdata->address}}" placeholder="Enter Address" required="true">
                                 @if ($errors->any() && $errors->has('address'))
                                 <span class="alert alert-danger">{{$errors->first('address')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-success">Update</button> 
                              <a href="{{url('party')}}" class="btn btn-default btn-sm">Cancel</a>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>            
           
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
@endif
@endsection