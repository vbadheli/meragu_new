@if(isset($workdata))
        <div class="tablshow">          
          <table class='table table-bordered'>        
                     <thead>
                        <tr>
                            <th>Employee Name</th>
                            <th>Party Name</th>
                            <th>Design No./Color/Cloth_name</th>                                                           
                            <th>Total Pices</th>                                                           
                            <th>Return Pices</th>                                                           
                            <th>remaining Pices</th>                                                           
                            <th>Total Rate</th>                                                           
                            <th>Assign Date</th>                                                           
                        </tr>                           
                    </thead>
                    <tbody>                     
                    @foreach ($workdata as $data)
                        <?php $returnedCount = StitchingReturn::where('employee_id',$data->employee_id)->where('stitching_id', $data->id )->sum('received_count');
                              $remainingCount = $data->count - $returnedCount;
                         ?>                     
                      <tr>
                          <td>{{$data->employee_id}} {{$data->employee->firstname}} {{$data->employee->lastname}}</td>                                
                          <td>{{$data->cuttingdetail->stockdetail->stock->party->firm_name}}</td>                                
                          <td>{{$data->cuttingdetail->stockdetail->stock->dno}} / {{$data->cuttingdetail->stockdetail->color}} / {{$data->cuttingdetail->stockdetail->stock->cloth_name}}</td>                                                       
                          <td>{{$data->count}}</td>                                                       
                          <td>{{$returnedCount}}</td>                                                       
                          <td>{{$remainingCount}}</td>                                                       
                          <td>{{$data->rate}}</td>                                                       
                          <td>{{date('d-m-Y', strtotime($data->duedate))}}</td>                                                       
                      </tr>
                    @endforeach
                  </tbody></table>
        </div>
      @endif