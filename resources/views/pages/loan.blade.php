@extends('layout.app')
@section('content')
<div class="row">	
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add Advance Amount</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/loan/store')}}" method="POST">
                     	@csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                {{ session('success') }}
                              </div>
                           @endif                           
                        <fieldset>
                           <div class="control-group span6">
                              <label class="control-label" for="employee">Employee Name:</label>
                              <div class="controls">
                                 <select class="span4" id="employee" name="employee"  required="true" autofocus="ture">
                                    <option value="" selected="true" disabled="true">Select Employee Name</option>
                                    @foreach($employee as $data)
                                    <option value="{{$data->id}}">{{$data->firstname}}  {{$data->lastname}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="amount">Advance Amount</label>
                              <div class="controls">
                                 <input type="number" class="span4" id="amount" placeholder="Enter Advance Amount"  required="true" name="amount">
                              </div>
                              <!-- /controls -->				
                           </div>                           
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save</button> 
                              <button class="btn btn-default" type="reset">Reset</button>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>    
               <div class="span11">
               <div class="widget-header">
                  <h3>Show Records</h3>
               </div>
               <!-- /widget-header -->
               <div class="widget-content">
                  <div class="card-body">
               <table class="table table-bordered" id="example">
                  <thead>
                     <tr>
                        <th>Sr.</th>                        
                        <th>Employee Name</th>                        
                        <th>Advance Amount</th>
                        <th>Date</th>
                        <th>Edit</th>
                        <th>Paid Amount</th>
                     </tr>
                  </thead>
                  <?php  $SrNo = 1; ?>
                  <tbody>
                     @foreach($advance as $data)                   
                     <tr>
                        <td>{{$SrNo++}}</td>
                        <td>{{$data->employee->firstname}}  {{$data->employee->lastname}}</td>                        
                        <td>{{$data->amount}}</td>                        
                        <td>{{$data->created_at->format('d/m/Y H:i:s')}}</td>                         
                        <td>
                           <a href="{{url('loan/edit', $data['id'])}}" class="btn btn-warning btn-sm"><i class="icon-pencil"></i></a>
                        <td>
                           <form action="{{url('loan/paid')}}" method="post">
                              @csrf
                              <input name="employee_id" type="hidden" value="{{$data->id}}">
                              <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')" type="submit"><i class="icon-remove"></i></button>
                            </form>
                        </td>                         
                     </tr>
                     @endforeach
                  </tbody>
               </table>             
            </div>
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
@endsection