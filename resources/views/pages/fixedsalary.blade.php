@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Fixed Payment Employee</h3>
         </div>
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-stitching" class="form-horizontal" action="{{url('/fixed/store')}}" method="POST">
                        @csrf
                        @if (Session::get('error'))
                        <div class="alert alert-danger">
                           {{ Session::get('error') }}
                        </div>
                        @endif
                        @if(session('success'))
                        <div class="alert alert-success">
                           {{ session('success') }}
                        </div>
                        @endif                          
                        <fieldset> 
                           <div class="control-group">
                              <label class="control-label" for="empid">Select Employee</label>
                              <div class="controls">
                                 <select class="span4" id="empid" name="empid" autofocus="true" required="true" onChange="getpayment(this.value);">
                                    <option value="">Select Employee</option>
                                    @foreach($employee as $data)
                                       <option value="{{$data->id}}">{{$data->firstname}} {{$data->lastname}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <!-- /controls -->            
                           </div> 
                           <div class="control-group">
                              <label class="control-label" for="days">Days</label>
                              <div class="controls">
                                 <input type='text' name='days'  id='days' readonly="true"  class='form-control span4' value='6'>
                              </div>
                           </div> 
                           <div class="control-group">
                              <label class="control-label" for="payment_amount">Daily Payment</label>
                              <div class="controls">
                                 <input type='text' name='payment_amount'  id='payment_amount'  class='form-control span4'>
                              </div>
                           </div> 
                           <div class="control-group">
                              <label class="control-label" for="attendance_days">No. of Present Days</label>
                              <div class="controls">
                                 <input type='text' name='attendance_days' id='attendance_days'  class='form-control span4'>
                              </div>
                           </div> 
                           <div class="control-group">
                              <label class="control-label" for="absent_days">No. of Absent Days</label>
                              <div class="controls">
                                 <input type='text' name='absent_days'   id='absent_days'  class='form-control span4'>
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="advance_cut">Advance Cutting</label>
                              <div class="controls">
                                 <input type='text' name='advance_cut'     id='advance_cut'  value="0"   class='form-control span4'>
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="total_amount">Payment amount</label>
                              <div class="controls">
                                 <input type='text' name='total_amount'    id='total_amount'  class='form-control span4'>
                              </div>
                           </div>                         
                           <br>
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary" id="btnsave">Save</button> 
                              <button class="btn btn-default" type="reset" id="reset" data-toggle="tooltip" title="Click me Open Save Button !..">Reset</button>
                           </div>                          
                        </fieldset>
                     </form>
                  </div>
                  <table id="example" class="tabel table-bordered" style="width:100%">
                        <thead>
                           <tr>
                              <th style="text-align:center">Sr.</th>
                              <th style="text-align:center">Employee Name</th>
                              <th style="text-align:center">Advance Cutting</th>
                              <th style="text-align:center">Pay Salary</th>
                              <!-- <th style="text-align:center">Action</th> -->
                           </tr>
                        </thead>
                        <?php  $SrNo = 1; ?>
                        <tbody>
                           @foreach($employee_payshit as $data)
                           <tr>
                              <td style="text-align:center">{{$SrNo++}}</td>
                              <td style="text-align:center">{{$data->employee->firstname}}  {{$data->employee->lastname}}</td>
                              @if($data->advance_cut==null)             
                              <td style="text-align:center">0 /-</td>
                              @else
                              <td style="text-align:center">{{$data->advance_cut}} /-</td>
                              @endif
                              <td style="text-align:center">{{$data->payable_amount}} /-</td>
                              <!-- <td>
                                 <a href="{{url('fixed_salary/edit', $data['id'])}}" class="btn btn-warning btn-sm"><i class="icon-pencil"></i></a>
                                 <form action="{{url('fixed_salary/destroy', $data['id'])}}" method="post">
                                    @csrf
                                    <input type="hidden" name="_token" value="DELETE"> 
                                    <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')" type="submit"><i class="icon-remove"></i></button>
                                 </form>
                              </td> -->
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
   $(document).on('change',function(){
          var totalattent=0;
          $('#days').each(function(){
             var days=$(this).val();
             var attendance_days=$('#attendance_days').val();
             var payment_amount=$('#payment_amount').val();             
             if($.isNumeric(days)){
                totalattent=parseInt(days)-parseInt(attendance_days);
             }
          });
          $("#absent_days").val(totalattent);
       });
   $(document).on('change',function(){
          var totalpayment=0;
          $('#attendance_days').each(function(){
            var attendance_days=$(this).val();
             var payment_amount=$('#payment_amount').val();             
             var advance_cut=$('#advance_cut').val();             
             if($.isNumeric(attendance_days)){
                totalpayment=parseInt(attendance_days)*parseInt(payment_amount)-parseInt(advance_cut);
             }
          });
           // alert(totalpayment);
          $("#total_amount").val(totalpayment);
       });
   function getpayment(empid) {
      $.ajax({
      type: "GET",
      url: "getpayment/"+empid,
      success: function(data){
         $("#payment_amount").val(data);
      }
      });
    }
    $(document).ready(function() {
     $('#example').DataTable({
       dom: 'Bfrtip',
       buttons: [
       {
         extend: 'copy',
         filename: 'payshit'
       }, {
         extend: 'csv',
         filename: 'payshit'
       },
       {
         extend: 'pdf',
         title: 'Employee Pay Shit Details',
         filename: 'payshit'
       }, {
         extend: 'excel',
         title: 'Employee Pay Shit Details',
         filename: 'payshit'
       },{
         extend: 'print',
         title: 'Employee Pay Shit Details',
         filename: 'payshit'
       }]
     });
   });
</script>
@endsection