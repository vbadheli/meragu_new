@extends('layout.app')
@section('content')
<div class="row">	
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Edit Advance Amount</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     @if($editdata)
                        <form id="edit-profile" class="form-horizontal" action="{{url('/loan/update',$editdata->id)}}" method="POST">
                        	@csrf
                           @if (Session::get('error'))
                              <div class="alert alert-danger">
                                 {{ Session::get('error') }}
                              </div>
                              @endif
                              @if(session('success'))
                                 <div class="alert alert-success">
                                   {{ session('success') }}
                                 </div>
                              @endif                           
                           <fieldset>
                              <div class="control-group span6">
                                 <label class="control-label" for="employee">Employee Name:</label>
                                 <div class="controls">
                                    <select class="span4" id="employee" name="employee"  required="true" autofocus="ture">
                                       <option value="" selected="true" disabled="true">Select Employee Name</option>
                                       @foreach($employee as $data)
                                       <option value="{{$data->id}}" @if ($editdata->employee_id == $data->id)selected="selected" @endif>{{$data->firstname}}  {{$data->lastname}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                              </div>
                              <div class="control-group">
                                 <label class="control-label" for="amount">Advance Amount</label>
                                 <div class="controls">
                                    <input type="text" class="span4" id="amount" value="{{$editdata->amount}}" placeholder="Enter Advance Amount" name="amount">
                                 </div>
                                 <!-- /controls -->				
                              </div>                           
                              <!-- /control-group -->
                              <div class="form-actions">
                                 <button type="submit" class="btn btn-success">Update</button> 
                                 <a href="{{url('loan')}}" class="btn btn-default btn-sm">Cancel</a>
                              </div>
                              <!-- /form-actions -->
                           </fieldset>
                        </form>
                      @endif
                  </div>                  
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
@endsection