@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Stock Usage</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     
                     <div class="control-group">
                        <label class="control-label" for="firm_name">Select Party/Firm</label>
                        <div class="controls">
                           <select class="span4" id="party_id" name="party_id" autofocus="true"  required="true">
                              <option value="">Select Party/Firm Name</option>
                              <option value="0" selected>All</option>
                              @foreach($partydata as $data)
                              <option value="{{$data->id}}" @if($partyid == $data->id) {{ "selected" }} @endif>{{$data->firm_name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>

                     <table id="tabdata" class="table table-bordered">
                        <?= view('stockpage.partial_stock_usage', compact('stock','partydata'));?>
                     </table>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">   
    $(document).ready(function(){

      $("#party_id").on("change",function(){
         var id = $(this).val();
         $.ajax({
         type: "GET",
         url: '{{URL::route("remaining-stock")}}',
         data: {party: id},
         success: function(data){
           $("#tabdata").html(data);
         }
       });
      });
   });
</script>
@endsection