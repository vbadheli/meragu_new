<tr>
   <th>Sr.no</th>
   <th>Party</th>
   <th>Invoice No.</th>
   <th>Date</th>
   <th>Remaining</th>
</tr>
<?php $i=1;?>

@foreach($allInvoices as $invoice)
   
   <tr>
      <td>{{$i++}}</td>
      <td>{{$invoice->party}}</td>
      <td>{{$invoice->invoice_number}}</td>
      <td>{{$invoice->created_at}}</td>
      <td>{{}}</td>
   </tr>
@endforeach
