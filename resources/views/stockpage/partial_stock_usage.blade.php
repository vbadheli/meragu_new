<tr>   
   <th>Stock Detail</th>
   <th>Total Mtrs</th>
   <th>Used</th>
   <th>Remaining</th>
</tr>
<?php $i=1;?>
@foreach($stock as $stk)

<tr style="background-color: lightgrey"><td colspan="5">
   <strong>Party: {{$stk->party->firm_name}} &nbsp;&nbsp;Dno.: {{$stk->dno}}  &nbsp;&nbsp;&nbsp;Cloth Name: {{$stk->cloth_name}} &nbsp;&nbsp;&nbsp; Panna: {{$stk->panna}}</strong></td></tr>
   @if(count($stk->stockdetails) > 0)

      @foreach($stk->stockdetails as $sd)
      <?php 
      $used = 0;
      if($sd->usage != null)
         $used = $sd->usage->used_mtrs;
      $remaining = $sd->length - $used;

      ?>
      <tr>
         <td><b>{{ $sd->color }}</b></td>
         <td><b>{{$sd->length}}</b></td>
         <td><b>{{$used}}</b></td>
         <td><b>{{$remaining}}</b></td>
      </tr>
      <!-- <tr>
      <td colspan="5"><strong>Color: {{$sd->color}} &nbsp;&nbsp;Meter: {{$sd->length}}</strong></td></tr> -->
      <?php $totalpieces = 0;?>
      @foreach ($sd->cuttingdetails as $key => $value)
      <?php $totalpieces += $value->count;?>

      <tr>
           <td>SIZE:- {{ $value->size->size }}</td> 
           <td>{{ $value->count }} pcs</td> 
           <td colspan="3"></td> 
      </tr>
      @endforeach

      <tr style="background-color: #aaa; color: #000">
           <td><strong>Total Pieces</strong></td> 
           <td><strong>{{ $totalpieces }} pcs</strong></td> 
           <td colspan="3"></td> 
      </tr>

      @endforeach
   @else
   <tr><td colspan="5">No stock found</td></tr>
   @endif
@endforeach