@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add Stock</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">

                     <form id="new-stock" name="new-stock" class="form-horizontal" action="{{url('/stock/store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if (Session::get('error'))
                        <div class="alert alert-danger">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                           {{ Session::get('error') }}
                        </div>
                        @endif
                        @if(session('success'))
                        <div class="alert alert-success">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                           {{ session('success') }}
                        </div>
                        @endif                       
                        <fieldset>
                           <div class="control-group span6">
                              <label class="control-label" for="firm_name">Select Party/Firm</label>
                              <div class="controls">
                                 <select class="span4" id="party" name="party" autofocus="true" required="true" >
                                    <option value="">Select Party/Firm Name</option>
                                    <option value="newparty">Add New Party</option>
                                    @foreach($partydata as $data)
                                    <option value="{{$data->id}}" @if(old('party') == $data->id){{ "selected" }}@endif>{{$data->firm_name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <!-- /controls -->            
                           </div>
                           <!-- <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#party_model">Add Party Name</a> -->
                           <div class="control-group">
                              <label class="control-label" for="date">Date</label>
                              <div class="controls">
                                 <input type="date" class="span4" id="date" name="date" value="<?php echo date('Y-m-d');?>">
                              </div>
                              <!-- /controls -->            
                           </div>
                           <!-- /control-group -->
                           <div class="control-group span6">
                              <label class="control-label" for="dno">Design No:</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="dno" name="dno" placeholder="Enter Your Design Number" required="" value="{{old('dno')}}">
                              </div>
                              <!-- /controls -->            
                           </div>

                           <div class="control-group">
                              <label class="control-label" for="panna">Panna/Width:</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="panna" name="panna" placeholder="Enter Panna/Width" value="{{old('panna')}}">
                              </div>
                              <!-- /controls -->            
                           </div>
                           <!-- /control-group -->
                           <div class="control-group span6">
                              <label class="control-label" for="challanno">Challan No.</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="challanno" name="challanno" placeholder="Enter Challan Number" value="{{old('challanno')}}">
                              </div>
                              <!-- /controls -->            
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="cloth_name">Cloth Brand Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="cloth_name" name="cloth_name" placeholder="Enter Cloth Brand Name" value="{{old('cloth_name')}}">
                              </div>
                              <!-- /controls -->            
                           </div>
                           <div class="control-group span6">
                              <label class="control-label" for="dno">Category Name:</label>
                              <div class="controls">
                                 <select class="span3" multiple id="category_name" name="category_name[]" required>
                                    <option value='' selected='true' disabled='true'>Select Category Name</option>
                                    @foreach($category as $cate)
                                       <option value="{{$cate->id}}">{{$cate->name}}</option>
                                    @endforeach
                                 </select>

                                 <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#category_model">Add Category Name.</a>
                                 @if($errors->has('category_name'))
                                 <span class="badge badge-danger">{{$errors->first('category_name')}}</span>
                                 @endif
                              </div>
                              <!-- /controls -->            
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="cloth_name">Sample Image</label>
                              <div class="controls">
                                 <input type="file" class="span4" id="sampleImage" name="sample_image">
                              </div>
                              <!-- /controls -->            
                           </div>
                           <br>
                           <!-- /control-group -->
                           
                           <!-- /form-actions -->
                        </fieldset>
                        <div class="">
                           <table id="sd_table" class="table table-bordered">
                              <thead>
                                 <tr>
                                    <th>Size/Mtr</th>
                                    <th>Color</th>
                                    <th>Photo</th>
                                    <th style="text-align:center"><a href="#" class="btn btn-success addRow"><i class="icon-plus-sign"></i></a></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td><input type="number"  placeholder="Enter Size Or Miter" name="sd[0][size]" class="span3  size" required="ture"></td>
                                    <td><input type="text" placeholder="Enter Color Name" name="sd[0][color]" class="span3 color" required="ture"></td>
                                    <td><input type="file" name="sd[0][photo]" class="form-control span3 photo"></td> <!-- sd_item -->
                                    <td><a href="#" class="btn btn-danger remove"><i class="icon-remove"></i></a></td>
                                 </tr>
                              </tbody>
                              <tfoot>
                                 <tr>
                                    <td id="total" colspan="4">Total</td>                                    
                                 </tr>
                              </tfoot>
                           </table>
                        </div>
                        <div class="form-actions">
                           <button type="submit" class="btn btn-primary" >Save</button> 
                           <button class="btn btn-default" type="reset" id="reset">Reset</button>
                        </div>                       
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
   // $(document).ready(function() {
   //    $('[data-toggle="tooltip"]').tooltip();
   //    $("#btnsave").click(function () {
   //       $("#btnsave").toggle();
   //    });
   //    $("#reset").click(function () {
   //       $("#btnsave").show();
   //    });
   // });
   var rowcount = 1;
   $(".addRow").on("click",function(){
      addRow();
   });

   
   function addRow()
   {
      console.log($("#sd_table tr").length)
      rowcount++;
      var addRow='<tr>'+
                  '<td><input type="number" placeholder="Enter Size Or Miter" name="sd['+rowcount+'][size]" class="span3 size"></td>'+
                  '<td><input type="text" placeholder="Enter Color Name" name="sd['+rowcount+'][color]" class="span3 color" ></td>'+
                  '<td><input type="file" name="sd['+rowcount+'][photo]" class="form-control span3 photo"></td>'+
                  '<td><a href="#" class="btn btn-danger remove"><i class="icon-remove"></i></a></td>'+
                  '</tr> ';
      $('tbody').append(addRow);
   }
   $("#sd_table").delegate(".sd_item","change",function(){
      addRow();
      console.log($('td').parent().next());       
   });     
   // function total()
   // {
   //    var total=0;
   //    $('.size').each(function(i,e){
   //       var size=$(this).val()-0;
   //       total+=size;
   //    })
   //    $('#total').text(total);
   // }
   $(document).on('keydown keyup',function(){
      var totalsum=0;
      $('.size').each(function(){
         var inputVal=$(this).val();
         if($.isNumeric(inputVal)){
            totalsum+=parseFloat(inputVal);
         }
      });
      $("#total").text(totalsum);
   });
   $('body').delegate('.remove','click',function(){
      var l=$('tbody tr').length;
      if(l==1)
      {
         alert("You can not remove last one");
      }else
         $(this).parent().parent().remove();
   });  
</script>
<!-- This is Party Model Start -->
<div class="modal fade" id="party_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <center>
               <h5 class="modal-title" id="exampleModalLongTitle">Add New Party</h5>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form id="new-party-from-new-stock" name="new-party-from-new-stock" class="form-horizontal" action="{{url('/store-ajax')}}" method="POST">
               @csrf
               @if (Session::get('error'))
               <div class="alert alert-danger">
                  {{ Session::get('error') }}
               </div>
               @endif
               @if(session('success'))
               <div class="alert alert-success">
                  {{ session('success') }}
               </div>
               @endif
               @if(session('danger'))
               <div class="alert alert-danger">
                  {{ session('danger') }}
               </div>
               @endif
               <fieldset>
                  <div class="control-group">
                     <label class="control-label" for="firm_name">Firm name</label>
                     <div class="controls">
                        <input type="text" class="span2" id="firm_name" placeholder="Enter Your firm_name" name="firm_name" required="true" autofocus="true" value="test firm">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="gst_no">GST NO.</label>
                     <div class="controls">
                        <input type="text" class="span2" id="gst" placeholder="Enter Your GST Number" name="gst" value="10021012">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="firstname">First Name</label>
                     <div class="controls">
                        <input type="text" class="span2" id="firstname" name="firstname" placeholder="Enter Your First Name" value="viny">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="lastname">Last Name</label>
                     <div class="controls">
                        <input type="text" class="span2" id="lastname" name="lastname" placeholder="Enter Your Last Name" value="tes">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="email">Email Address</label>
                     <div class="controls">
                        <input type="text" class="span2" id="email" name="email" placeholder="Enter Valid Email Address" value="test@firm.com">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="phone">Phone</label>
                     <div class="controls">
                        <input type="text" class="span2" id="phone" name="phone" placeholder="Enter phone Number" required="true" value="9970707012">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="address">Address</label>
                     <div class="controls">
                        <input type="text" class="span2" id="address" name="address" placeholder="Enter Address" value="solapur">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <!-- <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Save</button> 
                     <button class="btn btn-default" type="reset">Reset</button>
                     </div> -->
                  <!-- /form-actions -->
                  <div class="modal-footer">
                     <button id="new-party-from-new-stock-submit" type="button" class="btn btn-primary">Save</button>
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
               </fieldset>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- This is category model start -->
<div class="modal fade" id="category_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <center>
               <h5 class="modal-title" id="exampleModalLongTitle">Add New Category</h5>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form id="edit-profile" class="form-horizontal" action="{{url('/category/create')}}" method="POST">
               @csrf
               @if (Session::get('error'))
                  <div class="alert alert-danger">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                     {{ Session::get('error') }}
                  </div>
                  @endif
                  @if(session('success'))
                     <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       {{ session('success') }}
                     </div>
                  @endif                           
               <fieldset>
                  <div class="control-group">
                     <label class="control-label" for="category_name">Category Name</label>
                     <div class="controls">
                        <input type="text" class="span3" id="name" placeholder="Enter Your Category Name" name="name" required="true" autofocus="ture">
                     </div>
                     <!-- /controls -->            
                  </div>                           
                  <!-- /control-group -->
                  <div class="form-actions">
                     <button type="submit" class="btn btn-primary" id="btnsave">Save</button> 
                     <button class="btn btn-default" type="reset" id="reset" data-toggle="tooltip" title="Click me Open Save Button !..">Reset</button>
                  </div>
                  <!-- /form-actions -->
               </fieldset>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section("script")

<script>
   $(document).ready(function(){

      $("#party").on("change", function(){
         selectedValue = $(this).val();
         if(selectedValue == "newparty")
         {
            $("#party_model").modal('show');
            $(this).val("");
         }
      });


      $("#new-party-from-new-stock-submit").on("click", function(){

         // e.preventDefault();
         var data = $("#new-party-from-new-stock").serializeArray();
         var url = $("#new-party-from-new-stock").attr("action");
         $.ajax({
            method: "POST",
            url: url,
            data: data,
         })
         .done(function( data ) {
            console.log( "Sample of data:", data);
            var d = JSON.parse(data);
            status = d.status;
            result = d.data.id;

            if(status == 1)
            {
               $('#party').append('<option value="'+d.data.id+'">'+d.data.firm_name+'</option>');
               $("#party").val(result);
               $("#party_model").modal('hide');

            }

         });         
      });
   })
</script>

@endsection