@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Employee Details <?php echo $employee_details[0]->firstname .'' .$employee_details[0]->lastname ?> </h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">                    
                     <table id="tabdata" class="table table-bordered">                     	
                        <tr>
						   <th>Sr.no</th>
						   <th>Name</th>
						   <th>Email</th>
						   <th>Phone</th>
						   <th>Address</th>
						   <th>Department</th>
						   <th>Status</th>
						   <th>Date</th>
						</tr>
						<?php $i=1;?>						
						@if(count($employee_details)>0)
						@foreach($employee_details as $data)
						   @if($data->active_status==1)
						   	<?php $status="Active"; ?>
						   @else
						   	<?php $status="Deactive"; ?>
						   @endif
						   <tr>
						      <td>{{$i++}}</td>
						      <td>{{$data->firstname}} {{$data->lastname}}</td>
						      <td>{{$data->email}}</td>
						      <td> {{$data->phone}}</td>
						      <td> {{$data->address}}</td>
						      <td> {{$data->category}}</td>
						      <td> {{$status}}</td>
						      <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y')}}</td>						      
						   </tr>
						@endforeach
						 @else 
						<tr>
						   <td colspan="5">No Records Found</td>
						</tr>
						@endif                       
                     </table>
                  </div>
                  <h3>Current Working Details </h3> 
                  <div class="tab-pane active" id="formcontrols">                    
                     <table id="tabdata" class="table table-bordered">                     	
                        <tr>
						   <th>Sr.no</th>
						   <th>Party Name</th>
						   <th>Description</th>
						   <th>working Pics</th>
						   <th>Due Date</th>
						</tr>
						<?php $i=1;$ttlpic=0;?>						
						@if(count($stichassign)>0)
						@foreach($stichassign as $stichdata)
							<?php $ttlpic+=	$stichdata->count; ?>
						   <tr>
						      <td>{{$i++}}</td>
						      <td>{{$stichdata->cuttingdetail->stockdetail->stock->party->firm_name}}</td>
						      <td>{{$stichdata->cuttingdetail->stockdetail->stock->dno}} / {{$stichdata->cuttingdetail->stockdetail->color}} / {{$stichdata->cuttingdetail->stockdetail->stock->cloth_name}}</td>
						      <td>{{$stichdata->count}} pics</td>
						      <td>{{ \Carbon\Carbon::parse($stichdata->duedate)->format('d/m/Y')}}</td>						      
						   </tr>
						@endforeach
						<tr>
							<td>Total</td><td></td><td></td><td>{{$ttlpic}} pics</td><td></td>
						</tr>
						 @else 
						<tr>
						   <td colspan="5">No Records Found</td>
						</tr>
						@endif                       
                     </table>
                  </div> 
                  <h3>Advance Amount </h3> 
                  <div class="tab-pane active" id="formcontrols">                    
                     <table id="tabdata" class="table table-bordered">                     	
                        <tr>
						   <th>Sr.no</th>
						   <th>Amount</th>
						   <th>Cutting Amount</th>
						   <th>Remaining Amount</th>			   
						   <th>Date</th>
						</tr>
						<?php $i=1; 
							$remainingamount=0;
							$totalcutamt=0;
							$totalremingamt=0;
							$totalamt=0;
						?>						
						@if(count($advance)>0)
						@foreach($advance as $adv)
						<?php 
							$remainingamount=$adv->amount-$cuttingadv; 
							$totalcutamt+=$cuttingadv; 
							$totalremingamt+=$remainingamount; 
							$totalamt+=$adv->amount; 
						?>													  
						   <tr>
						      <td>{{$i++}}</td>
						      <td>Rs. {{$adv->amount}}</td>
						      <td>Rs. {{$cuttingadv}}</td>
						      <td>Rs. {{$remainingamount}}</td>				      
						      <td>{{ \Carbon\Carbon::parse($adv->created_at)->format('d/m/Y')}}</td>						      
						   </tr>
						@endforeach
						<tr>
							<td>Total</td><td>Rs. {{$totalamt}}</td><td>Rs. {{$totalcutamt}}</td><td>Rs. {{$totalremingamt}}</td><td></td>
						</tr>
						 @else 
						<tr>
						   <td colspan="5">No Records Found</td>
						</tr>
						@endif                       
                     </table>
                  </div> 

                  <h3>Employee Payment Details </h3> 
                  <div class="tab-pane active" id="formcontrols">                    
                     <table id="tabdata" class="table table-bordered">                     	
                        <tr>
						   <th>Sr.no</th>
						   <th>Pay Slip Number</th>
						   <th>Total No. Pics</th>
						   <th>Amount</th>			   
						   <th>ESI</th>			   
						   <th>PF</th>			   
						   <th>Total Amount</th>			   
						   <th>Pay Amount</th>			   
						   <th>Advance Cutting Amount</th>			   
						   <th>Date</th>
						</tr>
						<?php $i=1;							
							$totalcalamt=0;
							$totalesi=0;
							$totalpf=0;
							$totalaca=0;
							$totalamtepd=0;
							$totalpay=0;
							$advc=0;
						?>						
						@if(count($EmployeePayment)>0)
						@foreach($EmployeePayment as $emppay)
						<?php
							$totalcalamt+=$emppay->cal_amount;
							$totalesi+=$emppay->esi;
							$totalpf+=$emppay->pf;
							$totalaca+=$emppay->advance_cut;
							$totalamtepd+=$emppay->totalamount;
							$totalpay+=$emppay->payable_amount;
						?>	
						   <tr>
						      <td>{{$i++}}</td>
						      <td>{{$emppay->paymentsheet_id}}</td>						      			      
						      <td>{{$emppay->total_pics}}</td>						      			      
						      <td>Rs. {{$emppay->cal_amount}}</td>						      			      
						      <td>Rs. {{$emppay->esi}}</td>						      			      
						      <td>Rs. {{$emppay->pf}}</td>
						      <td>Rs. {{$emppay->totalamount}}</td>						      			      
						      <td>Rs. {{$emppay->payable_amount}}</td>	
						      @if($emppay->advance_cut==null)
								<td>Rs. {{$advc}}</td>
							  @else					      			      
						      	<td>Rs. {{$emppay->advance_cut}}</td>						      			      
						      @endif					      			      
						      <td>{{ \Carbon\Carbon::parse($emppay->created_at)->format('d/m/Y')}}</td>						      
						   </tr>
						@endforeach
						<tr>
							<td>Total</td><td></td><td></td><td>Rs. {{$totalcalamt}}</td><td>Rs. {{$totalesi}}</td>
							<td>Rs. {{$totalpf}}</td><td>Rs. {{$totalamtepd}}</td><td>Rs. {{$totalpay}}</td><td>Rs. {{$totalaca}}</td><td></td>
						</tr>
						 @else 
						<tr>
						   <td colspan="5">No Records Found</td>
						</tr>
						@endif                       
                     </table>
                  </div>                  
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

@endsection