@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Employee Pay Shit Details</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                    <center>
                        <div class="control-group">
                           <form method="post" action="{{url('report/employee/pay_shit')}}">
                              @csrf
                              <input type="text" name="daterange" id="daterange" class="form-control" value="" style="height: 40px;" />
                              <input type="submit" value="Submit" id="btnsubmit" class="btn btn-success">
                           </form>
                        </div>
                     </center>
                     @if(isset($employee_payshit))
                     <table id="example" class="tabel table-bordered" style="width:100%">
                        <thead>
                           <tr>
                              <th style="text-align:center">Sr.</th>
                              <th style="text-align:center">Employee Name</th>
                              <th style="text-align:center">Pices</th>
                              <th style="text-align:center">Amount</th>
                              <th style="text-align:center">ESI</th>
                              <th style="text-align:center">PF</th>
                              <th style="text-align:center">Advance Cutting</th>
                              <th style="text-align:center">Total Amount</th>
                              <th style="text-align:center">Pay Salary</th>
                              <th style="text-align:center">Created</th>
                           </tr>
                        </thead>
                        <?php  $SrNo = 1; ?>
                        <tbody>
                           @foreach($employee_payshit as $data)
                           <tr>
                              <td style="text-align:center">{{$SrNo++}}</td>
                              <td style="text-align:center">{{$data->employee->firstname}}  {{$data->employee->lastname}}</td>
                              <td style="text-align:center">{{$data->total_pics}} </td>
                              <td style="text-align:center">{{$data->cal_amount}} /-</td>
                              <td style="text-align:center">{{$data->esi}} /-</td>
                              <td style="text-align:center">{{$data->pf}} /-</td>
                              @if($data->advance_cut==null)             
                              <td style="text-align:center">0 /-</td>
                              @else
                              <td style="text-align:center">{{$data->advance_cut}} /-</td>
                              @endif
                              <td style="text-align:center">{{$data->totalamount}} /-</td>
                              <td style="text-align:center">{{$data->payable_amount}} /-</td>
                              <td style="text-align:center">{{date('d-m-Y', strtotime($data->created_at))}}</td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     @endif
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
   $(document).ready(function() {
     $('#example').DataTable({
       dom: 'Bfrtip',
       buttons: [
       {
         extend: 'copy',
         filename: 'payshit'
       }, {
         extend: 'csv',
         filename: 'payshit'
       },
       {
         extend: 'pdf',
         title: 'Employee Pay Shit Details',
         filename: 'payshit'
       }, {
         extend: 'excel',
         title: 'Employee Pay Shit Details',
         filename: 'payshit'
       },{
         extend: 'print',
         title: 'Employee Pay Shit Details',
         filename: 'payshit'
       }]
     });
   });
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'left'
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
   });
</script>
@endsection