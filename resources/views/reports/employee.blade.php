@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Employee Details</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <table id="example" class="table table-bordered" style="width:100%">
                        <thead>
                           <tr>
                              <th>Sr.</th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Mobile No</th>
                              <th>Address</th>
                              <th>Created</th>
                           </tr>
                        </thead>
                        <?php  $SrNo = 1; ?>
                        <tbody>
                           @foreach($employeeshow as $data)
                           <tr>
                              <td>{{$SrNo++}}</td>
                              <td><a href="{{route('employee-details',['id'=>$data['id']])}}">{{$data->firstname}}  {{$data->lastname}}</a></td>
                              <td>{{$data->email}}</td>
                              <td>{{$data->phone}}</td>
                              <td>{{$data->address}}</td>
                              <td>{{date('d-m-Y', strtotime($data->created_at))}}</td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
   $(document).ready(function() {
     $('#example').DataTable({
       dom: 'Bfrtip',
       buttons: [
       {
         extend: 'copy',
         filename: 'employee'
       }, {
         extend: 'csv',
         filename: 'employee'
       },
       {
         extend: 'pdf',
         title: 'Employee Details',
         filename: 'employee'
       }, {
         extend: 'excel',
         title: 'Employee Details',
         filename: 'employee'
       },{
         extend: 'print',
         title: 'Employee Details',
         filename: 'employee'
       }]
     });
   });
</script>
@endsection