@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Finishing Details</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                <center><h3>Finishing Assign Details </h3></center><br>
                  <div class="tab-pane active" id="formcontrols">
                     <table id="example" class="table table-bordered" style="width:100%">
                        <thead>
                           <tr>
                              <th>Sr.</th>
                              <th class="hidden">id</th>
                              <th>Employee</th>
                              <th>Description</th>
                              <th>Count</th>
                              <th>Rate</th>
                              <th>Date</th>
                           </tr>
                        </thead>
                        <?php  $SrNo = 1; ?>
                        <tbody>
                          @if(count($finishing)>0)
                           @foreach($finishing as $data)
                           <tr>
                              <td>{{$SrNo++}}</td>
                              <td class="hidden">{{$data->id}}</td>
                              <td>{{$data->employee->firstname}} {{$data->employee->lastname}}</td>
                              <td>{{$data->stitcreturn->stitcAss->cuttingdetail->stockdetail->stock->dno}} / {{$data->stitcreturn->stitcAss->cuttingdetail->stockdetail->color}} / {{$data->stitcreturn->stitcAss->cuttingdetail->stockdetail->length}} mts </td>
                              <td>{{$data->count}}</td>
                              <td>{{$data->rate}}</td>
                              <td>{{ \Carbon\Carbon::parse($data->due_date)->format('d/m/Y')}}</td>
                           </tr>
                           @endforeach
                           @else 
                            <tr>
                               <td colspan="5">No Records Found</td>
                            </tr>
                            @endif 
                        </tbody>
                     </table>
                  </div>
                  <center><h3>Finishing Return Details </h3></center><br>
                  <div class="tab-pane active" id="formcontrols">
                     <table id="example1" class="table table-bordered">
                        <tr>
                           <th>Sr.no</th>
                           <th>Employee</th>
                           <th>Description</th>
                           <th>Count</th>
                           <th>Date</th>
                        </tr>
                        <?php $i=1;$ttlpic=0;?>           
                        @if(count($finishing_return)>0)
                        @foreach($finishing_return as $finish)
                        <tr>
                           <td>{{$i++}}</td>
                           <td>{{$finish->employee->firstname}} {{$finish->employee->lastname}}</td>
                           <td>{{$finish->finishing->stitcreturn->stitcAss->cuttingdetail->stockdetail->stock->dno}} / {{$finish->finishing->stitcreturn->stitcAss->cuttingdetail->stockdetail->color}} / {{$finish->finishing->stitcreturn->stitcAss->cuttingdetail->stockdetail->length}} mts</td>
                           <td>{{$finish->received_count}} pics</td>
                           <td>{{ \Carbon\Carbon::parse($finish->received_date)->format('d/m/Y')}}</td>
                        </tr>
                        @endforeach                        
                        @else 
                        <tr>
                           <td colspan="5">No Records Found</td>
                        </tr>
                        @endif                       
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
   $(document).ready(function() {
     $('#example').DataTable({
       dom: 'Bfrtip',
       buttons: [
       {
         extend: 'copy',
         filename: 'Finishing'
       }, {
         extend: 'csv',
         filename: 'Finishing'
       },
       {
         extend: 'pdf',
         title: 'Finishing Details',
         filename: 'Finishing'
       }, {
         extend: 'excel',
         title: 'Finishing Details',
         filename: 'Finishing'
       },{
         extend: 'print',
         title: 'Finishing Details',
         filename: 'Finishing'
       }]
     });
   });
</script>
@endsection