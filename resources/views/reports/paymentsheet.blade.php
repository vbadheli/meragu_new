@extends('layout.app')
<style>
   table {
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
   }
   td, th {
   border: 1px solid #dddddd;
   text-align: left;
   padding: 8px;
   }
   tr:nth-child(even) {
   background-color: #dddddd;
   }
</style>
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Payment Sheet</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     @if(session('success'))
                     <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ session('success') }}
                     </div>
                     @endif                     
                     <center>
                        <div class="control-group">
                           <label class="control-label" for="firm_name">Select Employee Name</label>
                           <div class="controls">
                              <select class="span4" id="employee_id" name="employee_id" autofocus="true" required="true" onChange="getEmployee(this.value);">
                                 <option value="" selected="true" disabled="true">Select Employee Name</option>
                                 @foreach($employeedata as $data)
                                 <option value="{{$data->id}}">{{$data->firstname}} {{$data->lastname}}</option>
                                 @endforeach
                              </select>
                           </div>
                           <!-- /controls -->            
                        </div>
                     </center>
                     <div class="card">
                        <div class="card-body" >
                           <form class='form-horizontal' action="{{ url('report/employee/store') }}" method='POST'>
                              @csrf
                              <table class='table table-bordered'>
                                 <thead>
                                    <tr>
                                       <th>Date</th>
                                       <th>Party Name</th>
                                       <th>Design No.</th>
                                       <th>Particular</th>
                                       <th>Pices</th>
                                       <th>Rate</th>
                                       <th>Total Amount</th>
                                    </tr>
                                 </thead>
                                 <tbody id="tblshow">                                             
                                 </tbody>
                                 <tfoot id="tfoot">                    
                                 </tfoot>
                              </table>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
   $("#tfoot").hide();
    $("#tblshow").hide();
   function getEmployee() {
     var employeeid = $("#employee_id").val();
     $.ajax({
     type: "GET",
     url: "paymentdata/"+employeeid,
     success: function(data){
        // alert(data);
        $("#tblshow").show();
        $("#tfoot").show();
       $("#tblshow").html(data);
     }
    });
   }
</script>
@endsection