@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Weekly Payment</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <center>
                        <div class="control-group">
                           <form method="post" action="{{url('report/employee/employeepayment')}}">
                              @csrf
                              <input type="text" name="daterange" id="daterange" class="form-control" value="" style="height: 40px;" />
                              <input type="submit" value="Submit" id="btnsubmit" class="btn btn-success">
                           </form>
                        </div>
                     </center>
                  </div>
               </div>
               <br><br>
               @if(isset($stitchingreturn))
               <div class="tablshow">
                  <h2>
                     <center>Weekly Payment Sheet </center>
                  </h2>
                  <br>
                  <table class='table table-bordered'>
                     <thead>
                        <tr>
                           <th>Employee Name</th>
                           <th>Total Pices</th>
                           <th>Total Rate</th>
                           <th>Total Amount</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php 
                           $totalamount=0; 
                           $sumofpices=0;
                           $sumofamount=0;?>
                        @if(count($stitchingreturn)>0)
                        @foreach ($stitchingreturn as $stitch)
                        <?php 
                           $totalamount=$stitch->received_count*$stitch->stitcAss->rate; 
                           ?>
                        <tr>
                           <td>{{$stitch->employee->firstname}} {{$stitch->employee->lastname}}</td>
                           <td>{{$stitch->received_count}}</td>
                           <td>{{$stitch->stitcAss->rate}}</td>
                           <td>{{$totalamount}}</td>
                           <?php $sumofpices+=$stitch->received_count;
                              $sumofamount+=$totalamount;
                              ?>                                                   
                        </tr>
                        @endforeach
                        <?php  $converamount='Rs.'.number_format($sumofamount, 2, '.', ',');?>
                        <tr>
                           <td>Total</td>
                           <td>{{$sumofpices}} Pices</td>
                           <td></td>
                           <td> {{$converamount}} </td>
                        </tr>
                        @else 
                        <tr>
                           <td colspan="5">No Records Found</td>
                        </tr>
                        @endif
                     </tbody>
                  </table>
               </div>
               @endif
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'left'
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
   });
   // $("#btnsubmit").click(function(){
   //     var daterange=$('#daterange').val();
   //     alert(daterange);
   //       $.ajax({
   //       type: "GET",
   //       url: "getemployeepayment",
   //       data: {
   //         "_token": "{{ csrf_token() }}",
   //         "daterange": daterange
   //         },
   //       success: function(data){
   //         //alert(data);
   //         $("table").show();
   //         $(".tablshow").html(data);
   //       }
   //       });
   //     });     
</script>
@endsection