@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Party Details</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <table id="example" class="table table-bordered" style="width:100%">
                        <thead>
                           <tr>
                              <th>Sr.</th>
                              <th>Firm Name</th>
                              <th>GST No</th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Mobile No</th>
                              <th>Address</th>
                              <th>Created</th>
                           </tr>
                        </thead>
                        <?php  $SrNo = 1; ?>
                        <tbody>
                           @foreach($partyshow as $data)
                           <tr>
                              <td>{{$SrNo++}}</td>
                              <td><a href="{{route('party-details',['id'=>$data['id']])}}">{{$data->firm_name}}</a></td>
                              <td>{{$data->gst}}</td>
                              <td>{{$data->firstname}}  {{$data->lastname}}</td>
                              <td>{{$data->email}}</td>
                              <td>{{$data->phone}}</td>
                              <td>{{$data->address}}</td>
                              <td>{{date('d-m-Y', strtotime($data->created_at))}}</td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
   // $(document).ready(function() {
   //     $('#example').DataTable( {
   //         dom: 'Bfrtip',
   //         buttons: [
   //             'copy', 'csv', 'excel', 'pdf', 'print'
   //         ]
   //     } );
   // } );
   $(document).ready(function() {
     $('#example').DataTable({
       dom: 'Bfrtip',
       buttons: [
       {
         extend: 'copy',
         filename: 'party'
       }, {
         extend: 'csv',
         filename: 'party'
       },
       {
         extend: 'pdf',
         title: 'Party Details',
         filename: 'party'
       }, {
         extend: 'excel',
         title: 'Party Details',
         filename: 'party'
       },{
         extend: 'print',
         title: 'Party Details',
         filename: 'party'
       }]
     });
   });
   
</script>
@endsection