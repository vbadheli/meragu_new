@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Stock Details</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <table id="example" class="table table-bordered" style="width:100%">
                        <thead>
                           <tr>
                              <th>Sr.</th>
                              <th class="hidden">id</th>
                              <th>Party ID</th>
                              <th>Design No</th>
                              <th>Challan No</th>
                              <th>Cloth Brand Name</th>
                              <th>Date</th>
                           </tr>
                        </thead>
                        <?php  $SrNo = 1; ?>
                        <tbody>
                           @foreach($sdata as $data)
                           <tr>
                              <td>{{$SrNo++}}</td>
                              <td class="hidden">{{$data->id}}</td>
                              <td><a hre="#">{{$data->party->firm_name}}</a></td>
                              <td><a hre="#" class="open-my-modal" data-target="#exampleModal" data-id="{{$data->id}}" data-toggle="modal" >{{$data->dno}}</a></td>
                              <td>{{$data->challan_no}}</td>
                              <td>{{$data->cloth_name}}</td>
                              <td>{{date('d-m-Y', strtotime($data->date))}}</td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
   $(document).ready(function() {
     $('#example').DataTable({
       dom: 'Bfrtip',
       buttons: [
       {
         extend: 'copy',
         filename: 'stock'
       }, {
         extend: 'csv',
         filename: 'stock'
       },
       {
         extend: 'pdf',
         title: 'Stock Details',
         filename: 'stock'
       }, {
         extend: 'excel',
         title: 'Stock Details',
         filename: 'stock'
       },{
         extend: 'print',
         title: 'Stock Details',
         filename: 'stock'
       }]
     });
   });
   $(document).ready(function () {             
       $('.open-my-modal').click(function(){
        var stock_id=$(this).data('id');
           $.ajax({
                 type: "get",
                 url: "stock/detail/"+stock_id,             
                 success : function(stockdata)
                {
                   $('.fetched-data').html(stockdata);
                }
             });
       });
   });
</script>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <center>
               <h5 class="modal-title" id="exampleModalLabel stock_id">Design Number Details</h5>
            </center>
            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button> -->
         </div>
         <div class="modal-body" id="stock_id">
            <div class="fetched-data"></div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
         </div>
      </div>
   </div>
</div>
@endsection