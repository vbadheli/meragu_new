@extends('layout.app')
<style>
table, th, td {
    border: 1px solid black;
}
</style>
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<h2><center>{{ __('Stitching Assign Details') }} </center></h2>
				<div class="card-body">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr>
								<th>Sr.</th>
								<th>Garment Name</th>
								<th>Design No/color</th>
								<th>Employee</th>
								<th>Count</th>
								<th>Rate</th>
								<th>Date</th>								
							</tr>							
						</thead>
						<?php  $SrNo = 1; ?>
						<tbody>
							@foreach($stitchingassign as $data)               
							<tr>
								<td>{{$SrNo++}}</td>
								<td>{{$data->CuttingDetail->stockdetail->stock->party->firm_name}}</td>
								<td>{{$data->CuttingDetail->stockdetail->stock->dno}} / {{$data->CuttingDetail->stockdetail->color}}</td>
								<td>{{$data->employee->firstname}}  {{$data->employee->lastname}}</td>				
								<td>{{$data->count}}</td>
								<td>{{$data->rate}}</td>
								<td>{{date('d-m-Y', strtotime($data->duedate))}}</td>								
							</tr>
							@endforeach
						</tbody>						
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
  $('#example').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'copy',
      filename: 'Stitching_Assign'
    }, {
      extend: 'csv',
      filename: 'Stitching_Assign'
    },
    {
      extend: 'pdf',
      title: 'Stitching_Assign',
      filename: 'Stitching_Assign'
    }, {
      extend: 'excel',
      title: 'Stitching_Assign',
      filename: 'Stitching_Assign'
    },{
      extend: 'print',
      title: 'Stitching_Assign',
      filename: 'Stitching_Assign'
    }]
  });
});
</script>
@endsection


