@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Size Details</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <table id="example" class="table table-bordered" style="width:100%">
                        <thead>
                           <tr>
                              <th style="text-align:center">Sr.</th>
                              <th style="text-align:center">Size</th>
                              <th style="text-align:center">Created</th>
                           </tr>
                        </thead>
                        <?php  $SrNo = 1; ?>
                        <tbody>
                           @foreach($sizeshow as $data)
                           <tr>
                              <td style="text-align:center">{{$SrNo++}}</td>
                              <td style="text-align:center">{{$data->size}}</td>
                              <td style="text-align:center">{{date('d-m-Y', strtotime($data->created_at))}}</td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
   $(document).ready(function() {
     $('#example').DataTable({
       dom: 'Bfrtip',
       buttons: [
       {
         extend: 'copy',
         filename: 'sizes'
       }, {
         extend: 'csv',
         filename: 'sizes'
       },
       {
         extend: 'pdf',
         title: 'Size Details',
         filename: 'sizes'
       }, {
         extend: 'excel',
         title: 'Size Details',
         filename: 'sizes'
       },{
         extend: 'print',
         title: 'Size Details',
         filename: 'sizes'
       }]
     });
   });
</script>
@endsection