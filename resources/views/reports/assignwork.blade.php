@extends('layout.app')
<?php use App\StitchingReturn; ?>
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Work Assign</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <center>
                        <div class="control-group">
                           
                           <!-- <div class="control-group">
                              <label class="control-label" for="firm_name">Select Party/Firm</label>
                              <div class="controls">
                                 <select class="span4" id="party_id" name="party_id" autofocus="true">
                                    <option value="0" selected="true">All</option>
                                    @foreach($partydata as $data)
                                    <option value="{{$data->id}}" @if(old('party') == $data->id){{ "selected" }}@endif>{{$data->firm_name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              </div> --> 
                           <div class="control-group ">
                              <label class="control-label" for="employee">Employee Name:</label>
                              <div class="controls">
                                 <select class="span4" id="employee_id" name="employee_id" autofocus="true">
                                    <option value="0" selected="true">All</option>
                                    @foreach($employee as $data)
                                    <option value="{{$data->id}}">{{$data->firstname}}  {{$data->lastname}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                           <!-- <form method="post" action="{{url('report/employee/work')}}">
                              @csrf -->
                           <!-- <input type="text" name="daterange" id="daterange" class="form-control span4" value="" style="height: 40px;" /> -->
                           <input type="text" name="daterange" id="daterange" class="form-control span4" value="" />
                           <br>
                           <input type="submit" value="Submit" id="btnsubmit" class="btn btn-success">
                           <!-- </form>  -->
                        </div>
                     </center>
                  </div>
                  <div class="tablshow"></div>
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'left'
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
   });
   $("#btnsubmit").click(function(){
       var daterange=$('#daterange').val();
       // var party_id=$('#party_id').val();    
       var employee_id=$('#employee_id').val();    
         $.ajax({
         type: "POST",
         url: "work",
         data: {
           "_token": "{{ csrf_token() }}",
           "daterange": daterange,
           // "party_id": party_id,
           "employee_id": employee_id
           },
         success: function(data){
           // alert(data);
           $("table").show();
           $(".tablshow").html(data);
         }
         });
       });     
</script>
@endsection