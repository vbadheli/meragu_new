@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Party Details <?php echo $party_details[0]->firm_name ?> </h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">                    
                     <table id="tabdata" class="table table-bordered">                     	
                        <tr>
						   <th>Sr.no</th>
						   <th>Firm Name</th>
						   <th>Party Name</th>
						   <th>Email</th>
						   <th>Phone</th>
						   <th>Address</th>
						   <th>GST No.</th>
						   <th>Date</th>
						</tr>
						<?php $i=1;?>						
						@if(count($party_details)>0)
						@foreach($party_details as $data)
						   
						   <tr>
						      <td>{{$i++}}</td>
						      <td>{{$data->firm_name}}</td>
						      <td>{{$data->firstname}} {{$data->lastname}}</td>
						      <td> {{$data->email}}</td>
						      <td> {{$data->phone}}</td>
						      <td> {{$data->address}}</td>
						      <td> {{$data->gst}}</td>
						      <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y')}}</td>						      
						   </tr>
						@endforeach
						 @else 
						<tr>
						   <td colspan="5">No Records Found</td>
						</tr>
						@endif                       
                     </table>
                  </div>
                  <h3>Invoices</h3>
                  <div class="tab-pane active" id="formcontrols">                    
                     <table id="tabdata" class="table table-bordered">                     	
                        <tr>
						   <th>Sr.no</th>
						   <th>Invoice No</th>						   						   
						   <th>IGST</th>
						   <th>SGST</th>
						   <th>CGST</th>
						   <th>Advance Payment</th>
						   <th>Shipping</th>
						   <th>Total</th>
						   <th>Status</th>
						   <th>Date</th>
						</tr>
						<?php $i=1;?>						
						@if(count($invoices)>0)
						@foreach($invoices as $invoice)
						   @if($invoice->payment_status==0)
						   	<?php $status="unpaid"; ?>
						   	@else
						   	<?php $status="paid"; ?>
						   	@endif
						   <tr>
						      <td>{{$i++}}</td>
						      <td>{{$invoice->invoice_number}}</td>
						      <td>{{$invoice->igst}}</td> 
						      <td>{{$invoice->sgst}}</td>
						      <td> {{$invoice->cgst}}</td>
						      <td> {{$invoice->advance_payment}}</td>
						      <td> {{$invoice->shipping}}</td>
						      <td> {{$invoice->total}}</td>
						      <td> {{$status}}</td>
						      <td>{{ \Carbon\Carbon::parse($invoice->created_at)->format('d/m/Y')}}</td>						      
						   </tr>
						@endforeach
						 @else 
						<tr>
						   <td colspan="10">No Records Found</td>
						</tr>
						@endif
                       
                     </table>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

@endsection