@extends('layout.app')
<style>
table, th, td {
    border: 1px solid black;
}
</style>
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<h2><center>{{ __('Employee Pay Shit Details') }}</center></h2>
				<div class="card-body">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr>
								<th style="text-align:center">Sr.</th>
								<th style="text-align:center">Employee Name</th>								
								<th style="text-align:center">Pices</th>								
								<th style="text-align:center">Amount</th>								
								<th style="text-align:center">ESI</th>								
								<th style="text-align:center">PF</th>								
								<th style="text-align:center">Advance Cutting</th>								
								<th style="text-align:center">Total Amount</th>								
								<th style="text-align:center">Pay Salary</th>								
								<th style="text-align:center">Created</th>								
							</tr>							
						</thead>
						<?php  $SrNo = 1; ?>
						<tbody>
							@foreach($employee_payshit as $data)
							<tr>
								<td style="text-align:center">{{$SrNo++}}</td>																
								<td style="text-align:center">{{$data->employee->firstname}}  {{$data->employee->lastname}}</td>								
								<td style="text-align:center">{{$data->total_pics}} </td>								
								<td style="text-align:center">{{$data->cal_amount}} /-</td>								
								<td style="text-align:center">{{$data->esi}} /-</td>								
								<td style="text-align:center">{{$data->pf}} /-</td>	
								@if($data->advance_cut==null)							
									<td style="text-align:center">0 /-</td>
								@else
									<td style="text-align:center">{{$data->advance_cut}} /-</td>
								@endif
								<td style="text-align:center">{{$data->totalamount}} /-</td>								
								<td style="text-align:center">{{$data->payable_amount}} /-</td>								
								<td style="text-align:center">{{date('d-m-Y', strtotime($data->created_at))}}</td>								
							</tr>
							@endforeach
						</tbody>						
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
  $('#example').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'copy',
      filename: 'payshit'
    }, {
      extend: 'csv',
      filename: 'payshit'
    },
    {
      extend: 'pdf',
      title: 'Employee Pay Shit Details',
      filename: 'payshit'
    }, {
      extend: 'excel',
      title: 'Employee Pay Shit Details',
      filename: 'payshit'
    },{
      extend: 'print',
      title: 'Employee Pay Shit Details',
      filename: 'payshit'
    }]
  });
});
</script>
@endsection

