@extends('layout.app')
@section('content')
@if(isset($data))
<div class="row"> 
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Edit Category</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/category/update',$data->id)}}" method="POST">
                        @csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                {{ session('success') }}
                              </div>
                           @endif                           
                        <fieldset>
                           <div class="control-group">
                              <label class="control-label" for="category_name">Size</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="name"  value="{{$data->name}}" placeholder="Enter Your Category name" name="name" required="true" autofocus="ture">
                              </div>
                              <!-- /controls -->            
                           </div>                           
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-success">Update</button> 
                              <a  href="{{url('category')}}" class="btn btn-default" type="reset">Cancel</a>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>   
            
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
@endif
@endsection