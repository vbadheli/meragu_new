@extends('layout.app')
@section('content')
<div class="row">	
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add Category</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/category/create')}}" method="POST">
                     	@csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ session('success') }}
                              </div>
                           @endif                           
                        <fieldset>
                           <div class="control-group">
                              <label class="control-label" for="category_name">Category Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="name" placeholder="Enter Your Category Name" name="name" required="true" autofocus="ture">
                              </div>
                              <!-- /controls -->				
                           </div>                           
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save</button> 
                              <button class="btn btn-default" type="reset" id="reset">Reset</button>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>    
            <div class="span11">
               <div class="widget-header">
                  <h3>Show Records</h3>
               </div>
               <!-- /widget-header -->
               <div class="widget-content">
                  <div class="card-body">
               <table class="table table-bordered" id="example">
                  <thead>
                     <tr>
                        <th>Sr.</th>                        
                        <th>Category</th>                        
                        <!-- <th>Created At</th> -->
                        <th>Edit</th>
                        <th>Delete</th>
                     </tr>
                  </thead>
                  <?php  $SrNo = 1; ?>
                  <tbody>
                     @foreach($category as $data)                                        
                     <tr>
                        <td>{{$SrNo++}}</td>
                        <td>{{$data->name}}</td>                        
                        <!-- <td>{{$data->created_at}}</td>                          -->
                        <td>
                           <a href="{{url('category/edit', $data['id'])}}" class="btn btn-warning btn-sm"><i class="icon-pencil"></i></a></td>
                        <td>
                           <form action="{{url('category/destroy', $data['id'])}}" method="post">
                              @csrf
                              <input name="_method" type="hidden" value="DELETE">
                              <button class="btn btn-danger btn-sm"  onclick="return confirm('Are you sure?')" type="submit"><i class="icon-remove"></i></button>
                            </form>
                        </td>                         
                     </tr>
                     @endforeach
                  </tbody>
               </table>             
            </div>
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
// $('[data-toggle="tooltip"]').tooltip();
// $("#btnsave").click(function () {
//      $("#btnsave").toggle();
//   });
// $("#reset").click(function () {
//      $("#btnsave").toggle();
//   });
</script>
@endsection