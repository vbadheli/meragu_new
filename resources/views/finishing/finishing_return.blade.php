@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add Finishing Section</h3>
         </div>
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-stitching" class="form-horizontal" action="{{url('/finishing-return/store')}}" method="POST">
                        @csrf
                        @if (Session::get('error'))
                        <div class="alert alert-danger">
                           {{ Session::get('error') }}
                        </div>
                        @endif
                        @if(session('success'))
                        <div class="alert alert-success">
                           {{ session('success') }}
                        </div>
                        @endif                          
                        <fieldset> 
                           <div class="control-group span6">
                              <label class="control-label" for="dno">Design No:</label>
                              <div class="controls">
                                 <select class="span4" id="dno" name="dno" required="true" autofocus="true" onChange="showdesigndata();">
                                    <option value='0' selected='true'>Select design no</option>
                                    <!-- <option value='-1'>-- All --</option> -->
                                    @foreach ($stockdata as $key) 
                                          <option value='{{$key->id}}'>{{$key->dno}} - {{$key->party->firm_name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <!-- /controls -->            
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="date">Received Date:</label>
                              <div class="controls">
                                 <input type="date" class="span4" name="received_date" value="<?php echo date('Y-m-d'); ?>">
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="employee">Employee Name:</label>
                              <div class="controls">
                                 <select class="span4" id="employee" name="employee"  required="true">
                                    <option value="" selected="true" disabled="true">Select Employee Name</option>
                                    @foreach($employee as $data)
                                    <option value="{{$data->id}}">{{$data->firstname}}  {{$data->lastname}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>                           
                           <div id="showtable">

                           </div>
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary" id="btnsave">Save</button> 
                              <button class="btn btn-default" type="reset" id="reset" data-toggle="tooltip" title="Click me Open Save Button !..">Reset</button>
                           </div>                          
                        </fieldset>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript"> 
   function showdesigndata() {
      var stockid = $("#dno").val();
      if(stockid != '0')
      {
         $.ajax({
         type: "GET",
         url: "finishing_data/"+stockid,
         success: function(data){          
            $("table").show();
            $("#showtable").html(data);
         }
         });
      }
    }  
</script>
@endsection