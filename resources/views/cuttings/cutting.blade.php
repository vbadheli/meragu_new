@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add Cutting Section</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/cutting/store')}}" method="POST">
                        @csrf
                        @if (Session::get('error'))
                        <div class="alert alert-danger">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                           {{ Session::get('error') }}
                        </div>
                        @endif
                        @if(session('success'))
                        <div class="alert alert-success">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                           {{ session('success') }}
                        </div>
                        @endif    
                        <?php
                           $party_id=""; 
                           $stock_id=""; 
                           if($_GET==null){
                              $party_id="";
                              $stock_id="";
                           }
                           else{
                              $party_id=$_GET['party_insert_id'];
                              $stock_id=$_GET['stock_insert_id'];
                           }

                        ?>
                        <fieldset>
                           <div class="control-group">
                              <label class="control-label" for="firm_name">Select Party/Firm</label>
                              <div class="controls">
                                 <select class="span4" id="party_id" name="party_id" autofocus="true" onChange="getState(this.value);" required="true">
                                    <option value="">Select Party/Firm Name</option>
                                    @foreach($partydata as $data)
                                    <option value="{{$data->id}}" @if ($party_id == $data->id) selected="selected" @endif>{{$data->firm_name}}</option>
                                    @endforeach
                                 </select>
                                 <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#party_model">Add Party Name</a>
                              </div>
                              <!-- /controls -->            
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="dno">Design No:</label>
                              <div class="controls">
                                 <select class="span4" id="dno" name="dno" autofocus="true" required="true">
                                    <option value=''>Select Design No.</option>
                                    @foreach($allStock as $data)
                                    <option value="{{$data->id}}" @if ($stock_id == $data->id) selected="selected" @endif>{{$data->dno}}</option>
                                    @endforeach
                                 </select>
                                 <!-- <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#design_model">Add Design No.</a> -->
                              </div>
                              <!-- /controls -->            
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="category_name">Category Name:</label>
                              <div class="controls">
                                 <select class="span4" id="category_name" name="category_name" autofocus="true" required="true">
                                    <option value=''>Select Category Name.</option>
                                    @foreach($category as $cat)
                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                 </select>
                                 <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#category_model">Add Category Name.</a>
                              </div>
                              <!-- /controls -->            
                           </div>
                        </fieldset>
                           <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#sizesModel">Select Size</a>

                        <div id="tblshow" style="width: 1000px; overflow: auto">
                        </div>
                        <!-- <div id="buttonreplacement" style="margin-left:30px; display:none;">
                              <img src="//www.willmaster.com/images/preload.gif" alt="loading...">
                        </div> -->
                        <div class="form-actions">
                          <input type="submit" class="btn btn-primary" id="btnsave" value="Save">
                          <button class="btn btn-default" type="reset" id="reset" data-toggle="tooltip" title="Click me Open Save Button !..">Reset</button>
                        </div>
                        
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>

<!-- <input type='text'  readonly=' name='totalpic[]'  id='totalpic' class='form-control span3 totalpic' style='width: 45px;'> -->
<!-- /row -->
<div class="modal fade" id="sizesModel" tabindex="-1" role="dialog" aria-labelledby="sizesModelLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="sizesModelLabel">Select Size</h5>
            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
            <!-- <span aria-hidden="true">&times;</span> -->
            <!-- </button> -->
         </div>
         <div class="modal-body">
            <table id="sizestable" class="table">
                  <tr><th>Select All</th><td><input name="checkedSizes_all" class="checkedSizes_all" type="checkbox">Select All</td></tr>
                  <tr><th>Size</th><th>Select</th></tr>
               @foreach($getsizes as $size)
               <tr><td>{{$size->size}}</td>
               <td><input type="checkbox" name="checkedSizes[]" class="checkedSizes" value="{{$size->id}}"></td>
               </tr>
               @endforeach
            </table>
         </div>
         <div class="modal-footer">
            <button type="button" id="updatecolumns" class="btn btn-primary" data-dismiss="modal">Update Columns</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- This is Category Add model -->
<div class="modal fade" id="category_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <center>
               <h5 class="modal-title" id="exampleModalLongTitle">Add New Category</h5>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form id="edit-profile" class="form-horizontal" action="{{url('/category/create')}}" method="POST">
                        @csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ session('success') }}
                              </div>
                           @endif                           
                        <fieldset>
                           <div class="control-group">
                              <label class="control-label" for="category_name">Category Name</label>
                              <div class="controls">
                                 <input type="text" class="span3" id="name" placeholder="Enter Your Category Name" name="name" required="true" autofocus="ture">
                              </div>
                              <!-- /controls -->            
                           </div>                           
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary" id="btnsave">Save</button> 
                              <button class="btn btn-default" type="reset" id="reset" data-toggle="tooltip" title="Click me Open Save Button !..">Reset</button>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>
               </div>
            </div>
         </div>

<!-- This is model is Party inseterd -->
<div class="modal fade" id="party_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <center>
               <h5 class="modal-title" id="exampleModalLongTitle">Add New Party Name</h5>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form id="edit-profile" class="form-horizontal" action="{{url('/store')}}" method="POST">
               @csrf
               @if (Session::get('error'))
               <div class="alert alert-danger">
                  {{ Session::get('error') }}
               </div>
               @endif
               @if(session('success'))
               <div class="alert alert-success">
                  {{ session('success') }}
               </div>
               @endif
               @if(session('danger'))
               <div class="alert alert-danger">
                  {{ session('danger') }}
               </div>
               @endif
               <fieldset>
                  <div class="control-group">
                     <label class="control-label" for="firm_name">Firm name</label>
                     <div class="controls">
                        <input type="text" class="span2" id="firm_name" placeholder="Enter Your firm_name" name="firm_name" required="true" autofocus="ture">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="gst_no">GST NO.</label>
                     <div class="controls">
                        <input type="text" class="span2" id="gst" placeholder="Enter Your GST Number" name="gst">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="firstname">First Name</label>
                     <div class="controls">
                        <input type="text" class="span2" id="firstname" name="firstname" placeholder="Enter Your First Name">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="lastname">Last Name</label>
                     <div class="controls">
                        <input type="text" class="span2" id="lastname" name="lastname" placeholder="Enter Your Last Name">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="email">Email Address</label>
                     <div class="controls">
                        <input type="text" class="span2" id="email" name="email" placeholder="Enter Valid Email Address">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="phone">Phone</label>
                     <div class="controls">
                        <input type="text" class="span2" id="phone" name="phone" placeholder="Enter phone Number" required="true">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="address">Address</label>
                     <div class="controls">
                        <input type="text" class="span2" id="address" name="address" placeholder="Enter Address">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <!-- <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Save</button> 
                     <button class="btn btn-default" type="reset">Reset</button>
                     </div> -->
                  <!-- /form-actions -->
                  <div class="modal-footer">
                     <button type="submit" class="btn btn-primary">Save</button>
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
               </fieldset>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- This is model is design inseterd -->
<div class="modal fade" id="design_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <center>
               <h5 class="modal-title" id="exampleModalLongTitle">Add New Desing Number</h5>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form id="edit-profile" class="form-horizontal" action="{{url('/stock/store')}}" method="POST">
               @csrf
               @if (Session::get('error'))
               <div class="alert alert-danger">
                  {{ Session::get('error') }}
               </div>
               @endif
               @if(session('success'))
               <div class="alert alert-success">
                  {{ session('success') }}
               </div>
               @endif
               @if(session('danger'))
               <div class="alert alert-danger">
                  {{ session('danger') }}
               </div>
               @endif
               <fieldset>
                  <div class="control-group">
                     <label class="control-label" for="firm_name">Select Party/Firm</label>
                     <div class="controls">
                        <select class="span3" id="party" name="party" autofocus="true" required="true" >
                           <option value="" selected="true" disabled="true">Select Party/Firm Name</option>
                           @foreach($partydata as $data)
                           <option value="{{$data->id}}">{{$data->firm_name}}</option>
                           @endforeach
                        </select>
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#party_model">Add Party Name</a> -->
                  <div class="control-group">
                     <label class="control-label" for="date">Date</label>
                     <div class="controls">
                        <input type="date" class="span3" id="date" name="date" value="<?php echo date('Y-m-d');?>">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="dno">Design No:</label>
                     <div class="controls">
                        <input type="text" class="span3" id="dno" name="dno" placeholder="Enter Your Design Number" required="">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="Panna/Width">Panna/Width:</label>
                     <div class="controls">
                        <input type="text" class="span3" id="panna" name="panna" placeholder="Enter Panna/Width">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="challanno">Challan No.</label>
                     <div class="controls">
                        <input type="text" class="span3" id="challanno" name="challanno" placeholder="Enter Challan Number">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="cloth_name">Cloth Brand Name</label>
                     <div class="controls">
                        <input type="text" class="span3" id="cloth_name" name="cloth_name" placeholder="Enter Cloth Brand Name">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <div class="control-group span6">
                        <label class="control-label" for="dno">Category Name:</label>
                        <div class="controls">
                           <select class="span3" multiple  id="category_name" name="category_name[]" autofocus="true" required="true">
                              <option value='' selected='true' disabled='true'>Select Category Name</option>
                              @foreach($category as $cate)
                                 <option value="{{$cate->id}}">{{$cate->name}}</option>
                              @endforeach
                           </select>
                           <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#category_model">Add Category Name.</a>
                        </div>
                        <!-- /controls -->            
                     </div>
                  <br>
                  <!-- /control-group -->
                  <!-- <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Save</button> 
                     <button class="btn btn-default" type="reset">Reset</button>
                     </div> -->
                  <!-- /form-actions -->
               </fieldset>
               <div class="span11">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>Size/Mtr</th>
                           <th>Color</th>
                           <th>Photo</th>
                           <th style="text-align:center"><a href="#" class="btn btn-success addRow"><i class="icon-plus-sign"></i></a></th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td><input type="number"  placeholder="Enter Size Or Miter" name="sd[0][size]" class="span3  size" required="ture"></td>
                           <td><input type="text" placeholder="Enter Color Name" name="sd[0][color]" class="span3 sd_item color" required="ture"></td>
                           <td><input type="file" name="sd[0][photo]" class="form-control span3 photo sd_item"></td>
                           <td><a href="#" class="btn btn-danger remove"><i class="icon-remove"></i></a></td>
                        </tr>
                     </tbody>
                     <tfoot>
                        <tr>
                           <td id="total">Total</td>
                           <td style="border:none"></td>
                           <td style="border:none"></td>
                           <td style="border:none"></td>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
   //$("#tblshow").hide();
   $('.checkedSizes_all').on('change', function() {     
                $('.checkedSizes').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.checkedSizes').change(function(){ //".checkbox" change 
            if($('.checkedSizes:checked').length == $('.checkedSizes').length){
                   $('.checkedSizes_all').prop('checked',true);
            }else{
                   $('.checkedSizes_all').prop('checked',false);
            }
        });
   function getStockDetail(sizes) {
      var partyid = $("#party_id").val();
      var stockid = $("#dno").val();
      if(partyid != "" && stockid != "")
      {
         $.ajax({
         type: "POST",
         url: "{{URL::route('stock.cutting')}}",
         data:{"_token": "{{ csrf_token() }}",stockid: stockid, partyid: partyid, sizes:sizes},
         success: function(data){
           //alert(data);
           $("#tblshow").show();
           $("#tblshow").html(data);
         }
         });
      }
    }

   $("#dno").on("change", function(){
      if($(this)[0].selectedIndex > 0)
         $("#sizesModel").modal("show");
   });
   $("#updatecolumns").click(function(){
      
      var selectedItems = [];
      $("input[name='checkedSizes[]']").each(function(indx,ele){
         // console.log($(this).val()+":"+$(this).is(":checked"));
         var selected = $(this).is(":checked");
         var value = $(this).val();
         if(selected)
            selectedItems.push(value);
      });
      getStockDetail(selectedItems);
      console.log(selectedItems);
   });
   $('#icol').click(function(){
     if($('#col').val()){
         $('.mtable tr').append($("<td>"));
         $('.mtable thead tr>td:last').html($('#col').val());
         var columnname=$('#col').val();
         $('.mtable tbody tr').each(function(){$(this).children('td:last').append($('<input type="text" name="'+columnname+'" class="form-control span3" style="width: 45px;">'))});
       }
       //else{alert('Enter Text');}
       resetEditor();
   });
   
    function getState(val) {
      $.ajax({
      type: "GET",
      url: "view/"+val,
      success: function(data){
      //alert(data);
        $("#dno").html(data);
      }
      });
    }

    function getColor(stockid) {
      var partyid = $("#party_id").val();
      $.ajax({
      type: "GET",
      url: "viewstockdetails/"+stockid+"/"+partyid,
      success: function(data){
        //alert(data);
        $("table").show();
        $("#tblshow").html(data);
      }
      });
    }

    
       $(document).on('keydown keyup',function(){
          var totalsum=0;
          $('.count0').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                totalsum+=parseFloat(inputVal);
             }
          });
           // alert();
          $("#totalM").text(totalsum);
       });
       $(document).on('keydown keyup',function(){
          var totalsum=0;
          $('.count1').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                totalsum+=parseFloat(inputVal);
             }
          });
          $("#totalL").text(totalsum);
       });
       $(document).on('keydown keyup',function(){
          var totalsum=0;
          $('.count2').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                totalsum+=parseFloat(inputVal);
             }
          });
          $("#totalXL").text(totalsum);
       });
       $(document).on('keydown keyup',function(){
          var totalsum=0;
          $('.count3').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                totalsum+=parseFloat(inputVal);
             }
          });
          $("#totalTWOXL").text(totalsum);
       });
       $(document).on('keydown keyup',function(){
          var totalsum=0;
          $('.count4').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                totalsum+=parseFloat(inputVal);
             }
          });
          $("#totalTHREEXL").text(totalsum);
       });
       $(document).on('keydown keyup',function(){
          var grandtotal=0;
          $('.totalpics').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                grandtotal+=parseFloat(inputVal);
             }
          });
          $("#Sumoftotal").text(grandtotal);
       }); 

       $('tbody').delegate('.count0,.count1,.count2,.count3,.count4','keyup',function(){
          var tr=$(this).parent().parent();
          var total=0;
          var value1=tr.find('.count0').val();
          var value2=tr.find('.count1').val();
          var value3=tr.find('.count2').val();
          var value4=tr.find('.count3').val();
          var value5=tr.find('.count4').val();
          if(value1=="")
              value1=0;
          if(value2=="")
              value2=0;
          if(value3=="")
              value3=0;
          if(value4=="")
              value4=0;
          if(value5=="")
              value5=0;
          total=parseInt(value1)+parseInt(value2)+parseInt(value3)+parseInt(value4)+parseInt(value5);
          tr.find('.totalpics').val(total);
       }); 
       $('tfoot').delegate('.avg1,.avg2,.avg3,.avg4,.avg5','keyup',function(){
          var tr=$(this).parent().parent();
          var total=0;
          // var totalAvg=0;
          var Avg1=tr.find('.avg1').val();
          var Avg2=tr.find('.avg2').val();
          var Avg3=tr.find('.avg3').val();
          var Avg4=tr.find('.avg4').val();
          var Avg5=tr.find('.avg5').val();          
          if(Avg1=="")
              Avg1=0;
          if(Avg2=="")
              Avg2=0;
          if(Avg3=="")
              Avg3=0;
          if(Avg4=="")
              Avg4=0;
          if(Avg5=="")
              Avg5=0;
          total=parseFloat(Avg1)+parseFloat(Avg2)+parseFloat(Avg3)+parseFloat(Avg4)+parseFloat(Avg5);
          // totalAvg=total/5;
          tr.find('.totalpicavg').val(totalAvg);
       }); 
       function resetEditor()
       {
         $("#col").val("");
       }     
</script>

<script type="text/javascript">   
   var rowcount = 1;
   $(".addRow").on("click",function(){
      addRow();
   });

   
   function addRow()
   {
      console.log($("#sd_table tr").length)
      rowcount++;
      var addRow='<tr>'+
                  '<td><input type="number" placeholder="Enter Size Or Miter" name="sd['+rowcount+'][size]" class="span3 size"></td>'+
                  '<td><input type="text" placeholder="Enter Color Name" name="sd['+rowcount+'][color]" class="span3 color sd_item"></td>'+
                  '<td><input type="file" name="sd['+rowcount+'][photo]" class="form-control span3 photo sd_item"></td>'+
                  '<td><a href="#" class="btn btn-danger remove"><i class="icon-remove"></i></a></td>'+
                  '</tr> ';
      $('tbody').append(addRow);
   }
   $("#sd_table").delegate(".sd_item","change",function(){
      addRow();
   });
   // function total()
   // {
   //    var total=0;
   //    $('.size').each(function(i,e){
   //       var size=$(this).val()-0;
   //       total+=size;
   //    })
   //    $('#total').text(total);
   // }
   $(document).on('keydown keyup',function(){
      var totalsum=0;
      $('.size').each(function(){
         var inputVal=$(this).val();
         if($.isNumeric(inputVal)){
            totalsum+=parseFloat(inputVal);
         }
      });
      $("#total").text(totalsum);
   });
   $('body').delegate('.remove','click',function(){
      var l=$('tbody tr').length;
      if(l==1)
      {
         alert("You can not remove last one");
      }else
         $(this).parent().parent().remove();
   });
   // $(document).ready(function() {
   //    $('[data-toggle="tooltip"]').tooltip();
   //    $("#btnsave").click(function () {
   //       $("#btnsave").toggle();
   //    });
   //    $("#reset").click(function () {
   //       $("#btnsave").show();
   //    });
   // });
</script>
@endsection