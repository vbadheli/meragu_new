@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Edit Cutting Section</h3>
         </div>
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                        @if (Session::get('errors'))
                           <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              {{ Session::get('errors') }}
                           </div>
                           @endif
                           @if(session('success'))
                           <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              {{ session('success') }}
                           </div>
                           @endif                       
                        <fieldset>   
                           <div class="control-group">
                              <label class="control-label" for="firm_name">Select Party/Firm</label>
                              <div class="controls">
                                 <select class="span4" id="party_id" name="party_id" autofocus="true" onChange="getState(this.value);" required="true">
                                    <option value="">Select Party/Firm Name</option>
                                    @foreach($partydata as $data)
                                    <option value="{{$data->id}}">{{$data->firm_name}}</option>
                                    @endforeach
                                 </select>
                                 <!-- <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#party_model">Add Party Name</a> -->
                              </div>
                              <!-- /controls -->            
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="dno">Design No:</label>
                              <div class="controls">
                                 <select class="span4" id="dno" name="dno" autofocus="true" required="true" onClick="getColor(this.value);">
                                    <option value=''>Select Design No.</option>
                                    @foreach($allStock as $data)
                                    <option value="{{$data->id}}">{{$data->dno}}</option>
                                    @endforeach
                                 </select>
                                 <!-- <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#design_model">Add Design No.</a> -->
                              </div>
                              <!-- /controls -->            
                           </div>
                        <form id="edit-stitching" class="form-horizontal" action="{{url('/cutting/update')}}" method="POST">
                           @csrf
                           
                           <div id="tblshow"></div>
                        </fieldset>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript">    
   function getState(val) {
      $.ajax({
      type: "GET",
      url: "view/"+val,
      success: function(data){
      //alert(data);
        $("#dno").html(data);
      }
      });
    }

    function getColor(stockid) {
      var partyid = $("#party_id").val();
      $.ajax({
      type: "GET",
      url: "viewstockdetails/"+stockid+"/"+partyid,
      success: function(data){
        // alert(data);
        $("table").show();
        $("#tblshow").html(data);
      }
      });
    }
    function deletedata(id) {
   var data = confirm('Do you really want to delete this record?');
      if(data === true) {
       $.ajax({
       type: 'POST',
       url: 'destroy/'+id,
       data: {'_token': '{{ csrf_token() }}','id':id},
       success: function(data){
             // alert(data);
            }
       });
       return true;
      }
      return false;
   }  
</script>
@endsection