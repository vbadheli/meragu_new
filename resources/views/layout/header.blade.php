<div class="navbar navbar-fixed-top noprint">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="{{url('/home')}}">Archana Garments</a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <!-- <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li> -->
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> {{ Auth::user()->username }} <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Profile</a></li>
              <li><a href="javascript:;" href="{{ url('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                    </a>
                <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </li>
            </ul>
          </li>
        </ul>
        <!-- <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form> -->
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar noprint">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li class="active"><a href="{{url('/home')}}"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i><span>Important</span> </a>
            <ul class="dropdown-menu">
              <li><a href="{{url('/cutting')}}">Cutting</a></li>             
              <li><a href="{{url('/stitching')}}">Stitching</a></li>             
              <li><a href="{{url('/stitchings')}}">stitchings</a></li>             
              <li><a href="{{url('/fixedsalary')}}">Emp Fixed Salary</a></li>             
              <li><a href="{{url('/stitchingreturns')}}">Stitching Return</a></li>   
              <li><a href="{{url('/finishing')}}">Finishing</a></li>   
              <li><a href="{{url('/finishing-return')}}">Finishing Return</a></li>   
              <li><a href="{{url('/cutting/edit')}}">Edit Cutting</a></li>             
              <li><a href="{{url('/editstitching')}}">Edit Stitching</a></li>             
              <li><a href="{{url('/loan')}}">Advance Payment</a></li>             
            </ul>
        </li>
        <!-- <li><a href="{{url('/stock')}}"><i class="icon-money"></i><span>Stock</span></a></li> -->
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-money"></i></i><span>Stock Master</span> </a>
            <ul class="dropdown-menu">
              <li><a href="{{url('/stock')}}">Stock</a></li>
              <li><a href="{{url('/editstock')}}">Edit Stock</a></li>
            </ul>
        </li> 
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-paper-clip"></i><span>Reports</span> </a>
            <ul class="dropdown-menu">
              <li><a href="{{url('/report/party/show')}}">Party</a></li>
              <li><a href="{{url('/report/employee/show')}}">Employee</a></li>
              <li><a href="{{url('/report/size/show')}}">Size</a></li>             
              <li><a href="{{url('/report/stock')}}">Stock</a></li>             
              <li><a href="{{url('/report/stitching/assignwork')}}">Assign work</a></li>             
              <!-- <li><a href="{{url('/report/stitching/assign')}}">Stitching Assign</a></li>              -->
              <li><a href="{{url('/report/employee/employee_pay_shit')}}">Employee Payment Report</a></li>             
              <li><a href="{{url('/report/employee/paymentsheet')}}">Payment Sheet</a></li>             
              <li><a href="{{url('/report/employee/weeklypayment')}}">Weekly Payment Sheet</a></li>             
              <li><a href="{{url('/stock/usage')}}">Remaining Stock</a></li>             
              <li><a href="{{url('/report/finishing')}}">Finishing</a></li>             
            </ul>
        </li>

        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-th-list"></i><span>Invoice</span> </a>
            <ul class="dropdown-menu">
              <li><a href="{{url('/invoices')}}">View All Invoices</a></li>
              <li><a href="{{url('/invoices/create')}}">Create New</a></li>
            </ul>
        </li>

        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i><span>Master</span> </a>
            <ul class="dropdown-menu">
              <li><a href="{{url('/party')}}">Partys</a></li>
              <li><a href="{{url('/employee')}}">Employees</a></li>
              <li><a href="{{url('/size')}}">Size</a></li>
              <li><a href="{{url('/category')}}">Category</a></li>
            </ul>
        </li>

      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
