<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice - #{{$invoice->invoice_number}}</title>
    <link rel="stylesheet" href="{{url('css/4_1_3_bootstrap.min.css')}}" >
    <!-- <style type="text/css">
       table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
    </style> -->
</head>
<body>
<div class="information">
    <table width="100%" class="table">
        <tr>
            <td align="left" >
                <img src="{{asset('img/logo_3.png')}}" alt="Logo" width="80" class="logo"/>                   
             </td>
             <td></td>
            <td align="right">
               <h4><b><u>Invoice Bill</u></b></h4>
               <h5>Invoice no.: #{{$invoice->invoice_number}}<h5>
               <h6>Date: {{ date('l jS F Y', time()) }}</h6>
               <h6>Due Date: {{ date('l jS F Y', time()) }}</h6>
            </td>
        </tr>        
        <tr>
            <td align="left">                
                <h3>MERAGU GARMENT</h3>
                <h5>E-93/2/B M.I.D.C ,Akkalkot Road</h5>
                <h6>Solapur,Maharashtra(MH-27),PIN Code 413003,India</h6>
                <h6>Mobile No:- 9175583915</h6>
                <h6>Email Id:- sadanand.meragu@gmail.com</h6>
                <h6>GSTIN No:- 27BGJPM8890A1Z3</h6>
            </td>
            <td align="center" >
                <h3><b><u>Billing Address:</u></b></h3>
                <h3>{{$invoice->party->firm_name}}</h3>
                <h5>{{$invoice->party->address}}</h5>
                <h6>Mobile No:- {{$invoice->party->phone}}</h6>
                <h6>Email Id:- {{$invoice->party->email}}</h6>
                <h6>GSTIN NO:- {{$invoice->party->gst}}</h6>                   
             </td>
             <td align="right" >
                <h3><b><u>Shipping Address:</u></b></h3>
                <h3>{{$invoice->party->firm_name}}</h3>
                <h5>{{$invoice->party->address}}</h5>
                <h6>Mobile No:- {{$invoice->party->phone}}</h6>
                <h6>Email Id:- {{$invoice->party->email}}</h6>
                <h6>GSTIN NO:- {{$invoice->party->gst}}</h6>                   
             </td>                          
        </tr>       
    </table>
</div>


<div>    
    <table width="100%" class="table">
        <thead>
        <tr>
            <th>PRODUCT /SERVICE NAME</th>
            <th>HSN/SAC</th>
            <th>QTY</th>
            <th>UNIT PRICE</th>
            <th>IGST</th>
            <th>CGST</th>
            <th>SGST</th>
            <th>AMOUNT</th>
        </tr>
        </thead>
        <tbody>
            <?php $subTotal =0;
                $sumofigst=0;
                $sumofcgst=0;
                $sumofsgst=0;
                $totalgst=0;
                $sumofqty=0;
                $sumofprice=0;
                $totalamout=0;
                $sumofsub=0;
                $subigst=0;
                $subcgst=0;
                $subsgst=0;
                $TAX=0;

             ?>
            @foreach($invoice->invoiceItemDetails as $item)
             <?php   $sumofigst=($item->quantity * $item->rate) * $invoice->igst/100;
                     $sumofcgst=($item->quantity * $item->rate) * $invoice->cgst/100;
                     $sumofsgst=($item->quantity * $item->rate) * $invoice->sgst/100;
                     $totalgst=$invoice->igst+$invoice->cgst+$invoice->sgst;
                     $sumofqty+=$item->quantity;
                     $sumofprice+=$item->rate;
                     $totalamout=($item->quantity * $item->rate)+$sumofigst+$sumofcgst+$sumofsgst;
                     $sumofsub+=$totalamout;
                     $subigst+=$sumofigst;
                     $subcgst+=$sumofcgst;
                     $subsgst+=$sumofsgst;
                     $TAX = $subigst+$subcgst+$subsgst;
             ?>
        <tr>
            <td>Dno.:{{$item->stockdetail->stock->dno}}<br>{{$item->stockdetail->color}}: Size {{$item->cuttingdetail->size->size}}</td>
            <td></td>
            <td>{{number_format($item->quantity, 2, '.', '')}}</td>
            <td>{{number_format($item->rate, 2, '.', '')}}</td>
            <td>{{number_format($sumofigst, 2, '.', '') }} <br>
                {{number_format($invoice->igst, 2, '.', '')}} %</td>
            <td>{{number_format($sumofcgst, 2, '.', '')  }} <br>
                {{number_format($invoice->cgst, 2, '.', '')}} %</td>
            <td>{{number_format($sumofsgst, 2, '.', '')  }} <br>
                {{number_format($invoice->sgst, 2, '.', '')}} %</td>
            <td align="left">{{number_format($totalamout, 2, '.', '')}}</td>
            <?php $subTotal += ($item->quantity * $item->rate)?>
        </tr>
        @endforeach
        </tbody>
    <tr><td>Total</td><td>@ {{number_format($totalgst, 2, '.', '')}}</td><td>{{$sumofqty}}</td><td>{{number_format($sumofprice, 2, '.', '')}}</td><td>{{number_format($subigst, 2, '.', '')}}</td><td>{{number_format($subcgst, 2, '.', '')}}</td><td>{{number_format($subsgst, 2, '.', '')}}</td><td>{{number_format($sumofsub, 2, '.', '')}}</td></tr></table>
</div>

<table class="table">
    <tr>
        <td>&nbsp;</td>
        <td align="right">
        
<table class="table" id="tabletotal">
    <tr>
        <td align="left" style="width: 50%;">
            Total Before GST Amount
        </td>
        <td align="right" style="width: 50%;">
            {{number_format($subTotal, 2, '.', '')}}
        </td>
    </tr>
    <tr>
        <td align="left" style="width: 50%;">
            Toatl Tax Amount @ {{number_format($totalgst, 2, '.', '')}}
        </td>
        <td align="right" style="width: 50%;">
           {{number_format($TAX, 2, '.', '')}}
        </td>
    </tr>
    <tr>
        <td align="left" style="width: 50%;">
            Shipping
        </td>
        <td align="right" style="width: 50%;">
            Rs. {{number_format($invoice->shipping, 2, '.', '')}}
        </td>
    </tr>

    <tr>
        <td align="left" style="width: 50%;">
            Sub-Total
        </td>
        <td align="right" style="width: 50%;">
            Rs. {{number_format($sumofsub, 2, '.', '')}}
        </td>
    </tr>
    <tr>
        <td align="left" style="width: 50%;">
            <strong>Total</strong>
        </td>
        <td align="right" style="width: 50%;">
            <strong>Rs. {{number_format($sumofsub+$invoice->shipping, 2, '.', '')}}</strong>
        </td>
    </tr>

</table>
</td>
</tr>
<tr><td>______________________________</td><td></td></tr>
<tr><td>Authorized Signatory</td><td></td></tr>
</table>

</body>
</html>