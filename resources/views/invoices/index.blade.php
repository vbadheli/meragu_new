@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Invoices</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                    <form name="invoice_filter" action="">
	                     <div class="control-group">
	                        <label class="control-label" for="firm_name">Select Party/Firm</label>
	                        <div class="controls">
	                           <select class="span4" id="party_id" name="party_id">
	                              <option value="">Select Party/Firm Name</option>
	                              <option value="0">All</option>
	                              @foreach($partydata as $data)
	                              <option value="{{$data->id}}">{{$data->firm_name}}</option>
	                              @endforeach
	                           </select>
	                        </div>
	                        <!-- /controls -->            
	                     
	                     	<label class="control-label" for="firm_name">Payment Status</label>
	                        <div class="controls">
	                           <select class="span4" id="payment_status" name="payment_status">
	                              <option value="">All</option>
	                              <option value="0">Unpaid</option>
	                              <option value="1">Paid</option>
	                           </select>
	                        </div>
	                        <!-- /controls -->            
	                     </div>
	                     <input type="submit" class="btn btn-primary" name="Search" value="Search">
	                 </form>
                     <table id="tabdata" class="table table-bordered">
                     	
                        <?= view('invoices.partial_invoice_table', compact('allInvoices'))?>
                       
                     </table>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

@endsection