<tr>
   <th>Sr.no</th>
   <th>Amount</th>
   <th>Remarks</th>
   <th>Payment Date</th>
</tr>
<?php $i=1;?>
@if(count($invoice->partyPayments)>0)
@foreach($invoice->partyPayments as $payment)
   <tr>
      <td>{{$i++}}</td>
      <td>{{$payment->amount}}</td>
      <td>{{$payment->remarks}}</td>
      <td>{{ \Carbon\Carbon::parse($payment->created_at)->format('d/m/Y')}}</td>
   </tr>
@endforeach
 @else 
<tr>
   <td colspan="5">No Records Found</td>
</tr>
@endif