@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>New Invoice</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/invoices/store')}}" method="POST">
                        @csrf
                        @if (Session::get('error'))
                        <div class="alert alert-danger">
                           {{ Session::get('error') }}
                        </div>
                        @endif
                        @if(session('success'))
                        <div class="alert alert-success">
                           {{ session('success') }}
                        </div>
                        @endif                          
                        <fieldset>
                           <div class="control-group">
                              <label class="control-label" for="firm_name">Select Party/Firm</label>
                              <div class="controls">
                                 <select class="span4" id="party_id" name="invoice[party_id]" autofocus="true" required="true">
                                    <option value="">Select Party/Firm Name</option>
                                    @foreach($allParty as $data)
                                    <option value="{{$data->id}}">{{$data->firm_name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <!-- /controls -->            
                           </div>
                           <!-- <div class="control-group">
                              <label class="control-label" for="dno">Design No:</label>
                              <div class="controls">
                                 <select class="span4" id="dno" name="dno" autofocus="true" required="true">
                                    <option value=''>Select Design No.</option>
                                    @foreach($allStock as $data)
                                    <option value="{{$data->id}}">{{$data->dno}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div> -->

                           <div class="control-group">
                              <label class="control-label" for="dno">Invoice No:</label>
                              <div class="controls">
                                 <input type="text" name="invoice[invoice_number]" required="">
                              </div>
                              <!-- /controls -->            
                           </div>

                           
                        </fieldset>
                        <div >

                        	<table id="pos_table" class="table">
                        		<thead>
	                        		<tr>
	                        			<!-- <th>Sr.no.</th> -->
	                        			<th>Particulars</th>
	                        			<th>Quantity</th>
	                        			<th>Rate</th>
	                        			<th>Sub Total</th>
	                        			<th>x</th>
	                        		</tr>
	                        	</thead>
                        		<tbody>
	                        		<tr>
	                        			<!-- <td>1</td> -->
	                        			<td><select class="form-control" name="item[1][stockdetail_id]" required="true">
	                        				<option value="">Select Item</option>
                        					@foreach($allStockDetails as $sd)
                        						@foreach($sd->cuttingdetails as $cd)
                        							<option value="{{$sd->id}}:{{$cd->id}}" >{{$sd->color}}: Size {{$cd->size->size}}</option>
                        						@endforeach
                        					@endforeach
	                        			</select></td>
	                        			<td><input class="pos_quantity" type="text" name="item[1][quantity]" required="true"></td>
	                        			<td><input class="pos_unit_price_inc_tax" type="text" name="item[1][rate]" required="true"></td>
	                        			<td>
	                        				<input class="pos_line_total" type="hidden" name="item[1][line_total]">
	                        				<h3 class="display_currency pos_line_total_text">Rs 0</h3></td>
	                        			<td><i class="fa fa-close text-danger pos_remove_row cursor-pointer" >X</i></td>
	                        		</tr>
                        		</tbody>
                        	</table>

                        <button type="button" class="btn btn-primary" id="addnewitem">Add New</button> 
                        	<table>

                        		<tr><td><h3>Total: </h3></td><td><span class="price_total"></span></td></tr>
                        		<tr><td>GST %</td><td><input type="text" placeholder="IGST" class="igst" id="igst" name="invoice[igst]"></td></tr>
                        		<tr><td>SGST %</td><td><input type="text" placeholder="SGST" class="sgst" id="sgst" name="invoice[sgst]"></td></tr>
                        		<tr><td>CGST %</td><td><input type="text" placeholder="CGST" class="cgst" id="cgst" name="invoice[cgst]"></td></tr>
                        		<tr><td>Advance</td><td><input type="text" placeholder="Advance" class="advance" id="advance_payment" name="invoice[adv]"></td></tr>
                        		<tr><td>Shipping Charges</td><td><input type="text" placeholder="Shipping" id="shipping_charges" class="shipping" name="invoice[shipping]"></td></tr>
                        		<tr><td><h3>Total Payable: </h3></td><td>
                        			<input type="hidden" id="final_total_input" name="invoice[final_total_input]">
                        			<h3><span id="total_payable"></span><span id="total_payable_span"></span></h3>
                        		</td></tr>
                        	</table>
                        </div>
                        <div class="form-actions">
                          <button type="submit" class="btn btn-primary">Save</button> 
                          <button class="btn btn-default" type="reset">Reset</button>
                        </div>
                        <!-- <div id="buttonreplacement" style="margin-left:30px; display:none;">
                              <img src="//www.willmaster.com/images/preload.gif" alt="loading...">
                        </div> -->
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{asset('js/accounting.min.js')}}"></script>
<script src="{{asset('js/functions.js')}}"></script>
<script src="{{asset('js/pos.js')}}"></script>
<script type="text/javascript">

	<?php 
$op = "";
	foreach($allStockDetails as $sd):
		foreach($sd->cuttingdetails as $cd):
			$op .=  "<option value='$sd->id:$cd->id'>$sd->color: Size ".$cd->size->size."</option>";
		endforeach;
	endforeach;
?>
var op = "<?=$op?>";
$(document).ready(function(){

	$("#dno").on("change",function(){
	var id = $(this).val();
		$.ajax({
	      type: "GET",
	      url: '{{URL::route("stockdetails" )}}',
	      data: {stock_id: id},
	      success: function(data){
	      	// console.log(data);
	        // $("#dno").html(data);
	      }
	    });
	});

	$("#addnewitem").on('click', function() {
var rowcount = $("#pos_table").find('tr').length;
// alert(rowcount);
		row = '<tr><td><select class="form-control" name="item['+rowcount+'][stockdetail_id]"><option>Select Item</option>'+op+'</select></td><td><input class="pos_quantity" type="text" name="item['+rowcount+'][quantity]"></td><td><input class="pos_unit_price_inc_tax" type="text" name="item['+rowcount+'][rate]"></td><td><input class="pos_line_total" type="hidden" name="item['+rowcount+'][line_total]"><h3 class="display_currency pos_line_total_text">Rs 0</h3></td><td><i class="fa fa-close text-danger pos_remove_row cursor-pointer" >X</i></td></tr>';
		$("#pos_table").append(row);
		
	});

	$("#pos_table").on('click','.removeRow', function(){
		// alert("hi");
		$(this).parents('tr').remove();
		//pos_total_row();
	});
});
function ButtonClicked()
{
   document.getElementById("formsubmitbutton").style.display = "none"; // to undisplay
   document.getElementById("buttonreplacement").style.display = ""; // to display
   return true;
}
var FirstLoading = true;
function RestoreSubmitButton()
{
   if( FirstLoading )
   {
      FirstLoading = false;
      return;
   }
   document.getElementById("formsubmitbutton").style.display = ""; // to display
   document.getElementById("buttonreplacement").style.display = "none"; // to undisplay
}
// To disable restoring submit button, disable or delete next line.
document.onfocus = RestoreSubmitButton;


</script>
@endsection