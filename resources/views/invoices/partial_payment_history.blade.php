<tr>
   <th>Sr.no</th>
   <th>Invoice No.</th>
   <th>Total</th>
   <th>Paid</th>
   <th>Balance</th>
   <th>Last Payment Date</th>
   <th>Payment Status</th>
   <th>Action</th>
</tr>
<?php $i=1;
      $sumoftotal=0;
      $sumofpaidamount=0;
      $sumofremaining=0;
?>
@if(count($invoices)>0)
@foreach($invoices as $invoice)
   <?php 
   $total = $invoice->total;
   $paid = $invoice->partyPayments->sum('amount');
   $remaining = $total - $paid;
   ?>
   <tr>
      <td>{{$i++}}</td>
      <td><a href="#"> {{$invoice->invoice_number}}</a></td>
      <td>{{$invoice->total}}</td>
      <td>{{$paid}}</td>
      <td>{{$remaining}}</td>
      <td>{{ \Carbon\Carbon::parse($invoice->created_at)->format('d/m/Y')}}</td>
      <td>@if($invoice->payment_status == App\Invoices::PAYMENT_STATUS_UNPAID){{"Un-paid"}} @else {{"Paid"}} @endif</td>
      <td><a href="{{route('print-invoice',['id'=>$invoice->id])}}" target="__blank">Print</a><br>
      <a href="{{route('invoice-payment',['id'=>$invoice->id])}}" target="__blank">Add Payment</a></br>
      <a href="{{route('invoice-payment-history',['id'=>$invoice->id])}}" target="__blank">History</a></td>
   </tr>
     <?php  $sumoftotal+=$total;
            $sumofpaidamount+=$paid;
            $sumofremaining+=$remaining;
      ?>
@endforeach
 @else 
<tr>
   <td colspan="5">No Records Found</td>
</tr>
@endif

<tr><td>Total</td><td></td><td><?= $sumoftotal ?></td><td><?= $sumofpaidamount ?></td><td><?= $sumofremaining ?></td><td></td><td></td><td></td></tr>