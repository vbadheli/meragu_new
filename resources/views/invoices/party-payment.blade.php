@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Invoice Party Payments : invoice # {{$invoice->invoice_number}}</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                    <form name="invoice_filter" action="{{route('invoice-payment-store')}}" method="post">
                    	@csrf
	                     <div class="control-group">
	                        <label class="control-label" for="firm_name">Amount</label>
	                        <div class="controls">
	                           <input type="hidden" name="invoice_id" value="{{$id}}">
	                           <input type="text" name="amount" required="">
	                        </div>

	                        <label class="control-label" for="firm_name">Remarks</label>
	                        <div class="controls">
	                           <input type="text" name="remarks" required="">
	                        </div>
	                        <!-- /controls -->            
	                     
	                        <!-- /controls -->            
	                     </div>
	                     <input type="submit" class="btn btn-primary" name="Submit" value="Submit">
	                 </form>
                     <table id="tabdata" class="table table-bordered">
                     	
                        
                       
                     </table>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- /row -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

@endsection