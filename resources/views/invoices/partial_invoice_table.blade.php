<tr>
   <th>Sr.no</th>
   <th>Party</th>
   <th>Invoice No.</th>
   <th>Date</th>
   <th>Payment Status</th>
   <th>Action</th>
</tr>
<?php $i=1;?>
@if(count($allInvoices)>0)
@foreach($allInvoices as $invoice)
   
   <tr>
      <td>{{$i++}}</td>
      <td><a href="#"> {{$invoice->party->firm_name}}</a></td>
      <td><a href="#"> {{$invoice->invoice_number}}</a></td>
      <td>{{ \Carbon\Carbon::parse($invoice->created_at)->format('d/m/Y')}}</td>
      <td>@if($invoice->payment_status == App\Invoices::PAYMENT_STATUS_UNPAID){{"Un-paid"}} @else {{"Paid"}} @endif</td>
      <td><a href="{{route('print-invoice',['id'=>$invoice->id])}}" target="__blank">Invoice Bill Print</a><br>
         <a href="{{route('original-bill-print',['id'=>$invoice->id])}}" target="__blank">Original Bill Print</a><br>
         <a href="{{route('duplicate-transport-bill',['id'=>$invoice->id])}}" target="__blank">Duplicate for Transport Bill Print</a><br>
      <a href="{{route('invoice-payment',['id'=>$invoice->id])}}" target="__blank">Add Payment</a><br>
      <a href="{{route('invoice-payment-history',['id'=>$invoice->id])}}" target="__blank">History</a></td>
   </tr>
@endforeach
 @else 
<tr>
   <td colspan="5">No Records Found</td>
</tr>
@endif