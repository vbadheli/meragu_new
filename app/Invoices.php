<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    /*protected $dates = [
        'created_at','updated_at'
    ];*/

    const PAYMENT_STATUS_PAID = 10;
    const PAYMENT_STATUS_PARTIAL = 1;
    const PAYMENT_STATUS_UNPAID = 0;

    public function invoiceItemDetails()
    {
        return $this->hasMany('App\InvoiceItemDetails','invoice_id');
    }

    public function partyPayments()
    {
        return $this->hasMany('App\PartyPayments','invoice_id')->orderBy('created_at', 'desc');
    }

    public function party()
    {
        return $this->belongsTo('App\Party');
    }


}
