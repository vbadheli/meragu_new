<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuttingdetail extends Model
{
    
    public function stockdetail()
    {
        return $this->belongsTo('App\Stockdetails');
    }

    public function size()
    {
        return $this->belongsTo('App\Size','size_id');
    }
}
