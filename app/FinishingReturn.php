<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinishingReturn extends Model
{
    public function employee()
    {
        return $this->belongsTo('App\Employee','employee_id','id');
    }
    public function finishing()
    {
        return $this->belongsTo('App\Finishing','finishing_id','id');
    }
    public function stitcreturn()
    {
        return $this->belongsTo('App\StitchingReturn','stitching_return_id','id');
    }
    public function stitcAss()
    {
        return $this->belongsTo('App\StitchingAssignment','stitching_id','id');
    }
    public function cuttingdetail()
    {
        return $this->belongsTo('App\Cuttingdetail','cuttingdetail_id','id');
    }
    public function stockdetail()
    {
        return $this->belongsTo('App\Stockdetails','stockdetail_id','id');
    }
    public function stock()
    {
        return $this->belongsTo('App\Stock','stock_id','id');
    }
}
