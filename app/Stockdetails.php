<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stockdetails extends Model
{
    public function stock()
    {
        return $this->belongsTo('App\Stock');
    }

    public function cuttingdetails()
    {
        return $this->hasMany('App\Cuttingdetail','stockdetail_id');
    }

    public function usage()
    {
        return $this->hasOne('App\StockUses','stockdetail_id');
    }
}
