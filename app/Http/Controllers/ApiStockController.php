<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stock;
use App\Stockdetails;
use App\StockFashion;

class ApiStockController extends Controller
{
    public function stock_store(Request $request)
    {
       // $data =stocks::create($request->all());
        $stock_data = new Stock();
        $stock_data->party_id = $request->party;
        $stock_data->date = $request->date;
        $stock_data->dno = $request->dno;
        $stock_data->challan_no = $request->challanno;
        $stock_data->panna = $request->panna;
        $stock_data->cloth_name = $request->cloth_name;      
        if($stock_data->save()){
            $id=$stock_data->id;
           
            $stockfashion = new StockFashion();
            $stockfashion->stock_id = $id;
            $stockfashion->fashion_id = $request->cloth_name;                
            $stockfashion->save();           
        
           $data=new Stockdetails();
           $data->stock_id=$id;
           $data->length=$request->length;
           $data->color=$request->color;
           $data->photo=$request->photo;
           $data->save();
            
        }        
        return response()->json(['status'=>'Submit Successfullly']);
    }
}
