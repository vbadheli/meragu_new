<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Employee;
use App\Party;
use App\Stock;
use App\Stockdetails;
use App\Size;
use App\Cutting;
use App\Cuttingdetail;
use App\StitchingAssignment;
use App\StitchingReturn;
use App\Advance;
use App\StockUses;
use App\StockSizeAvg;
use App\StockFashion;
use App\Fashion;
use Illuminate\Support\Facades\DB;

class CuttingController extends Controller
{
    
    public function index(Request $request)
    {
         
        if($request->party_insert_id!=null && $request->stock_insert_id!=null){
            $partydata = Party::where('id',$request->party_insert_id)->get();
            $allStock = Stock::where('id',$request->stock_insert_id)->get();
        }
        else{
            $partydata = Party::get();
            $allStock = Stock::get();
        }
        $getsizes = Size::get();
        $category = Fashion::all();   

        return view('cuttings.cutting')->with('partydata',$partydata)->with('getsizes', $getsizes)->with('allStock', $allStock)->with('category', $category);
    }

    public function showList()
    {
        $allCutting = Stock::all();
        return view('cuttings.list')->with('allCutting', $allCutting);
    }

    public function getStockDetails(Request $request)
    {
    	$stockid = $request->stockid;
    	$partyid = $request->partyid;
    	$sizesData = $request->sizes;
      	
        $stockdata = Stockdetails::where('stock_id',$stockid)->get();
        // dd($stockdata);
        $cuttingdata = Cutting::where('stock_id',$stockid)->where('party_id',$partyid)->first();
        $getsizes = Size::get();
        $data="";
        $i = 0;
        // dd($cuttingdata);
                              
        $sizedata = count($sizesData);
        $data .= "<table class='table table-bordered mtable' id='cuttingtable'>";
        if($cuttingdata == null)
        {
            $stockdetails_id_arr_count = count($stockdata);
            //dd($stockdetails_id_arr_count);
                $data .= "<thead>
                    <tr>
                        <th style='text-align:center'>Size Cutting</th>";
                        //for ($i=0; $i <$stockdetails_id_arr_count ; $i++) { 
                        for ($j=0; $j <$sizedata ; $j++) { 
                        	$sizeModel = Size::find($sizesData[$j]);
                            $data .= "<th style='text-align:center'>
                            	<input type='hidden' name='sizes[]' id='sizes$sizesData[$j]' value='$sizesData[$j]'/>
                            	$sizeModel->size
                            </th>";
                        }
                        $data .= "
                        <th style='text-align:center'>Remaining Stock</th>
                        <th style='text-align:center'>Total Mtrs used</th> 
                        
                        <th style='text-align:center'>Total Pics</th>                            
                    </tr>
                </thead><tbody>";
            foreach ($stockdata as $key) {
                $i=0;
                // $i_i=0;
                $data.= "<tr>                       
                           <td style='text-align:center'>
                           <input type='hidden' readonly='true' name='leng[]' class='span3 leng' value='$key->length'>
                           <input type='hidden' readonly='true' name='stockid[]' class='span3' value='$key->id' style='width: 45px;'>
                           <input type='hidden' readonly='true' name='sizename[]' class='span3 sizename' value='$key->color - $key->length (mtrs)'>$key->color - $key->length (mtrs)</td>";
                           for ($j=0; $j <$sizedata ; $j++)
                           $data .= "<td style='text-align:center'><input type='number' name='count[$i][]' id='count$i' class='form-control span3 count_h count".$i++."' style='width: 45px;'></td>";

                           $data .= "
                           <td style='text-align:center'><input type='text' readonly='' name='totalrm[]' class='form-control span3 totalrm' style='width: 45px;'></td>
                           <td style='text-align:center'><input type='text' readonly='' name='totalmts[]' class='form-control span3 totalmtsused' style='width: 45px;'></td>
                           
                           <td style='text-align:center'><input type='text' readonly='' name='totalpic[]' class='form-control span3 totalpics' style='width: 45px;'></td>
                         </tr>";
                $i ++;
            }
            $data .= "</tbody><tfoot>
                                 <tr>
                                    <td style='text-align:center'>AVG</td>";

                                    for ($j=0; $j <$sizedata ; $j++)
                                    $data .= "<td style='text-align:center'><input type='text' name='avg[]' class='form-control span3 avg_h avg1' style='width: 45px;'></td>";

                                    
                                    $data .= "<td style='text-align:center'></td><td style='text-align:center'></td>
                                    <td style='text-align:center'><input  readonly='' type='text' name='totalpicavg[]' id='totalpicavg' class='form-control span3 totalpicavg' style='width: 45px;'></td>
                                 </tr>
                                 <tr>
                                    <td style='text-align:center'>Total</td>";
                                    for ($j=0; $j <$sizedata ; $j++)
                                    $data .= "<td style='text-align:center' id='total$j' class='totalamount'></td>";
                                    
                                    $data .= "<td style='text-align:center' class='mtsrm'></td><td style='text-align:center' class='mtsused'></td>
                                    <td style='text-align:center' id='Sumoftotal' class='Sumoftotal'></td>
                                 </tr>
                              </tfoot>";
        }
        else
        {   
            $data .= "";
            $stockSizeAvg = StockSizeAvg::where('stock_id', $stockid)->get();
            foreach ($stockdata as $key => $sd) {

                $i=0;
                $cuttingDetailData = Cuttingdetail::where('cutting_id',$cuttingdata->id)->where('stockdetail_id', $sd->id)->get();
                $stockUses = StockUses::where('stockdetail_id', $sd->id)->first();
                
                
                if($key == 0)
                {
                    $data .= "<tr><th style='text-align:center'>Description</th>";

                    for ($j=0; $j < count($cuttingDetailData) ; $j++) { 
                        $data .= "<th style='text-align:center'>
                            <select class='span4 sizes$j' id='sizes$j' name='sizes[]' autofocus='true' style='width: 70px;'>
                              <option value=''>Sizes</option>";
                                foreach ($getsizes as $value) {
                                    $selected = "";
                                    if($cuttingDetailData[$j]->size_id == $value->id)
                                        $selected = "selected";
                                    $data .= "<option value='$value->id' ".$selected.">$value->size</option>";
                                }
                           $data .= "</select></th>";
                    }

                    for ($j=count($cuttingDetailData); $j < $sizedata ; $j++) { 
                        $data .= "<th style='text-align:center'>
                            <select class='span4 sizes$j' id='sizes$j' name='sizes[]' autofocus='true' style='width: 70px;'>
                              <option value=''>Sizes</option>";
                                foreach ($getsizes as $value) {
                                    $data .= "<option value='$value->id'>$value->size</option>";
                                }
                           $data .= "</select></th>";
                    }
                    $data .= "<th style='text-align:center'>Total Mts used</th>
                    <th style='text-align:center'>Total Pics</th></tr>";
                }
            
                $stockuse = ($stockUses != null) ? $stockUses->used_mtrs:"";
                $stockpcs = ($stockUses != null) ? $stockUses->total_pcs:"";
                
                $data.= "<tr>                       
                           <td style='text-align:center'>
                           <input type='hidden' readonly='true' name='stockdetails_id[]' class='span3' value='$sd->id' style='width: 45px;'>
                           <input type='text' readonly='true' name='sizename[]' class='span3 sizename' value='$sd->color - $sd->length (mtrs)'></td>
                           ";
                           foreach ($cuttingDetailData as $cdd) {
                            $data .= "<td style='text-align:center'><input type='number' name='count[$i][]' id='count".$i."' class='span3 count_h count".$i++."' style='width: 45px;' value='".$cdd->count."'></td>";
                               
                           }

                           for($j=$i; $j<$sizedata;$j++)
                               $data .="<td style='text-align:center'><input type='text' name='count[$j][]' id='count$j' class='span3 count_h count$j' style='width: 45px;'></td>";
                         $data .= "<td style='text-align:center'><input type='text' value='".$stockuse."' name='totalmtsused[]' class='form-control span3 totalmtsused' style='width: 45px;' readonly></td>";
                         $data .= "<td style='text-align:center'><input type='text' value='".$stockpcs."' name='totalpic[]'  id='totalpics' class='form-control span3 totalpic' style='width: 45px;' readonly></td></tr>";
                //$i ++;
                
            }
            $data .= "
                                 <tr>
                                    <td style='text-align:center'>AVG</td>";
                                    foreach ($stockSizeAvg as $key => $value) {
                                        $data .= "<td style='text-align:center'>
                                        <input type='text' value='$value->avg' name='avg[]' class='form-control span3 avg_h avg1' style='width: 45px;'></td>";
                                        
                                    }
                                    for($j = count($stockSizeAvg); $j < $sizedata; $j++) {
                                        $data .= "<td style='text-align:center'><input type='text' name='avg[]' class='form-control span3 avg_h avg2' style='width: 45px;'></td>";
                                    }
                                    "<td style='text-align:center'><input type='text' name='avg[]' id='XLavg' class='form-control span3 avg_h avg3' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' id='2XLavg' class='form-control span3 avg_h avg4' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' id='3XLaavgvg' class='form-control span3 avg_h avg5' style='width: 45px;'></td>";
                                    $data .= "<td style='text-align:center'></td><td style='text-align:center'></td>
                                    <td style='text-align:center'><input  readonly='' type='text' name='totalpicavg[]' id='totalpicavg' class='form-control span3 totalpicavg' style='width: 45px;'></td>
                                 </tr>
                                 <tr>
                                    <td style='text-align:center'>Total</td>";
                                    for ($j=0; $j <$sizedata ; $j++)
                                    $data.="<td style='text-align:center' id='total$j' class='totalamount'></td>";
                                    
                                    $data .= "<td style='text-align:center' class='mtsused'></td>
                                    <td style='text-align:center' id='Sumoftotal' class='Sumoftotal'></td>
                                 </tr>
                              ";
            $data .= "</table>";


        }
        $script = "<script>
        function calculateMtsUsed()
            {
                $('#cuttingtable tr').each(function(key, value){
                    
                    var rowlength = $(this).children().length;
                    var cols = $(this).children();
                    var totalavg = parseFloat($('#totalpicavg').val());
                    if(key != 0 && key != (rowlength-1) && key != (rowlength-2))
                    {
                        var colslength = $(this).children().length;
                        // var totalmtsused = parseInt( $(this).children().last().children()[0].value );
                        var totalmtsrm = parseInt( $(this).children().first().children()[0].value );
                        var totalmtsused = parseInt( $(this).children().last().children()[0].value ) * totalavg;
                        // console.log(totalmtsused);
                         // console.log($($(this).children()[colslength-2]).children()[0].value);

                        if(totalmtsused>totalmtsrm)
                        {
                            alert('Length of stock in not available');
                        }
                        if(totalmtsused > 0)
                        {
                            console.log($($(this).children()[colslength-2]).children()[0].value);
                            $($(this).children()[colslength-2]).children()[0].value = totalmtsused;
                            $($(this).children()[colslength-3]).children()[0].value = totalmtsrm - totalmtsused;
                        }
                    }
                    });
            }
        $('.count_h').on('change', function(){

            var thiss = $(this);
            var parent = thiss.parent();
            var parent_parent = parent.parent();
            var rowlength = $(parent_parent).children().length;
            var totalsum =0;
            $(parent_parent).children().each(function(key, value){
                var thiss = $(this);
                if(key != 0 && key != (rowlength-1) && key != (rowlength-2) && key != (rowlength-3))
                {
                
                    if(parseInt(thiss.children()[0].value) > 0)
                        totalsum += parseInt(thiss.children()[0].value);
                
                }
                });
            $(parent_parent).children().last().children().first().val(totalsum);
            calculateMtsUsed();
            });

        $('.avg_h').on('change', function(){

            var thiss = $(this);
            var parent = thiss.parent();
            var parent_parent = parent.parent();
            var rowlength = $(parent_parent).children().length;
            var totalsum =0;
            var totalcount = 0;
            var totalavg = 0;
            $(parent_parent).children().each(function(key, value){
                var thiss = $(this);
                if(key != 0 && key != (rowlength-1) && key != (rowlength-2) && key != (rowlength-3))
                {
                
                    if(parseInt(thiss.children()[0].value) > 0)
                    {
                        totalcount++;
                        totalsum += parseFloat(thiss.children()[0].value);
                    }
                
                }
                });

            totalavg = (totalsum/totalcount).toFixed(2);
            $(parent_parent).children().last().children().first().val(totalavg);
            calculateMtsUsed();
            });

            $(document).on('keydown keyup',function(){
              var grandtotal=0;
              $('.totalpics').each(function(){
                 var inputVal=$(this).val();
                 if($.isNumeric(inputVal)){
                    grandtotal+=parseFloat(inputVal);
                 }
              });
               // alert(grandtotal);
              $('#Sumoftotal').text(grandtotal);
           });           

        </script>";
        return $data.$script;
    }
    public function view($id)
    {
        $stockdata = Stock::where('party_id',$id)->get();
        $data="<option value=''>Select Design No.</option>";
        foreach ($stockdata as $key) {
          $data.= "<option value='$key->id'>$key->dno</option>";
        }
        return $data;
    }public function viewstockdetails($stockid,$partyid)
    {
        $data="";
        $script="";
        $stockdata = Stockdetails::where('stock_id',$stockid)->get();        
        $cuttingdata = Cutting::where('stock_id',$stockid)->where('party_id',$partyid)->first();
            $data .= "<table class='table table-bordered mtable' id='cuttingtable'>";
            $data .= "<tr><th>Description</th><th>Total Cutting</th><th>Actions</th>";
            foreach ($stockdata as $sd) {
                $cuttingdetail = Cuttingdetail::where('stockdetail_id',$sd->id)->get();
                foreach ($cuttingdetail as $cutting) {
                    $data .= "<tr>                    
                    <td><input type='hidden' name='cuttingdetail_id[]' id='cuttingdetail_id' value='$cutting->id'/>".$cutting->size->size."-"." $sd->color - $sd->length mts</td>
                    <td><input type='number' name='count[]' value='$cutting->count' class='form-control'/></td>                                        
                    <td><button type='button' class='btn btn-danger deldata' id='deldata$cutting->id' onClick='deletedata($cutting->id);'><i class='icon-remove'></i></button></td></tr>";
                }
            }
            $data .= "<tr><td></td><td><input type='submit' class='btn btn-success'  value='Update'></td><td></td></tr></table>"; 
            return $data;
    }
    public function editcutting()
    {
        $partydata = Party::get();
        $allStock = Stock::all();
        return view('cuttings.editcutting')->with('partydata',$partydata)->with('allStock', $allStock);
    }
    public function destroy($id){
        // dd($id);
        $cuttingdetail = Cuttingdetail::find($id);
        $cuttingdetail->delete();
        return redirect()->back()->with('success','Cutting Data deleted');
    }
    public function update(Request $request){ 
        // dd($request);  
        $this->validate($request, [
            'count' => 'required',                      
        ]);   
        
        $cdid = $request->cuttingdetail_id;
        $count = $request->count;        
        foreach ($cdid as $index => $item) {
            if($count[$index] != null)
            {
                $cuttingdetail = Cuttingdetail::find($item);
                $cuttingdetail->count = $count[$index];                        
                $cuttingdetail->save();
            }
        }
        return back()->with('success','Updated Successfullly');         
    }
}
