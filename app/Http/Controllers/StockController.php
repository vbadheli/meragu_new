<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Party;
use App\Stock;
use App\Stockdetails;
use App\Size;
use App\Cutting;
use App\Cuttingdetail;
use App\StitchingAssignment;
use App\StitchingReturn;
use App\Advance;
use App\StockUses;
use App\EmployeePayment;
use App\StockSizeAvg;
use App\Fashion;
use App\StockFashion;
use App\Finishing;
use App\FinishingReturn;
use Illuminate\Support\Facades\DB;

class StockController extends Controller
{
    public function index()
    {
        $partydata = Party::get();          
        $category = Fashion::get();          
        return view('stockpage.stock')->with('partydata', $partydata)->with('category', $category);
    }

    public function stockUsage(Request $request)
    {
        $partydata = Party::get();
        $stock = Stock::all();
        $partyid = $request->party;
        if($request->has('party') && $request->party > 0)
        {
          $stock = Stock::where('party_id', $request->party)->get();
        }
        return view('stockpage.stockusage', compact('stock','partydata', 'partyid'));
    }

    public function stockUsageAjax(Request $request)
    {
        $stock = Stock::all();
        if($request->has('party') && $request->party > 0)
        {
          $stock = Stock::where('party_id', $request->party)->get();
        }
        echo view('stockpage.partial_stock_usage', compact('stock'));
    }
    public function editstock(Request $request)
    {
        $partydata = Party::get();          
        $category = Fashion::get();          
        return view('stockpage.editstock')->with('partydata', $partydata)->with('category', $category);
    }
    public function viewstockdata($id)
    {
        $stockdata = Stock::where('party_id',$id)->get();
        $data="<option value=''>Select Design No.</option>";
        foreach ($stockdata as $key) {
          $data.= "<option value='$key->id'>$key->dno</option>";
        }
        return $data;
    }
    public function editdetails($stockid,$partyid)
    {
        $data="";
        $i = 0;
        $stock = Stock::where('id',$stockid)->where('party_id',$partyid)->get();
        foreach ($stock as $stkdata) { 
          $stockdetail = Stockdetails::where('stock_id',$stkdata->id)->get();
          // $data.="<input type='text' value='Panna :- $stkdata->panna' readonly class='span4' id='panna' name='panna' placeholder='Enter Panna/Width'>
          //              <input type='text' value='Challan No :- $stkdata->challan_no' readonly class='span4' id='challanno' name='challanno' placeholder='Enter challanno'>
          //              <input type='text' value='Cloth Name :- $stkdata->cloth_name' readonly class='span4' id='cloth_name' name='cloth_name' placeholder='Enter cloth_name'>";
            
            foreach ($stockdetail as $stkdetaildata) 
            {                
              $data .= "<tr><input type='hidden'  name='stkdetailid[]'  value='$stkdetaildata->id'>
                    <td><input type='number'  placeholder='Enter Size Or Miter' name='size[]' class='span3  size' required='ture' value='$stkdetaildata->length'></td>
                    <td><input type='text' placeholder='Enter Color Name' name='color[]' class='span3 color' required='ture' value='$stkdetaildata->color'></td>
                    <td><input type='file' name='photo[]' class='form-control span3 photo sd_item' value='$stkdetaildata->photo'></td>
                    <td><img src='stockimages/$stkdetaildata->photo' width=25 /></td>
                    <td><a href='#' class='btn btn-danger remove'><i class='icon-remove'></i></a></td>
                 </tr>";
            } 
        } 
        return $data;
    }
    public function updatestock(Request $request)
    {
      $this->validate($request, [
            'party' => 'required',
            'dno' => 'required',
            'size' => 'required',
            'color' => 'required',
        ]);
      $post = $request->all();   
      // dd($post);  
        $stock = Stock::where('id',$request->dno)->where('party_id',$request->party)->first();
        if($stock){
          $stockdetail_arr_count = count($post["stkdetailid"]);
          $stockdetail_arr=$post['stkdetailid'];
          $size_arr=$post['size'];
          $color_arr=$post['color'];
          $photo_arr=$post['photo'];
          for ($i=0; $i <$stockdetail_arr_count ; $i++) { 
            if($size_arr[$i] != null)
            {
                $stkdetails = Stockdetails::where('stock_id',$request->dno)->where('id',$stockdetail_arr[$i])->first();
                if(!$request->sd){
                    $stkdetails->length = $size_arr[$i];
                    $stkdetails->color = $color_arr[$i];
                    if($photo_arr[$i]!=null)
                    {
                      $stkdetails->photo = $photo_arr[$i]; 
                    }
                    $stkdetails->save();
                  }
                  else
                  {
                      $id=$stock->id;                      
                      foreach ($request->sd as $sdItem) 
                      {
                        if($sdItem['size'] != null && $sdItem['color'] != null)
                        {
                            $photo = null;
                            if( isset($sdItem['photo']))
                            {
                              $photo= $sdItem['photo']->getClientOriginalName();
                              $sdItem['photo']->move(public_path().'/stockimages/', $photo);  
                            }
                              $data=array('stock_id'=>$id,
                                      'length'=>$sdItem['size'],
                                      'color'=>$sdItem['color'],
                                      'photo'=>$photo);                              
                              Stockdetails::insert($data);
                        }                        
                      }
                    }
              }            
           } 
        }
        return back()->with('success','Updated Successfullly');
    }
    
    
    public function store(Request $request)
    {
    //   dd($request->all());
        $validatedata = $this->validate($request, [
            'party' => 'required',
            'dno' => 'required',
            'category_name' => 'required',
            'sd.*.size' => 'sometimes',
            'sd.*.color' => 'sometimes',
            'sd.*.photo' => 'sometimes|mimes:jpg,jpeg,bmp,png|max:10000',
            // 'sample_image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'sample_image' => 'sometimes|mimes:jpg,jpeg,bmp,png|max:10000',
        ]);

       // DB::transaction(function() use ($request) {

        $stock = new Stock;        
        $stock->party_id=$request->party;
        $stock->date=$request->date;
        $stock->dno=$request->dno;
        $stock->challan_no=$request->challanno;
        $stock->panna = $request->panna;
        $stock->cloth_name = $request->cloth_name;

        $name = null;
        if( isset($request->sample_image))
        {
          $name = $request->file('sample_image')->getClientOriginalName();
          $request->file('sample_image')->move(public_path().'/sampleimages/', $name);
        }
        
        $stock->image = $name;
          if($stock->save()){
            $id=$stock->id;
            foreach ($request->category_name as $key => $cate) {
                $stockfashion = new StockFashion();
                $stockfashion->stock_id = $id;
                $stockfashion->fashion_id = $cate;                
                $stockfashion->save();
            }
            foreach ($request->sd as $sdItem)
            {
              // dd($sdItem);
              if($sdItem['size'] != null && $sdItem['color'] != null)
              {
                $photo = null;
                if( isset($sdItem['photo']))
                {
                  $photo= $sdItem['photo']->getClientOriginalName();
                  $sdItem['photo']->move(public_path().'/stockimages/', $photo);  
                }
               $data=array('stock_id'=>$id,
                            'length'=>$sdItem['size'],
                            'color'=>$sdItem['color'],
                            'photo'=>$photo);
               Stockdetails::insert($data);
              }
            }
          }
        //});
        $party_insert_id=$stock->party_id;
        $stock_insert_id=$stock->id;
        return redirect()->route('loadcutting',['party_insert_id'=>$party_insert_id,'stock_insert_id'=>$stock_insert_id]);
        // return back()->with('success','Submit Successfullly');
        //return back()->withErrors($validatedata)->withInput();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $sdata = Stock::with('party')->get();   
        return view('reports.stock')->with('sdata', $sdata);
    }
    public function stockdetailsajax($stock_id)
    {
        $stockdata="";
        $stockdetaildata = Stockdetails::where('stock_id',$stock_id)->get();  
        $stockdata.="<table class='table table-bordered'> 
            <thead>
              <tr>
                <th>Cloth meters</th>
                <th>Cloth Color</th>
                <th>Cloth Photo</th>               
              </tr>             
            </thead><tbody>";
            $SrNo = 1;            
              foreach($stockdetaildata as $data){
              $stockdata.="<tr>
                <td>$data->length</td>               
                <td>$data->color</td>               
                <td><img src='stockimages/$data->photo' /></td>               
              </tr>";
            }
            $stockdata.="</tbody></table>"; 
        return $stockdata;
    }
    public function partyshow()
    {
        $partyshow = Party::get();
        return view('reports.party')->with('partyshow', $partyshow);
    }
    public function employeeshow()
    {
        $employeeshow = Employee::get();          
        return view('reports.employee')->with('employeeshow', $employeeshow);
    }
    public function employee_pay_shit()
    {                  
        return view('reports.employee_pay_shit');
    }
    public function getpay_shit(Request $request)
    {     
        $orderdate = explode('-', $request->daterange);       
        $startdate=date('Y-m-d', strtotime($orderdate[0]));
        $enddate=date('Y-m-d', strtotime($orderdate[1]));
        $employee_payshit = EmployeePayment::whereBetween('created_at',[$startdate, $enddate])->get();        
        return view('reports.employee_pay_shit')->with('employee_payshit', $employee_payshit);
    }
    public function weeklypayment()
    {                
        return view('reports.weeklypayment');
    }
    public function getemployeepayment(Request $request)
    {     
        $orderdate = explode('-', $request->daterange);
        $startdate=date('Y-m-d', strtotime($orderdate[0]));
        $enddate=date('Y-m-d', strtotime($orderdate[1]));
        $stitchingreturn = StitchingReturn::whereBetween('received_date',[$startdate, $enddate])->get();
        return view('reports.weeklypayment')->with('stitchingreturn', $stitchingreturn);
    }

    public function paymentsheetshow()
    {
        $employeedata = Employee::get(); 
        return view('reports.paymentsheet')->with('employeedata', $employeedata);
    }
    public function paymentdata($employeeid)
    {  

        $tabledata="";
        $totalpics=0;
        $total=0;
        $sumofamount=0;
        $grandtotal=0;
        $advcutting=0;
        $rateofpices=0;
        $soa=0;
        $employeename = Employee::where('id',$employeeid)->first();
        $stitchingreturn = DB::table('stitching_returns')
                 ->select('stitching_id', DB::raw('sum(received_count) as pices,received_date,id'))
                 ->groupBy(DB::raw("stitching_id,received_date,id"))
                 ->where('employee_id',$employeeid)
                 ->where('paid_status',0)
                 ->get();
        foreach ($stitchingreturn as $stitchretrun) {
            $stitchingassign = StitchingAssignment::where('id',$stitchretrun->stitching_id)->first();
            $cuttingdetails = Cuttingdetail::where('id',$stitchingassign->cuttingdetail_id)->first();
            $stockdata = Stockdetails::where('id',$cuttingdetails->stockdetail_id)->first();
            $stock = Stock::where('id',$stockdata->stock_id)->first();
            $partydata = Party::where('id',$stock->party_id)->first();            
            $currentdate=date('d/M/Y', strtotime($stitchretrun->received_date));
            $rateofpices=number_format($stitchingassign->rate, 2, '.', '');
            $total=number_format($stitchretrun->pices*$stitchingassign->rate, 2, '.', '');
        $tabledata.="<tr>
                    <td><input type='hidden' readonly class='form-control' name='stitchretrun_id[]' value='$stitchretrun->id'>
                    $currentdate</td>                                
                    <td>$partydata->firm_name</td>
                    <td>$stock->dno</td>
                    <td>$stockdata->color</td>
                    <td>$stitchretrun->pices</td>                               
                    <td>$rateofpices</td>                               
                    <td>$total</td>                               
                </tr>";
                $totalpics+=$stitchretrun->pices;
                $sumofamount+=$total;
                $soa=number_format($sumofamount, 2, '.', '');
            }
            $grandtotal+=$sumofamount;
            $advanceamount = Advance::where('employee_id',$employeeid)->where('status',0)->sum('amount');
            $advancecutamount = EmployeePayment::where('employee_id',$employeeid)->where('status',1)->sum('advance_cut');
            $advcutting=number_format($advanceamount-$advancecutamount, 2, '.', '');
           if($totalpics!=0){
            $tabledata.="<tr><input type='hidden' class='form-control' name='employeeid' value='$employeeid'>
                        <td colspan='4'>Total </td>
                        <td> <input type='text' readonly class='form-control' name='totalpics' value='$totalpics Pices' style='height: 38px;width: 70px;'></td>
                        <td>RS:</td>
                        <td><input type='text' readonly class='form-control' name='sumofamount' id='sumofamount'  value='$soa' style='height: 38px;width: 70px;'></td>
                    </tr>
                    <tr>
                        <td colspan='4'>Advance Amount </td> 
                        <td></td><input type='hidden'  class='form-control'  name='advancecut' id='advancecut'>                           
                        <td>Remaining Rs:  </td>                            
                        <td><input type='text' readonly class='form-control' placeholder='Advance'  value='$advcutting' style='height: 38px;width: 70px;'></td>
                    </tr>
                    <tr>
                        <td colspan='4'>ESI Amount</td> 
                        <td></td>                           
                        <td>Rs:</td>                            
                        <td><input type='number'  class='form-control' min='0' max='999999' name='esi' id='esi_amount' placeholder='Enter ESI' style='height: 38px;width: 70px;' required='true'></td>
                    </tr>
                    <tr>
                        <td colspan='4'>PF Amount</td> 
                        <td></td>                           
                        <td>Rs:</td>                            
                        <td><input type='number'  class='form-control' min='0' max='999999' name='PF' id='pf_amount' placeholder='Enter PF' style='height: 38px;width: 70px;' required='true'></td>
                    </tr>                    
                    <tr>
                        <td colspan='4'>Total Amount</td> 
                        <td></td>                           
                        <td>Rs:</td>                            
                        <td><input type='text' class='form-control' readonly placeholder='Total Amount' value='$grandtotal' name='totalamount' id='totalamount' style='height: 38px;width: 70px;'></td>
                    </tr>
                    <tr>
                        <td colspan='4'>Payable Amount </td> 
                        <td></td>                           
                        <td>Rs:</td>
                        <td><input type='number' min='1' max='999999' class='form-control'  name='grandtotal' id='grandtotal'  style='height: 38px;width: 70px;'></td>
                    </tr>
                    <tr>
                        <td colspan='6'></td> 
                        <td><input type='submit'  class='btn btn-success btn-sm' value='Pay Salary'></td>
                    </tr>";
                  }
                    $script = "<script>
                    $('#esi_amount').on('change',function(){
                     var sumofesi=0;           
                        var inputVal=$(this).val();
                        var sum=$('#sumofamount').val();
                        if($.isNumeric(inputVal)){
                           sumofesi=parseFloat(sum)-parseFloat(inputVal);
                        }
                     $('#totalamount').val(sumofesi);    
                     $('#grandtotal').val(sumofesi);
                  });
                  $('#pf_amount').on('change',function(){
                   var sumofpf=0;           
                    var inputVal=$(this).val();
                    var sumgrand=$('#sumofamount').val();
                    var esi_amount=$('#esi_amount').val();                    
                      if($.isNumeric(inputVal)){
                         sumofpf=parseFloat(sumgrand)-parseFloat(inputVal)-parseFloat(esi_amount);
                      }                   
                   $('#totalamount').val(sumofpf); 
                   $('#grandtotal').val(sumofpf);
                });               
                 $('#advance').on('change',function(){
                   var sumofadvance=0;           
                    var inputVal=$(this).val();
                    var sumgrand=$('#sumofamount').val();
                    var esi_amount=$('#esi_amount').val();                    
                    var pf_amount=$('#pf_amount').val();                    
                      if($.isNumeric(inputVal)){
                         sumofadvance=parseFloat(sumgrand)-parseFloat(inputVal)-parseFloat(esi_amount)-parseFloat(pf_amount);
                      }                    
                   $('#totalamount').val(sumofadvance);
                   $('#grandtotal').val(sumofadvance);
                });
                $('#grandtotal').on('change',function(){
                   var advancecutting=0;           
                    var inputVal=$(this).val();
                    var totalamount=$('#totalamount').val();                                       
                      if($.isNumeric(inputVal)){
                         advancecutting=parseFloat(totalamount)-parseFloat(inputVal);
                      }     
                   $('#advancecut').val(advancecutting);
                });               
             </script>";
         if($tabledata=="")
            return $tabledata="<tr><td colspan=7>No Records Found</td></tr>";
         else
            return $tabledata.$script;
    }
    public function storepayment(Request $request)
    {
         // dd($request->all());     
         
        $six_digit_random_number = mt_rand(100000, 999999);
        $payment = new EmployeePayment();
        $payment->paymentsheet_id = $six_digit_random_number;
        $payment->employee_id = $request->employeeid;
        $payment->total_pics = $request->totalpics;        
        $payment->cal_amount = $request->sumofamount;        
        $payment->esi = $request->esi;        
        $payment->pf = $request->PF;        
        $payment->totalamount = $request->totalamount;        
        $payment->payable_amount = $request->grandtotal;        
        $payment->advance_cut = $request->advancecut;        
        $payment->status = 1;        
        if($payment->save())
        {
            foreach ($request->stitchretrun_id as $key => $value) {
            $stitchingreturn = StitchingReturn::where('id',$value)->where('employee_id',$request->employeeid)->first();
            $stitchingreturn->paid_status = 1;
            $stitchingreturn->save();
          }

        }        
        return back()->with('success','Submit Successfullly');
    }
    public function sizeshow()
    {
        $sizeshow = Size::get();          
        return view('reports.size')->with('sizeshow', $sizeshow);
    }

    
    public function datashow()
    {
        $partydata = Party::get();
        $getsizes = Size::get();
        $allStock = Stock::all();
        return view('pages.cutting')->with('partydata',$partydata)->with('getsizes', $getsizes)->with('allStock', $allStock);
    }
    
    public function view($id)
    {
        $stockdata = Stock::where('party_id',$id)->get();
        $data="<option value=''>Select Design No.</option>";
        foreach ($stockdata as $key) {
          $data.= "<option value='$key->id'>$key->dno</option>";
        }
        return $data;
    }

    public function viewstockdetails($stockid,$partyid)
    {
        $stockdata = Stockdetails::where('stock_id',$stockid)->get();        
        $cuttingdata = Cutting::where('stock_id',$stockid)->where('party_id',$partyid)->first();
        $getsizes = Size::get();
        $data="";
        $i = 0;
                              
        $sizedata=5;
        $data .= "<table class='table table-bordered mtable' id='cuttingtable'>";
        if($cuttingdata == null)
        {
            $stockdetails_id_arr_count = count($stockdata);
                $data .= "<thead>
                    <tr>
                        <th style='text-align:center'>Size Cutting</th>";
                        for ($j=0; $j <$sizedata ; $j++) { 
                            $data .= "<th style='text-align:center'>
                                <select class='span4 sizes$j' id='sizes$j' name='sizes[]' autofocus='true' style='width: 70px;'>
                                  <option value=''>Sizes</option>";
                                    foreach ($getsizes as $value) {
                                        $data .= "<option value='$value->id'>$value->size</option>";
                                    }
                               "</select>
                            </th>";
                        }
                        $data .= "<th style='text-align:center'>Total Mtrs used</th> 
                        <th style='text-align:center'>Total Pics</th>                            
                    </tr>
                </thead><tbody>";
            foreach ($stockdata as $key) {
                $i=0;
                $data.= "<tr>                       
                           <td style='text-align:center'>
                           <input type='hidden' readonly='true' name='stockid[]' class='span3' value='$key->id' style='width: 45px;'>
                           <input type='text' readonly='true' name='sizename[]' class='span3 sizename' value='$key->color - $key->length (mtrs)'></td>
                           <td style='text-align:center'><input type='text' name='count[$i][]' id='count$i' class='form-control span3 count_h count".$i++."' style='width: 45px;'></td>
                           <td style='text-align:center'><input type='text' name='count[$i][]' id='count$i' class='form-control span3 count_h count".$i++."' style='width: 45px;'></td>
                           <td style='text-align:center'><input type='text' name='count[$i][]' id='count$i' class='form-control span3 count_h count".$i++."' style='width: 45px;'></td>
                           <td style='text-align:center'><input type='text' name='count[$i][]' id='count$i' class='form-control span3 count_h count".$i++."' style='width: 45px;'></td>
                           <td style='text-align:center'><input type='text' name='count[$i][]' id='count$i' class='form-control span3 count_h count".$i++."' style='width: 45px;'></td>
                           <td style='text-align:center'><input type='text' readonly='' name='totalmts[]' class='form-control span3 totalmtsused' style='width: 45px;'></td>
                           <td style='text-align:center'><input type='text' readonly='' name='totalpic[]'  id='totalpic' class='form-control span3 totalpics' style='width: 45px;'></td>
                         </tr>";
                $i ++;
            }
            $data .= "</tbody><tfoot>
                                 <tr>
                                    <td style='text-align:center'>AVG</td>
                                    <td style='text-align:center'><input type='text' name='avg[]' class='form-control span3 avg_h avg1' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' class='form-control span3 avg_h avg2' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' class='form-control span3 avg_h avg3' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' class='form-control span3 avg_h avg4' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' class='form-control span3 avg_h avg5' style='width: 45px;'></td>
                                    <td style='text-align:center'></td>
                                    <td style='text-align:center'><input  readonly='' type='text' name='totalpicavg[]' id='totalpicavg' class='form-control span3 totalpicavg' style='width: 45px;'></td>
                                 </tr>
                                 <tr>
                                    <td style='text-align:center'>Total</td>
                                    <td style='text-align:center' id='totalM'></td>
                                    <td style='text-align:center' id='totalL'></td>
                                    <td style='text-align:center' id='totalXL'></td>
                                    <td style='text-align:center' id='totalTWOXL'></td>
                                    <td style='text-align:center' id='totalTHREEXL'></td>
                                    <td style='text-align:center' class='mtsused'></td>
                                    <td style='text-align:center' id='Sumoftotal' class='Sumoftotal'></td>
                                 </tr>
                              </tfoot>";
        }
        else
        {   
            $data .= "";
            $stockSizeAvg = StockSizeAvg::where('stock_id', $stockid)->get();
            foreach ($stockdata as $key => $sd) {

                $i=0;
                $cuttingDetailData = Cuttingdetail::where('cutting_id',$cuttingdata->id)->where('stockdetail_id', $sd->id)->get();
                $stockUses = StockUses::where('stockdetail_id', $sd->id)->first();
                
                
                if($key == 0)
                {
                    $data .= "<tr><th style='text-align:center'>Description</th>";

                    for ($j=0; $j < count($cuttingDetailData) ; $j++) { 
                        $data .= "<th style='text-align:center'>
                            <select class='span4 sizes$j' id='sizes$j' name='sizes[]' autofocus='true' style='width: 70px;'>
                              <option value=''>Sizes</option>";
                                foreach ($getsizes as $value) {
                                    $selected = "";
                                    if($cuttingDetailData[$j]->size_id == $value->id)
                                        $selected = "selected";
                                    $data .= "<option value='$value->id' ".$selected.">$value->size</option>";
                                }
                           $data .= "</select></th>";
                    }

                    for ($j=count($cuttingDetailData); $j < $sizedata ; $j++) { 
                        $data .= "<th style='text-align:center'>
                            <select class='span4 sizes$j' id='sizes$j' name='sizes[]' autofocus='true' style='width: 70px;'>
                              <option value=''>Sizes</option>";
                                foreach ($getsizes as $value) {
                                    $data .= "<option value='$value->id'>$value->size</option>";
                                }
                           $data .= "</select></th>";
                    }
                    $data .= "<th style='text-align:center'>Total Mts used</th>
                    <th style='text-align:center'>Total Pics</th></tr>";
                }
            
            $stockuse = ($stockUses != null) ? $stockUses->used_mtrs:"";
            $stockpcs = ($stockUses != null) ? $stockUses->total_pcs:"";
                
                $data.= "<tr>                       
                           <td style='text-align:center'>
                           <input type='hidden' readonly='true' name='stockdetails_id[]' class='span3' value='$sd->id' style='width: 45px;'>
                           <input type='text' readonly='true' name='sizename[]' class='span3 sizename' value='$sd->color - $sd->length (mtrs)'></td>
                           ";
                           foreach ($cuttingDetailData as $cdd) {
                            $data .= "<td style='text-align:center'><input type='text' name='count[$i][]' id='count".$i."' class='span3 count_h count".$i++."' style='width: 45px;' value='".$cdd->count."'></td>";
                               
                           }

                           for($j=$i; $j<5;$j++)
                               $data .="<td style='text-align:center'><input type='text' name='count[$j][]' id='count$j' class='span3 count_h count$j' style='width: 45px;'></td>";
                         $data .= "<td style='text-align:center'><input type='text' value='".$stockuse."' name='totalmtsused[]' class='form-control span3 totalmtsused' style='width: 45px;' readonly></td>";
                         $data .= "<td style='text-align:center'><input type='text' value='".$stockpcs."' name='totalpic[]'  id='totalpics' class='form-control span3 totalpic' style='width: 45px;' readonly></td></tr>";
                //$i ++;
                
            }
            $data .= "
                                 <tr>
                                    <td style='text-align:center'>AVG</td>";
                                    foreach ($stockSizeAvg as $key => $value) {
                                        $data .= "<td style='text-align:center'>
                                        <input type='text' value='$value->avg' name='avg[]' class='form-control span3 avg_h avg1' style='width: 45px;'></td>";
                                        
                                    }
                                    for($j = count($stockSizeAvg); $j < $sizedata; $j++) {
                                        $data .= "<td style='text-align:center'><input type='text' name='avg[]' class='form-control span3 avg_h avg2' style='width: 45px;'></td>";
                                    }
                                    "<td style='text-align:center'><input type='text' name='avg[]' id='XLavg' class='form-control span3 avg_h avg3' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' id='2XLavg' class='form-control span3 avg_h avg4' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' id='3XLaavgvg' class='form-control span3 avg_h avg5' style='width: 45px;'></td>";
                                    $data .= "<td style='text-align:center'></td>
                                    <td style='text-align:center'><input  readonly='' type='text' name='totalpicavg[]' id='totalpicavg' class='form-control span3 totalpicavg' style='width: 45px;'></td>
                                 </tr>
                                 <tr>
                                    <td style='text-align:center'>Total</td>
                                    <td style='text-align:center' id='totalM'></td>
                                    <td style='text-align:center' id='totalL'></td>
                                    <td style='text-align:center' id='totalXL'></td>
                                    <td style='text-align:center' id='totalTWOXL'></td>
                                    <td style='text-align:center' id='totalTHREEXL'></td>
                                    <td style='text-align:center'></td>
                                    <td style='text-align:center' id='Sumoftotal' class='Sumoftotal'></td>
                                 </tr>
                              ";
            $data .= "</table>";


        }
        $script = "<script>
        function calculateMtsUsed()
            {
                $('#cuttingtable tr').each(function(key, value){
                    
                    var rowlength = $(this).children().length;
                    var cols = $(this).children();
                    var totalavg = parseFloat($('#totalpicavg').val());
                    if(key != 0 && key != (rowlength-1) && key != (rowlength-2))
                    {
                        var colslength = $(this).children().length;
                        // var totalmtsused = parseInt( $(this).children().last().children()[0].value );
                        var totalmtsused = parseInt( $(this).children().last().children()[0].value ) * totalavg;
                        // console.log(totalmtsused);
                         // console.log($($(this).children()[colslength-2]).children()[0].value);
                        if(totalmtsused > 0)
                        {
                            console.log($($(this).children()[colslength-2]).children()[0].value);
                            $($(this).children()[colslength-2]).children()[0].value = totalmtsused;
                        }
                    }
                    });
            }
        $('.count_h').on('change', function(){

            var thiss = $(this);
            var parent = thiss.parent();
            var parent_parent = parent.parent();
            var rowlength = $(parent_parent).children().length;
            var totalsum =0;
            $(parent_parent).children().each(function(key, value){
                var thiss = $(this);
                if(key != 0 && key != (rowlength-1) && key != (rowlength-2))
                {
                
                    if(parseInt(thiss.children()[0].value) > 0)
                        totalsum += parseInt(thiss.children()[0].value);
                
                }
                });
            $(parent_parent).children().last().children().first().val(totalsum);
            calculateMtsUsed();
            });

        $('.avg_h').on('change', function(){

            var thiss = $(this);
            var parent = thiss.parent();
            var parent_parent = parent.parent();
            var rowlength = $(parent_parent).children().length;
            var totalsum =0;
            var totalcount = 0;
            var totalavg = 0;
            $(parent_parent).children().each(function(key, value){
                var thiss = $(this);
                if(key != 0 && key != (rowlength-1) && key != (rowlength-2))
                {
                
                    if(parseInt(thiss.children()[0].value) > 0)
                    {
                        totalcount++;
                        totalsum += parseFloat(thiss.children()[0].value);
                    }
                
                }
                });

            totalavg = (totalsum/totalcount).toFixed(2);
            $(parent_parent).children().last().children().first().val(totalavg);
            calculateMtsUsed();
            });

            


        </script>";
        return $data.$script;
    }


    public function insert(Request $request)
    {        
         
         $post = $request->all();
        $this->validate($request, [
            'party_id' => 'required',
            'dno' => 'required'
        ]);         
        $existingdata = Cutting::where('party_id',$request->party_id)->where('stock_id',$request->dno)->first();
        if(!$existingdata)
        {            
          if($request->sizes!=null)
          {
            $cutting=new Cutting;
            $cutting->party_id=$request->party_id;
            $cutting->stock_id=$request->dno;
            $sizes_arr = $post["sizes"];
            $stockdetails_id_arr = $post["stockid"];
            $count_arr = $post["count"];
            $totalmts = $post["totalmts"];
            $totalpic = $post["totalpic"];
            $totalmts_arr_count = count($post["totalmts"]);
            $totalpic_arr_count = count($post["totalpic"]);
            $count_arr_count = count($post["count"]);
            $stockdetails_id_arr_count = count($post["stockid"]);
            $sizes_arr_count = count($post["sizes"]);
            $avg_arr = $post["avg"];           
            if($cutting->save()) {
                $cuttingid=$cutting->id;
                for ($x=0 ; $x < $sizes_arr_count; $x++) {
                    for ($y=0 ; $y < $stockdetails_id_arr_count; $y++) {
                            if($count_arr[$x][$y] != null)
                            {
                                $cd = new Cuttingdetail();
                                $cd->cutting_id = $cuttingid;
                                $cd->size_id = $sizes_arr[$x];
                                $cd->stockdetail_id = $stockdetails_id_arr[$y];
                                $cd->count = $count_arr[$x][$y];
                                $cd->avg = $avg_arr[$x];
                               $cd->save();
                            }
                        }
                    }
                  for ($k=0 ; $k < $stockdetails_id_arr_count; $k++) {
                          if($totalmts[$k] != null && $totalpic[$k] != null)
                          { 
                              $su = new StockUses();
                              $su->stockdetail_id = $stockdetails_id_arr[$k];
                              $su->used_mtrs = $totalmts[$k];
                              $su->total_pcs = $totalpic[$k];                                
                              $su->save();  
                          }                       
                    }
                    for ($p=0 ; $p < $sizes_arr_count; $p++) {                        
                        if($avg_arr[$p] != null)
                        {
                            $stockSizeAvg = new StockSizeAvg();
                            $stockSizeAvg->stock_id = $request->dno;
                            $stockSizeAvg->size_id = $sizes_arr[$p];                           
                            $stockSizeAvg->avg = $avg_arr[$p];                           
                            $stockSizeAvg->save();                                                     
                        }                   
                    }
              }
              return back()->with('success','Submit Successfullly');
              //$last_insert_id=$cutting->stock_id;
              //return redirect('stitching')->with('success','Cutting Information has been store'. $last_insert_id);
            }
            else
            {
              return redirect()->back()->with('error','Selected Size And Insert Atleast One !...');
            }
          }
        else{
          return redirect()->back()->with('error','Party And Design Number Already Exists');
        }
    }    
   public function showstitching()
    {
        $partydata = Party::get(); 
        $employee = Employee::get(); 
        $category = Fashion::get(); 
        return view('pages.stitching')->with('partydata',$partydata)->with('employee', $employee)->with('category',$category);
    }
    public function desingdata($id)
    {
        $stockdata = Stock::where('party_id',$id)->get();
        $data="<option value='' selected='true' disabled='true'>Select Design No.</option>";
        foreach ($stockdata as $key) {
            $data.= "<option value='$key->id'>$key->dno</option>";
        }      
        return $data;
    }   
    public function stockdetailsdata($stockid,$partyid)
    {
        $stockdata = Stockdetails::where('stock_id',$stockid)->get();        
      
        $data="";
        $i = 0;
        $data .= "<table class='table table-bordered mtable' id='cuttingtable'>";
        $data .= "<tr><th>Description</th><th>Total</th><th>Assigned</th><th>Remaining</th><th>Count</th>";
        foreach ($stockdata as $sd) {
            // echo "<pre>$sd->id"; 
            $cuttingdetaildata = Cuttingdetail::where('stockdetail_id',$sd->id)->get();
            $cuttingCount = Cuttingdetail::where('stockdetail_id',$sd->id)->sum('count');
            if($cuttingdetaildata)
            {
                foreach ($cuttingdetaildata as $key => $value) {
                    $assignedCount = StitchingAssignment::where('cuttingdetail_id', $value->id)->sum('count');
                    $sizeData = Size::where('id', $value->size_id)->first();
                    $remaining_pcs = $value->count - $assignedCount;
                    $data .= "<tr>
                    <input type='hidden' name='cdid[]' value='$value->id'/>
                    <td>$sd->color - $sd->length mts - size - $sizeData->size</td>
                    <td>$value->count pcs </td>
                    <td> $assignedCount pcs </td>
                    <td> $remaining_pcs pcs </td>";
                    $data.="<td>";
                        if($remaining_pcs==0)
                            $data.="<input type='number' readonly name='count[]' value='' class='form-control' max='$remaining_pcs'/>";
                        else
                            $data.="<input type='number' name='count[]' value='' class='form-control' max='$remaining_pcs'/>";
                    $data.="</td>";
                }
            }
        }
        $data .= "</table>";
        return $data;
    }

    public function storestitching(Request $request)
    {
        // dd($request);
        $cdid = $request->cdid;
        $count = $request->count;
        $empid = $request->employee;
        $due_date = $request->due_date;
        $category_name = $request->category_name;
        $rate = $request->rate;
        foreach ($cdid as $indx => $item) {
            if($count[$indx] != null)
            {
                $stitchAssign = new StitchingAssignment();
                $stitchAssign->cuttingdetail_id = $item;
                $stitchAssign->count = $count[$indx];
                $stitchAssign->rate = $rate;
                $stitchAssign->duedate = $due_date;
                $stitchAssign->fashion_id = $category_name;
                $stitchAssign->employee_id = $empid;
                $stitchAssign->save();
            }
        }
        return back()->with('success','Submit Successfullly');
    }
    public function stitchingreturns()
    {        
        $employee = Employee::get(); 
        $stockdata = Stock::with('party')->get();
        return view('pages.stitchingreturns')->with('employee', $employee)->with('stockdata', $stockdata);
    }
    public function showdesigndata($stockid,$Employeeid)
    {
        $StitchingData = StitchingAssignment::where('employee_id',$Employeeid)->get();
       
        $dno = DB::select( DB::raw("SELECT * FROM `stocks` WHERE id in (SELECT DISTINCT(stock_id) FROM `stockdetails` where id in (SELECT stockdetail_id FROM `cuttingdetails` WHERE id in(SELECT cuttingdetail_id FROM `stitching_assignments` WHERE `employee_id`=$Employeeid)) ORDER BY `stock_id` ASC)"));
      
        $data="";
        $i = 0;
        if($stockid == -1)
        {
            foreach ($dno as $indx => $stock) {
                $data .= "<h2>D.No.: $stock->dno</h2>";
                $data .= "<table class='table table-bordered mtable' id='cuttingtable'>";
                $data .= "<tr><th>Description</th><th>Total Assigned</th><th>Total Returned</th><th>Rate</th><th>Return Pcs Count</th>";
                $StockDetail = Stockdetails::where('stock_id', $stock->id)->get();
                foreach ($StockDetail as $sd) {
                    // $StockDetail = Stockdetails::with('stockdata')->where('stock_id', $stock->id)->where('id', $cuttingdetail->stockdetail_id)->first();
                    $cuttingdetail = Cuttingdetail::select('id')->where('stockdetail_id',$sd->id)->get();
                    $StitchingData = StitchingAssignment::where('employee_id',$Employeeid)->whereIn('cuttingdetail_id', $cuttingdetail )->get();
                    foreach ($StitchingData as $stchd) {
                        $returnedCount = StitchingReturn::where('employee_id',$Employeeid)->where('stitching_id', $stchd->id )->sum('received_count');
                        $maxCount = $stchd->count - $returnedCount;
                        $data .= "<tr>
                        <input type='hidden' name='sdid[]' value='$stchd->id'/>
                        <td>$sd->color - $sd->length mts</td>
                        <td>$stchd->count pcs </td>
                        <td>$returnedCount pcs </td>
                        <td>$stchd->rate RS. </td>";
                        if($stchd->count==$returnedCount)
                            $data.="<td><input type='number' name='count[]' readonly value='' class='form-control' max='$maxCount'/></td>";
                        else
                            $data.="<td><input type='number' name='count[]' value='' class='form-control' max='$maxCount'/></td>";
                        $data.="</tr>";
                    }
                }
                $data .= "</table>";
            }
        }
        else
        {
            $stock = Stock::where('id', $stockid)->first();
            $data .= "<h2>D.No.: $stock->dno</h2>";
            $data .= "<table class='table table-bordered mtable' id='cuttingtable'>";
            $data .= "<tr><th>Description</th><th>Total Assigned</th><th>Total Returned</th><th>Rate</th><th>Return Pcs Count</th>";
            $StockDetail = Stockdetails::where('stock_id', $stockid)->get();
            foreach ($StockDetail as $sd) {
                // $StockDetail = Stockdetails::with('stockdata')->where('stock_id', $stock->id)->where('id', $cuttingdetail->stockdetail_id)->first();
                $cuttingdetail = Cuttingdetail::select('id')->where('stockdetail_id',$sd->id)->get();
                $StitchingData = StitchingAssignment::where('employee_id',$Employeeid)->whereIn('cuttingdetail_id', $cuttingdetail )->get();
                foreach ($StitchingData as $stchd) {
                    $returnedCount = StitchingReturn::where('employee_id',$Employeeid)->where('stitching_id', $stchd->id )->sum('received_count');
                    $maxCount = $stchd->count - $returnedCount;
                    $data .= "<tr>
                    <input type='hidden' name='sdid[]' value='$stchd->id'/>
                    <td>$sd->color - $sd->length mts</td>
                    <td>$stchd->count pcs </td>
                    <td>$returnedCount pcs </td>
                    <td>$stchd->rate RS. </td>";
                    if($stchd->count==$returnedCount)
                        $data.="<td><input type='number' name='count[]' readonly value='' class='form-control' max='$maxCount'/></td>";
                    else
                        $data.="<td><input type='number' name='count[]' value='' class='form-control' max='$maxCount'/></td>";
                    $data.="</tr>";
                }
            }
            $data .= "</table>";
        }
        return $data;
    }  
    
    public function storestitchingreturns(Request $request)
    {     
      $this->validate($request, [
            'employee' => 'required',
            'dno' => 'required',
        ]);
        if($request->sdid!=null){
          $sdid = $request->sdid;
          $count = $request->count;
          $employee_id = $request->employee;
          $received_date = $request->received_date;
          foreach ($sdid as $index => $item) {
              if($count[$index] != null)
              { 
                  $StitchingReturn = new StitchingReturn();
                  $StitchingReturn->stitching_id = $item;
                  $StitchingReturn->received_count = $count[$index];
                  $StitchingReturn->received_date = $received_date;
                  $StitchingReturn->employee_id = $employee_id;
                  $StitchingReturn->paid_status = 0;
                  $StitchingReturn->save();
              }

          }
          return back()->with('success','Submit Successfullly');
        }
        return back()->with('error','Select Design Number And Stitching Return');
    }
    public function Advance()
    {        
        $employee = Employee::get(); 
        $advance = Advance::with('employee')->where('status',0)->get();
        return view('pages.loan')->with('employee', $employee)->with('advance', $advance);
    }
    public function stitchingassign()
    {   
        $stitchingassign = StitchingAssignment::get();        
        return view('reports.stitchingassign')->with('stitchingassign', $stitchingassign);
    }
    public function assignwork()
    {   
        $partydata = Party::get(); 
        $employee = Employee::get();
        return view('reports.assignwork')->with('partydata',$partydata)->with('employee',$employee);
    }
    public function getwork(Request $request)
    {   
        // dd($request);
        $data="";
        $totalAssignpcs=0;
        $totalreturnpcs=0;
        $totalremaming=0;
        $orderdate = explode('-', $request->daterange);
        $startdate=date('Y-m-d', strtotime($orderdate[0]));
        $enddate=date('Y-m-d', strtotime($orderdate[1])); 
        // if($request->daterange!=null && $request->party_id!=0 && $request->employee_id ==0) 
        // { 
        //    $stockdata = Stock::where('party_id',$request->party_id)->first();
        //    $stockdetaildata = Stockdetails::where('stock_id',$stockdata->id)->get();
        //    foreach ($stockdetaildata as $value ) 
        //        $cuttingdetaildata = Cuttingdetail::where('stockdetail_id',$value->id)->get();
        //         // dd($cuttingdetaildata);
        //             foreach ($cuttingdetaildata as $cutdetail)    
        //                 $workdata = StitchingAssignment::whereBetween('duedate',[$startdate, $enddate])->where('cuttingdetail_id',$cutdetail->id)->get();                                     
           
           
        // }
        if ($request->daterange!=null && $request->employee_id!=0 && $request->party_id == 0) {
            $workdata = StitchingAssignment::whereBetween('duedate',[$startdate, $enddate])->Where('employee_id',$request->employee_id)->get();        
        }
        else{            
            $workdata = StitchingAssignment::whereBetween('duedate',[$startdate, $enddate])->get();        
        }
            $data .= "<table class='table table-bordered mtable' id='cuttingtable'>";
            $data .= "<tr>
                        <th>Employee Name</th>
                        <th>Party Name</th>
                        <th>Design No / Color / Cloth Name</th>
                        <th>Total Assigned</th>
                        <th>Total Returned</th>
                        <th>Remaining Pices</th>
                        <th>Rate</th></tr>";                               
            foreach ($workdata as $stchd) {
                $employee = Employee::where('id',$stchd->employee_id)->first();
                $returnedCount = StitchingReturn::where('employee_id',$stchd->employee_id)->where('stitching_id', $stchd->id )->sum('received_count');
                $RemainingCount = $stchd->count - $returnedCount;
                $totalAssignpcs += $stchd->count;
                $totalreturnpcs += $returnedCount;
                $totalremaming = $totalAssignpcs - $totalreturnpcs;
                $data .= "<tr>
                <td>$employee->firstname $employee->lastname</td>
                <td>".$stchd->cuttingdetail->stockdetail->stock->party->firm_name."</td>
                <td>".$stchd->cuttingdetail->stockdetail->stock->dno. " / ". $stchd->cuttingdetail->stockdetail->color ." / ". $stchd->cuttingdetail->stockdetail->stock->cloth_name ."</td>
                <td>$stchd->count pcs </td>
                <td>$returnedCount pcs </td>
                <td>$RemainingCount pcs</td>
                <td>$stchd->rate RS. </td></tr>";
            }
            $data .= "<tr><td colspan='3'>Total</td><td>$totalAssignpcs pcs</td><td>$totalreturnpcs pcs</td><td>$totalremaming pcs</td><td></td></tr></table>";
            
            if($totalAssignpcs==0)
                return $data="No Records Found"; 
            else
                return $data; 
    }
    public function loanstore(Request $request)
    {
        // dd($request->all());
        $advance = new Advance();
        $advance->employee_id = $request->employee;
        $advance->amount = $request->amount;        
        $advance->status = 0;
        $advance->save();
        return back()->with('success','Submit Successfullly');
    }
    public function paidloan(Request $request)
    {
        $advance=Advance::where('employee_id',$request->employee_id)->update(['status' => 1]);        
        return back()->with('success','Submit Successfullly');
    } 
    public function editloan(Request $request)
    {
        $editdata = Advance::find($request->id);
        $employee = Employee::get();        
        return view('pages.editloan')->with('editdata', $editdata)->with('employee',$employee);
    }   
    public function loanupdate(Request $request,$id)
    {
        $updatedata= Advance::find($id);
        $updatedata->employee_id=$request->employee;
        $updatedata->amount=$request->amount;
        $updatedata->status=0;        
        $updatedata->save();
        return redirect('loan')->with('success','Information has been Updated');
    }
    public function editstitching()
    {
        $partydata = Party::get(); 
        $employee = Employee::get(); 
        $category = Fashion::get(); 
        return view('pages.editstitching')->with('partydata',$partydata)->with('employee', $employee)->with('category', $category);
    }
    public function getemployeedata(Request $request,$stockid,$employeeid)
    {
      // dd($stockid);
        $data="";
        $i = 0;
        $rateofdata = 0;
        $stockdata = Stockdetails::where('stock_id',$stockid)->get(); 
        $data .= "<table class='table table-bordered mtable' id='cuttingtable'>";
        $data .= "<tr><th>Description</th><th>Count</th>";
        foreach ($stockdata as $sd) {
            $cuttingdetaildata = Cuttingdetail::where('stockdetail_id',$sd->id)->first();
            $stitchingAssignment = StitchingAssignment::where('employee_id',$employeeid)->where('cuttingdetail_id', $cuttingdetaildata->id)->get();
            foreach ($stitchingAssignment as  $value) {                
            $assignedCount = StitchingAssignment::where('cuttingdetail_id', $cuttingdetaildata->id)->sum('count');
            $remaining_pcs = $cuttingdetaildata->count - $assignedCount;
            $rateofdata="<tr><td>Rate (Per Pics): </td><td><input type='text' id='rate' name='rate' value='$value->rate'></td></tr>"; 
            $data .= "<tr>
            <input type='hidden' name='said[]' value='$value->id'/>
            <input type='hidden' name='cdid[]' value='$cuttingdetaildata->id'/>
            <td>$sd->color - $sd->length mts</td>            
            <td><input type='number' name='count[]' value='$value->count' class='form-control'/></td>";
            }
        }
            $data .= "</table>";
            return $data.$rateofdata;
    }
    public function updatestitching(Request $request)
    {
        // dd($request); 
        if($request->cdid!=null){       
          $cdid = $request->cdid;
          $count = $request->count;
          $empid = $request->employee;
          $due_date = $request->due_date;
          $rate = $request->rate;
          foreach ($request->said as $key => $value) {
              foreach ($cdid as $indx => $item) {
                  if($count[$indx] != null)
                  {
                      //$stitchAssign= StitchingAssignment::where('id',$value)->where('employee_id',$empid)->where('cuttingdetail_id',$item)->first();
                      $stitchAssign= StitchingAssignment::where('cuttingdetail_id',$item)->first();
                      $stitchAssign->cuttingdetail_id = $item;
                      $stitchAssign->count = $count[$indx];
                      $stitchAssign->rate = $rate;
                      $stitchAssign->duedate = $due_date;
                      $stitchAssign->save();
                  }
              }
          }
          return back()->with('success','Update Successfullly');
        }
        else{
          return back()->with('error','Error Not Updated Data !...');
        }
    }
    public function editdata($id)
    {
        $stockdata = Stock::where('party_id',$id)->get();
        $data="<option value='' selected='true' disabled='true'>Select Design No.</option>";
        foreach ($stockdata as $key) {
            $data.= "<option value='$key->id'>$key->dno "." - ".$key->party->firm_name."</option>";
        }      
        return $data;
    }
    public function editstitchdata($stockid,$partyid)
    {       
        $data="";
        $i = 0;
        $stockdata = Stockdetails::where('stock_id',$stockid)->get();        
        $data .= "<table class='table table-bordered mtable' id='cuttingtable'>";
        $data .= "<tr><th>Description</th><th>Total</th><th>Assigned</th><th>Remaining</th><th>Count</th>";
        foreach ($stockdata as $sd) {
            $cuttingdetaildata = Cuttingdetail::where('stockdetail_id',$sd->id)->first();
            $assignedCount = StitchingAssignment::where('cuttingdetail_id', $cuttingdetaildata->id)->sum('count');
            $remaining_pcs = $cuttingdetaildata->count - $assignedCount;
            $data .= "<tr>
            <input type='hidden' name='cdid[]' value='$cuttingdetaildata->id'/>
            <td>$sd->color - $sd->length mts</td>
            <td>$cuttingdetaildata->count pcs </td>
            <td> $assignedCount pcs </td>
            <td> $remaining_pcs pcs </td>
            <td><input type='number' name='count[]' value='' class='form-control' max='$remaining_pcs'/></td>";
        }
        $data .= "</table>";
        return $data;
    }
    public function finishing(Request $request){
        $employee = Employee::where('category','finishing')->get(); 
        $stockdata = Stock::with('party')->get();
        return view('finishing.index')->with('employee', $employee)->with('stockdata', $stockdata);        
    }    

    public function stitching_return_data($stockid)
    {
        
        $data="";
        $i = 0;        
        $stock = Stock::where('id', $stockid)->first();
        $data .= "<h2>D.No.: $stock->dno </h2>";
        $data .= "<table class='table table-bordered mtable'>";
        $data .= "<tr><th>Description</th><th>Total Returned</th><th>Finish Pcs</th>";
        $StockDetail = Stockdetails::where('stock_id', $stockid)->get();
        foreach ($StockDetail as $sd) {
            $cuttingdetail = Cuttingdetail::select('id')->where('stockdetail_id',$sd->id)->get();
            $StitchingData = StitchingAssignment::whereIn('cuttingdetail_id', $cuttingdetail )->get();
            foreach ($StitchingData as $stchd) {
                $stitchreturned = StitchingReturn::where('employee_id',$stchd->employee_id)->where('stitching_id', $stchd->id )->get();
                foreach ($stitchreturned as $stchreturn) {
                    $finish = Finishing::where('stitching_return_id', $stchreturn->id )->where('status',0)->sum('count');                    
                    $maxCount = $stchreturn->received_count - $finish;
                    $data .= "<tr>
                    <input type='hidden' name='sdid[]' value='$stchreturn->id'/>
                    <td>$sd->color - $sd->length mts</td>
                    <td>$stchreturn->received_count pcs </td>";
                    if($stchreturn->received_count == $finish)
                        $data.="<td><input type='number' readonly max='$maxCount' name='count[]' value='' class='form-control'/></td>";
                    else
                        $data.="<td><input type='number' max='$maxCount' name='count[]' value='' class='form-control'/></td>";
                    "</tr>";
                }
            }
        }
        $data .= "</table>";
        return $data;
    }
    public function finishing_store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'employee' => 'required',
            'dno' => 'required',
        ]);
        if($request->sdid!=null){
          $sdid = $request->sdid;
          $count = $request->count;
          $employee_id = $request->employee;
          $due_date = $request->due_date;
          $rate = $request->rate;
          foreach ($sdid as $index => $item) {
              if($count[$index] != null)
              { 
                  $finish = new Finishing();
                  $finish->stitching_return_id = $item;
                  $finish->count = $count[$index];
                  $finish->due_date = $due_date;
                  $finish->rate = $rate;
                  $finish->employee_id = $employee_id;
                  $finish->status = 0;
                  $finish->save();
              }

          }
          return back()->with('success','Submit Successfullly');
        }
        return back()->with('error','Select Design Number');
    }
    public function finishing_return(Request $request){
        $employee = Employee::where('category','finishing')->get(); 
        $stockdata = Stock::with('party')->get();
        return view('finishing.finishing_return')->with('employee', $employee)->with('stockdata', $stockdata);        
    }
    public function finishing_data($stockid)
    {   
        
        $data="";
        $i = 0;        
        $stock = Stock::where('id', $stockid)->first();
        $data .= "<h2>D.No.: $stock->dno </h2>";
        $data .= "<table class='table table-bordered mtable'>";
        $data .= "<tr><th>Description</th><th>Assign</th><th>Total Returned</th><th>Finish Pcs</th>";
        $StockDetail = Stockdetails::where('stock_id', $stockid)->get();
        foreach ($StockDetail as $sd) {
            $cuttingdetail = Cuttingdetail::select('id')->where('stockdetail_id',$sd->id)->get();
            $StitchingData = StitchingAssignment::whereIn('cuttingdetail_id', $cuttingdetail )->get();
            foreach ($StitchingData as $stchd) {
                $stitchreturned = StitchingReturn::where('stitching_id', $stchd->id )->get();
                foreach ($stitchreturned as $stchreturn) {
                    $finish = Finishing::where('stitching_return_id', $stchreturn->id )->where('status',0)->get();                    
                    foreach ($finish as $finishdata) {
                        $countFR = FinishingReturn::where('employee_id',$finishdata->employee_id)->where('finishing_id', $finishdata->id )->sum('received_count');                    
                        $maxCount = $finishdata->count-$countFR;
                        $data .= "<tr>
                        <input type='hidden' name='fdid[]' value='$finishdata->id'/>
                        <td>$sd->color - $sd->length mts</td>
                        <td>$finishdata->count pcs </td>
                        <td>$countFR pcs</td>";
                        if($finishdata->count == $countFR)
                            $data.="<td><input type='number' readonly max='$maxCount' name='received_count[]' value='' class='form-control'/></td>";
                        else
                            $data.="<td><input type='number' max='$maxCount' name='received_count[]' value='' class='form-control'/></td>";
                        "</tr>";
                        
                    }
                }
            }
        }
        $data .= "</table>";
        return $data;
    }
    public function finish_return_store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'employee' => 'required',
            'dno' => 'required',
        ]);
        if($request->fdid!=null){
          $fdid = $request->fdid;
          $received_count = $request->received_count;
          $employee_id = $request->employee;
          $received_date = $request->received_date;
          foreach ($fdid as $index => $item) {
              if($received_count[$index] != null)
              { 
                  $finish = new FinishingReturn();
                  $finish->finishing_id = $item;
                  $finish->received_count = $received_count[$index];
                  $finish->received_date = $received_date;
                  $finish->employee_id = $employee_id;
                  $finish->status = 0;
                  if($finish->save())
                  {
                    $finish_check = Finishing::where('id',$finish->finishing_id)->where('status',0)->first();
                    if($finish_check->count==$finish->received_count){
                        $finish_check->status = 1;
                        $finish_check->save();
                    }
                  }
              }
          }
          return back()->with('success','Submit Successfullly');
        }
        return back()->with('error','Select Design Number');
    }
    public function getfinishing(Request $request)
    {   
        $finishing = Finishing::get(); 
        $finishing_return = FinishingReturn::get();
        return view('reports.finishing')->with('finishing', $finishing)->with('finishing_return', $finishing_return);        
    }
}
