<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Employee;
use App\Party;
use App\Stock;
use App\Stockdetails;
use App\Size;

class ApiPartyController extends Controller
{
    public function index()
    {
    	$data = Party::all();
        return response()->json(['data'=>$data]);
    }

    public function create(Request $request)
    {
    	$request->validate([
    		'firm_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3',
            'firstname'  => 'required|alpha|min:3',
            'lastname'  => 'required|alpha|min:3',
            'email'  => 'nullable|email',
            'phone'  => 'nullable|digits:10',
            'gst'  => 'nullable|string|min:15|max:15',
            'address'  => 'required|alpha|min:3',
        ]);

        $data = Party::create($request->all());
        return response()->json(['status'=>'success', 'data'=>$data]);
    }

    public function edit($id)
    {
    	$data = Party::find($id);
    	return response()->json(['data'=>$data]);
    }

    public function update($id, Request $request)
    {
    	$data = Party::find($id);
    	// dd($data);
    	if($request)
    	{
    		$request->validate([
                'firm_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3',
                'firstname'  => 'required|alpha|min:3',
                'lastname'  => 'required|alpha|min:3',
                'email'  => 'nullable|email',
                'phone'  => 'nullable|digits:10',
                'gst'  => 'nullable|string|min:15|max:15',
                'address'  => 'required|alpha|min:3',
            ]);
	    	$data->firm_name = $request->firm_name;
	    	$data->firstname = $request->firstname;
	    	$data->lastname = $request->lastname;
	    	$data->email = $request->email;
	    	$data->phone = $request->phone;
	    	$data->address = $request->address;
	    	$data->gst = $request->gst;
	        $data->save();
	        return response()->json(['status'=>'success', 'data'=>$data]);
    	}
    	return response()->json(['data'=>$data]);
    }

    public function delete($id)
    {
    	$data = Party::find($id);
    	$data->delete();
    	return response()->json(['data'=>$data]);
    }
}
