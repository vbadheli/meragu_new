<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoices;
use App\InvoiceItemDetails;
use App\Party;
use App\Stock;
use App\Stockdetails;
use App\PartyPayments;
class InvoiceController extends Controller
{
    
    public function index(Request $request)
    {

        $allInvoices = Invoices::all();
        $partydata = Party::get();

        if($request->has('party_id') && $request->party_id !="" && $request->has('payment_status') && $request->payment_status != "")
        {
           $allInvoices = Invoices::where('party_id', $request->party_id)->where('payment_status', $request->payment_status)->get();
        }
        else if($request->has('party_id') && $request->party_id !="" )
        {
           $allInvoices = Invoices::where('party_id', $request->party_id)->get();
        }
        else if($request->has('payment_status')  && $request->payment_status != "")
        {
    	   $allInvoices = Invoices::where('payment_status', $request->payment_status)->get();
        }

    	return view('invoices.index', compact('allInvoices','partydata'));
    }

    public function createInvoice(Request $request){
       
        $invoice = new Invoices();
        $invoice->party_id = $request->invoice['party_id'];
        $invoice->invoice_number = $request->invoice['invoice_number'];
        $invoice->igst = $request->invoice['igst'];
        $invoice->sgst = $request->invoice['sgst'];
        $invoice->cgst = $request->invoice['cgst'];
        $invoice->advance_payment = $request->invoice['adv'];
        $invoice->shipping = $request->invoice['shipping'];
        $invoice->total = floatval(preg_replace("/[^-0-9\.]/","",$request->invoice['final_total_input']));
        // dd($invoice);
        if($invoice->save())
        {
            foreach ($request->item as $key => $invoiceItem) {
                $invoiceItemDetails = new InvoiceItemDetails();
                $ar = explode(":", $invoiceItem['stockdetail_id']);
        // dd($ar);
                $invoiceItemDetails->stockdetail_id = $ar[0];
                $invoiceItemDetails->cuttingdetails_id = $ar[1];
                $invoiceItemDetails->quantity = $invoiceItem['quantity'];
                $invoiceItemDetails->rate = $invoiceItem['rate'];
                $invoiceItemDetails->invoice_id = $invoice->id;
                $invoiceItemDetails->save();
            }
        }
        return redirect()->route("invoice-home");
    }

    public function print($id){
        $invoice = Invoices::find($id);
        $type = "stream";

        $pdf = app('dompdf.wrapper')->loadView('invoices.print', ['invoice' => $invoice]);

        if ($type == 'stream') {
            return $pdf->stream('invoice.pdf');
        }

        if ($type == 'download') {
            return $pdf->download('invoice.pdf');
        }

        return view('invoices.print', compact('invoice'));
    }
    public function original_bill_print($id){
        $invoice = Invoices::find($id);
        $type = "stream";

        $pdf = app('dompdf.wrapper')->loadView('invoices.original_bill_print', ['invoice' => $invoice]);

        if ($type == 'stream') {
            return $pdf->stream('original_bill_print.pdf');
        }

        if ($type == 'download') {
            return $pdf->download('original_bill_print.pdf');
        }

        return view('invoices.original_bill_print', compact('invoice'));
    }
    public function duplicate_transport_bill($id){
        $invoice = Invoices::find($id);
        $type = "stream";

        $pdf = app('dompdf.wrapper')->loadView('invoices.duplicate_bill_transport_print', ['invoice' => $invoice]);

        if ($type == 'stream') {
            return $pdf->stream('duplicate_transport_bill.pdf');
        }

        if ($type == 'download') {
            return $pdf->download('duplicate_transport_bill.pdf');
        }

        return view('invoices.duplicate_bill_transport_print', compact('invoice'));
    }

    public function update($id){}
    public function delete($id){}
    public function updateInvoice(){}

    public function create()
    {
    	$allParty = Party::all();
    	$allStock = Stock::all();
    	$allStockDetails = Stockdetails::all();
    	return view('invoices.create-invoice', compact("allParty","allStock","allStockDetails") );
    }

    public function getStockDetails(Request $request)
    {
    	$stock = Stock::find($request->stock_id);
    	$stockDetails = $stock->stockdetails;
    	$cuttingDetails = $stock->stockdetails;
    	$options = "<option>Select Item</option>";
    	foreach ($stockDetails as $key => $value) {
    		foreach ($value->cuttingdetails as $ckey => $cvalue) {
    			$sizename = $cvalue->size->size;
    			$options .= "<option >$value->color : $value->length mtrs: size $sizename</option>";
    		}
    	}
    	echo $options;
    }

    public function makePayment(Request $request)
    {
        if($request->has('id'))
        {
            $invoice = Invoices::find($request->id);
            return view('invoices.party-payment', ['id'=>$request->id,'invoice'=>$invoice]);
        }
        return redirect()->back();
    }

    public function paymentHistory($partyid)
    {
        $invoices = Invoices::where('party_id', $partyid)->get();
        return view('invoices.payment-history', compact('invoices'));

    }

    public function invoicePaymentHistory($invoiceId)
    {
        $invoice = Invoices::find($invoiceId);
        return view('invoices.invoice-payment-history', compact('invoice'));

    }

    public function invoicePayment(Request $request)
    {
        // dd($request->all());
        $pp = new PartyPayments();
        $pp->amount = $request->amount;
        $pp->remarks = $request->remarks;
        $pp->invoice_id = $request->invoice_id;
        // dd($pp);
        $pp->save();        
        return redirect()->back();
        // return redirect()->route('payment-history',1);
    }
}
