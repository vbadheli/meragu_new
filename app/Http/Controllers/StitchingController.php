<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Party;
use App\Stock;
use App\Fashion;
use App\Size;
use App\Employee;
use App\Advance;
use App\EmployeePayment;

class StitchingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->party_insert_id!=null && $request->stock_insert_id!=null){
            $partydata = Party::where('id',$request->party_insert_id)->get();
            $allStock = Stock::where('id',$request->stock_insert_id)->get();
        }
        else{
            $partydata = Party::get();
            $allStock = Stock::get();
        }
        $getsizes = Size::get();
        $category = Fashion::all();   

        return view('stitching.index')->with('partydata',$partydata)->with('getsizes', $getsizes)->with('allStock', $allStock)->with('category', $category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function getFixed(Request $request)
    {
        $data="";
        $employee = Employee::where('category','helper')->get(); 
        $employee_payshit = EmployeePayment::where('category','fixed')->get(); 
        return view('pages.fixedsalary')->with('employee',$employee)->with('employee_payshit',$employee_payshit);
    }
    public function getpayment(Request $request,$empid)
    {
        $data = Employee::where('id',$empid)->first(); 
        $adv_amt = Advance::where('employee_id',$data->id)->where('status',0)->sum('amount'); 
        return $data->daily_payment;
    }
    public function fixed_store(Request $request)
    { 
        $six_digit_random_number = mt_rand(100000, 999999);
        $fixed= new EmployeePayment();
        $fixed->paymentsheet_id = $six_digit_random_number;        
        $fixed->employee_id = $request->empid;        
        $fixed->daily_amount=$request->payment_amount;
        $fixed->attendance_days=$request->attendance_days;
        $fixed->absent_days=$request->absent_days;
        $fixed->advance_cut=$request->advance_cut;
        $fixed->payable_amount=$request->total_amount;        
        $fixed->category = 'fixed';
        $fixed->save();
        return redirect('fixedsalary')->with('success','Information has been Submit Successfully !');
    }
}
