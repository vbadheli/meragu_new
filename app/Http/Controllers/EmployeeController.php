<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Party;
use App\Stock;
use App\Stockdetails;
use App\Size;
use App\Fashion;
use App\StitchingAssignment;
use App\StitchingReturn;
use App\Advance;
use App\EmployeePayment;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(array(
                // 'firm_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3',
                'firstname'  => 'required|alpha|min:3',
                'lastname'  => 'required|alpha|min:3',
                'email'  => 'nullable|email',
                'category'  => 'required|alpha',
                // 'gst'  => 'nullable|string|min:15|max:15',
                'phone'  => 'nullable|digits:10',
                'address'  => 'required|min:3',
            )
        );

        $data =Employee::create($request->all());
        return back()->with('success','Submit Successfullly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $Employee=Employee::all();
        return view('pages.employee')->with('Employee', $Employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Employee::find($id);
        return view('pages.editemployee')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(array(
                // 'firm_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3',
                'firstname'  => 'required|alpha|min:3',
                'lastname'  => 'required|alpha|min:3',
                'email'  => 'nullable|email',
                // 'gst'  => 'nullable|string|min:15|max:15',
                'phone'  => 'nullable|digits:10',
                'address'  => 'required|min:3',
            )
        );
        $updatedata= Employee::find($id);
        $updatedata->firstname=$request->firstname;
        $updatedata->lastname=$request->lastname;
        $updatedata->email=$request->email;
        $updatedata->phone=$request->phone;
        $updatedata->address=$request->address;
        $updatedata->daily_payment=$request->daily_payment;
        $updatedata->category = $request->category;
        $updatedata->save();
        return redirect('employee')->with('success','Information has been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        return redirect('employee')->with('danger','Information has been deleted');
    }
    public function EmployeeCount()
    {
        $empcount = Employee::count();        
        $partycount = Party::count();        
        $sumStock = Stockdetails::sum('length');
        return view('layout.home')->with('empcount',$empcount)->with('partycount',$partycount)->with('sumStock',$sumStock);
    }
    public function showdata()
    {
        $sizes=Size::all();
        return view('pages.size')->with('sizes', $sizes);
    }
    public function insert(Request $request)
    {
        $this->validate($request, [
            'size' => 'required'
        ]);

        $data =Size::create($request->all());
        return back()->with('success','Submit Successfullly');
    }
    public function sizeedit($id)
    {
        $data = Size::find($id);
        return view('pages.editsize')->with('data',$data);
    }
    public function sizeupdate(Request $request, $id)
    {
        $updatedata= Size::find($id);
        $updatedata->size=$request->size;        
        $updatedata->save();
        return redirect('size')->with('success','Information has been Updated');
    }
    public function delete($id)
    {
        $size = Size::find($id);
        $size->delete();
        return redirect('size')->with('danger','Information has been deleted');
    }
    public function categoryindex()
    {
         $category=Fashion::all();        
        return view('category.category')->with('category',$category);
    }
    public function storecategory(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $data =Fashion::create($request->all());
        return back()->with('success','Submit Successfullly');
    }
    public function categoryedit($id)
    {
        $data = Fashion::find($id);        
        return view('category.editcategory')->with('data',$data);
    }
    public function categoryupdate(Request $request, $id)
    {
        $updatedata= Fashion::find($id);
        $updatedata->name=$request->name;        
        $updatedata->save();
        return redirect('category')->with('success','Information has been Updated');
    }
    public function deletecategory($id)
    {
        $category = Fashion::find($id);
        $category->delete();
        return redirect('category')->with('danger','Information has been deleted');
    }
    public function employeedetails($empid)
    {
        $employee_details = Employee::where('id', $empid)->get();
        $advance = Advance::where('employee_id',$empid)->get();
        foreach ($advance as  $value) {
            $cuttingadv = EmployeePayment::where('employee_id',$value->employee_id)->sum('advance_cut');        
        }
        $EmployeePayment = EmployeePayment::where('employee_id',$empid)->orderBy('created_at', 'DESC')->limit(5)->get();       
        $stichassign = StitchingAssignment::where('employee_id',$empid)->orderBy('duedate', 'DESC')->limit(10)->get();       
        return view('reports.employee-details', compact('employee_details','stichassign','advance','cuttingadv','EmployeePayment'));

    }
}
