<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Party;
use App\Invoices;

class PartyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(array(
                'firm_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3',
                'firstname'  => 'required|alpha|min:3',
                'lastname'  => 'required|alpha|min:3',
                'email'  => 'nullable|email',
                'gst'  => 'nullable|string|min:15|max:15',
                'phone'  => 'nullable|digits:10',
                'address'  => 'required|min:3',
            )
        );

        $data = Party::create($request->all());

        return back()->with('success','Submit Successfullly');
    }
    public function storeAjax(Request $request)
    {
        $request->validate(array(
                'firm_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3',
                'firstname'  => 'required|alpha|min:3',
                'lastname'  => 'required|alpha|min:3',
                'email'  => 'nullable|email',
                'gst'  => 'nullable|string|min:15|max:15',
                'phone'  => 'nullable|digits:10',
                'address'  => 'required|min:3',
            )
        );

        $data = Party::create($request->all());

        echo json_encode(['status'=>1, 'message'=>"Party Added Successfully", 'data'=>$data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $partydata = Party::get();  
        return view('pages.partys')->with('partydata', $partydata);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editdata = Party::find($id);
        return view('pages.editparty')->with('editdata',$editdata);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(array(
                'firm_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3',
                'firstname'  => 'required|alpha|min:3',
                'lastname'  => 'required|alpha|min:3',
                'email'  => 'nullable|email',
                'gst'  => 'nullable|string|min:15|max:15',
                'phone'  => 'nullable|digits:10|required',
                'address'  => 'required|min:3',
            )
        );
        $updatedata= Party::find($id);
        $updatedata->firm_name=$request->firm_name;
        $updatedata->gst=$request->gst;
        $updatedata->firstname=$request->firstname;
        $updatedata->lastname=$request->lastname;
        $updatedata->email=$request->email;
        $updatedata->phone=$request->phone;
        $updatedata->address=$request->address;
        $updatedata->save();
        return redirect('party')->with('success','Information has been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $party = Party::find($id);
        $party->delete();
        return redirect('party')->with('danger','Information has been deleted');
    }
    public function partydetails($partyid)
    {
        $party_details = Party::where('id', $partyid)->get();
        $invoices = Invoices::where('party_id', $partyid)->get();
        return view('reports.party-details', compact('party_details','invoices'));

    }
}
