<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    private $token;
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {        
        if(auth()->user()==null)
        {           
            $user = User::where('username', $request->username)->where('password', $request->password)->where('active',1)->first();            
            if($user)
            {
               
                $this->token = auth()->login($user);
                if($user->role=="administrator")
                {
                    $user->last_login = now();
                    $user->save();
                    return redirect('/home');
                }                

            }
            else
            {
                return redirect('/')->with('message', 'Invalid account !');;
                
            }
        }
        else
        {
            return redirect('/');
        }
    }
    

    public function logout()
    {
        auth()->logout();
        return redirect('/');
    }
}
