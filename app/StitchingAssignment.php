<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StitchingAssignment extends Model
{
    public function cuttingdetail()
    {
        return $this->belongsTo('App\Cuttingdetail');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
    
}
