<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItemDetails extends Model
{
    
    public function invoice()
    {
        return $this->belongsTo('App\Invoices');
    }

    public function stockdetail()
    {
        return $this->belongsTo('App\Stockdetails');
    }

    public function cuttingdetail()
    {
        return $this->belongsTo('App\Cuttingdetail','cuttingdetails_id');
    }
}
