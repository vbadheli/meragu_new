<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
        'party_id','date','dno','challan_no', 'panna','cloth_name','image'
    ];
    public function party()
    {
        return $this->belongsTo('App\Party');
    }

    public function stockdetails()
    {
        return $this->hasMany('App\Stockdetails');
    }
    public function category()
    {
        return $this->belongsTo('App\Stock');
    }
}
