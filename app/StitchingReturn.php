<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StitchingReturn extends Model
{
    public function stitchingAssign()
    {
        return $this->hasMany('App\StitchingAssignment','id','stitching_id');
    }
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
    public function stitcAss()
    {
        return $this->belongsTo('App\StitchingAssignment','stitching_id','id');
    }
}
