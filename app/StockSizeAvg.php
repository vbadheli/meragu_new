<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockSizeAvg extends Model
{
    public function stock()
    {
        return $this->belongsTo('App\Stock');
    }
}
