<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Party extends Model
{
	private $errors;

    protected $fillable = [
        'firm_name','firstname','lastname','email', 'phone','address','gst',
    ];

    private $rules = array(
        'firm_name' => 'required|alpha|min:3',
        'firstname'  => 'required',
        'lastname'  => 'required',
        'email'  => 'required',
        'phone'  => 'required',
        'address'  => 'required',
        // .. more rules here ..
    );
    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->errors;
            return false;
        }
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }
}
