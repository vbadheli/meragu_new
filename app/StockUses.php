<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockUses extends Model
{
    public function stockdetail()
    {
        return $this->belongsTo('App\Stockdetails');
    }
}
