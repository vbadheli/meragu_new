<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cutting extends Model
{
    protected $fillable = [
        'party_id','dno'
    ];

    public function party()
    {
        return $this->belongsTo('App\Party');
    }

    public function stock()
    {
        return $this->belongsTo('App\Stock');
    }

    public function cuttingdetail()
    {
        return $this->hasMany('App\Cuttingdetail');
    }

    
}
