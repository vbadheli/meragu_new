<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartyPayments extends Model
{
    //

    public function party()
    {
        return $this->belongsTo('App\Party');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Invoices');
    }
}
