<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layout.home');
// });
Route::middleware(['guest'])->group(function(){
	Route::post('/login', 'Auth\LoginController@login');

	Route::get('/', function () {
	    return view('login');
	})->name('login');

});
Route::middleware(['auth'])->group(function(){

	Route::post('/logout', 'Auth\LoginController@logout');
	Route::get('/home', ['as' => 'layout.home', 'uses' => 'EmployeeController@EmployeeCount']);
	Route::get('/party', ['as' => 'pages.partys', 'uses' => 'PartyController@show']);

Route::get('/edit/{id}', ['as' => 'pages.editparty', 'uses' => 'PartyController@edit']);
Route::post('/update/{id}', ['as' => 'pages.editparty', 'uses' => 'PartyController@update']);
Route::delete('/destroy/{id}', ['as' => 'pages.partys', 'uses' => 'PartyController@destroy']);
Route::post('/store', ['as' => 'pages.partys', 'uses' => 'PartyController@store']);
Route::post('/store-ajax', ['uses' => 'PartyController@storeAjax']);

Route::get('/employee', ['as' => 'pages.employee', 'uses' => 'EmployeeController@show']);
Route::get('/employee/edit/{id}', ['as' => 'pages.editemployee', 'uses' => 'EmployeeController@edit']);
Route::post('/employee/update/{id}', ['as' => 'pages.editemployee', 'uses' => 'EmployeeController@update']);
Route::delete('/employee/destroy/{id}', ['as' => 'pages.employee', 'uses' => 'EmployeeController@destroy']);
Route::post('/employee/store', ['as' => 'pages.employee', 'uses' => 'EmployeeController@store']);

Route::get('/stock', ['as' => 'stockpage.stock', 'uses' => 'StockController@index'])->middleware("auth");
Route::post('/stock/store', ['as' => 'stockpage.stock', 'uses' => 'StockController@store']);
Route::get('/editstock', ['as' => 'stockpage.editstock', 'uses' => 'StockController@editstock'])->middleware("auth");
Route::get('/viewstockdata/{id}', ['as' => 'stockpage.editstock', 'uses' => 'StockController@viewstockdata']);
Route::get('/editstockdetails/{stockid}/{partyid}', ['as' => 'stockpage.editstock', 'uses' => 'StockController@editdetails']);
Route::post('/stock/edit/store', ['as' => 'stockpage.editstock', 'uses' => 'StockController@updatestock']);

Route::get('/report/stock', ['as' => 'reports.stock', 'uses' => 'StockController@show']);
Route::get('/report/stock/detail/{stock_id}', ['as' => 'reports.stock', 'uses' => 'StockController@stockdetailsajax']);
Route::get('/report/party/show', ['as' => 'reports.party', 'uses' => 'StockController@partyshow']);
Route::get('/report/employee/show', ['as' => 'reports.employee', 'uses' => 'StockController@employeeshow']);
Route::get('/report/size/show', ['as' => 'reports.size', 'uses' => 'StockController@sizeshow']);
Route::get('/report/employee/paymentsheet', ['as' => 'reports.paymentsheet', 'uses' => 'StockController@paymentsheetshow']);
Route::get('/report/employee/paymentdata/{empid}', ['as' => 'reports.paymentsheet', 'uses' => 'StockController@paymentdata']);
Route::post('/report/employee/store', ['as' => 'reports.paymentsheet', 'uses' => 'StockController@storepayment']);
Route::get('/report/employee/weeklypayment', ['as' => 'reports.weeklypayment', 'uses' => 'StockController@weeklypayment']);
Route::post('/report/employee/employeepayment', ['as' => 'reports.weeklypayment', 'uses' => 'StockController@getemployeepayment']);
Route::get('/report/employee/employee_pay_shit', ['as' => 'reports.employee_pay_shit', 'uses' => 'StockController@employee_pay_shit']);
Route::post('/report/employee/pay_shit', ['as' => 'reports.employee_pay_shit', 'uses' => 'StockController@getpay_shit']);
Route::get('/report/stitching/assign', ['as' => 'reports.stitchingassign', 'uses' => 'StockController@stitchingassign']);
Route::get('/report/stitching/assignwork', ['as' => 'reports.assignwork', 'uses' => 'StockController@assignwork']);
Route::post('/report/stitching/work', ['as' => 'reports.assignwork', 'uses' => 'StockController@getwork']);
Route::get('/report/finishing', ['as' => 'reports.finishing', 'uses' => 'StockController@getfinishing']);

Route::get('/size', ['as' => 'pages.size', 'uses' => 'EmployeeController@showdata']);
Route::post('/create', ['as' => 'pages.size', 'uses' => 'EmployeeController@insert']);
Route::get('/size/edit/{id}', ['as' => 'pages.size', 'uses' => 'EmployeeController@sizeedit']);
Route::post('/size/update/{id}', ['as' => 'pages.size', 'uses' => 'EmployeeController@sizeupdate']);
Route::delete('/size/destroy/{id}', ['as' => 'pages.size', 'uses' => 'EmployeeController@delete']);

Route::get('/category', ['as' => 'category.category', 'uses' => 'EmployeeController@categoryindex']);
Route::post('/category/create', ['as' => 'category.category', 'uses' => 'EmployeeController@storecategory']);
Route::get('/category/edit/{id}', ['as' => 'category.editcategory', 'uses' => 'EmployeeController@categoryedit']);
Route::post('/category/update/{id}', ['as' => 'category.editcategory', 'uses' => 'EmployeeController@categoryupdate']);
Route::delete('/category/destroy/{id}', ['as' => 'category.category', 'uses' => 'EmployeeController@deletecategory']);

// Route::get('/cutting', ['as' => 'pages.cutting', 'uses' => 'StockController@datashow']);
Route::get('/cutting', ['as' => 'loadcutting', 'uses' => 'CuttingController@index']);
Route::get('/cutting/list', ['as' => 'loadcuttinglist', 'uses' => 'CuttingController@showList']);
Route::post('/cutting/store', ['as' => 'pages.cutting', 'uses' => 'StockController@insert']);
Route::get('/view/{id}', ['as' => 'pages.cutting', 'uses' => 'StockController@view']);
Route::get('/viewstockdetails/{stockid}/{partyid}', ['as' => 'pages.cutting', 'uses' => 'StockController@viewstockdetails']);
Route::post('/cutting/getStockDetails', ['as' => 'stock.cutting', 'uses' => 'CuttingController@getStockDetails']);
Route::get('/cutting/edit', ['as' => 'cuttings.editcutting', 'uses' => 'CuttingController@editcutting']);
Route::get('/cutting/view/{id}', ['as' => 'cuttings.cutting', 'uses' => 'CuttingController@view']);
Route::get('/cutting/viewstockdetails/{stockid}/{partyid}', ['as' => 'cuttings.cutting', 'uses' => 'CuttingController@viewstockdetails']);
Route::post('cutting/destroy/{id}', ['as'=>'cuttings.cutting','uses'=>'CuttingController@destroy']);
Route::post('cutting/update', ['as'=>'cuttings.cutting','uses'=>'CuttingController@update']);

Route::get('/stitching', ['as' => 'stitching', 'uses' => 'StockController@showstitching']);
Route::post('/stitching/store', ['as' => 'pages.stitching', 'uses' => 'StockController@storestitching']);
Route::get('/desingdata/{id}', ['as' => 'pages.stitching', 'uses' => 'StockController@desingdata']);
Route::get('/stockdetailsdata/{stockid}/{partyid}', ['as' => 'pages.stitching', 'uses' => 'StockController@stockdetailsdata']);


Route::get('/editstitching', ['as' => 'pages.editstitching', 'uses' => 'StockController@editstitching']);
Route::get('/editdata/{id}', ['as' => 'pages.editstitching', 'uses' => 'StockController@editdata']);
Route::get('/getemployeedata/{stockid}/{employeeid}', ['as' => 'pages.editstitching', 'uses' => 'StockController@getemployeedata']);
Route::get('/editstitchdata/{stockid}/{partyid}', ['as' => 'pages.editstitching', 'uses' => 'StockController@editstitchdata']);
Route::post('/editstitchdata/update', ['as' => 'pages.editstitching', 'uses' => 'StockController@updatestitching']);

Route::get('/stitchingreturns', ['as' => 'pages.stitchingreturns', 'uses' => 'StockController@stitchingreturns']);
Route::get('/EmployeeData/{empid}', ['as' => 'pages.stitchingreturns', 'uses' => 'StockController@EmployeeData']);
Route::get('/showdesigndata/{empid}/{stockid}', ['as' => 'pages.stitchingreturns', 'uses' => 'StockController@showdesigndata']);
Route::post('/stitchingreturns/store', ['as' => 'pages.stitchingreturns', 'uses' => 'StockController@storestitchingreturns']);

Route::get('/loan', ['as' => 'pages.loan', 'uses' => 'StockController@Advance']);
Route::post('/loan/store', ['as' => 'pages.loan', 'uses' => 'StockController@loanstore']);
Route::post('/loan/paid', ['as' => 'pages.loan', 'uses' => 'StockController@paidloan']);
Route::get('/loan/edit/{id}', ['as' => 'pages.editloan', 'uses' => 'StockController@editloan']);

Route::post('/loan/update/{id}', ['as' => 'pages.editloan', 'uses' => 'StockController@loanupdate']);

Route::get('/payment-history/{partyid}', ['as' => 'payment-history','uses' => 'InvoiceController@paymentHistory']);
Route::get('/invoice-payment-history/{invoiceid}', ['as' => 'invoice-payment-history','uses' => 'InvoiceController@invoicePaymentHistory']);
Route::get('/invoices', ['as' => 'invoice-home','uses' => 'InvoiceController@index']);
Route::get('/invoices/print/{id}', ['as' => 'print-invoice','uses' => 'InvoiceController@print']);
Route::get('/invoices/original_bill/{id}', ['as' => 'original-bill-print','uses' => 'InvoiceController@original_bill_print']);
Route::get('/invoices/transport_bill/{id}', ['as' => 'duplicate-transport-bill','uses' => 'InvoiceController@duplicate_transport_bill']);
Route::get('/invoices/create', ['uses' => 'InvoiceController@create']);
Route::post('/invoices/store', ['uses' => 'InvoiceController@createInvoice']);
Route::get('/invoices/payment', ['as'=> 'invoice-payment','uses' => 'InvoiceController@makePayment']);
Route::post('/invoices/payment/store', ['as'=> 'invoice-payment-store','uses' => 'InvoiceController@invoicePayment']);
Route::get('/invoices/update/{id}', ['uses' => 'InvoiceController@update']);
Route::post('/invoices/update', ['uses' => 'InvoiceController@updateInvoice']);
Route::get('/invoices/delete/{id}', ['uses' => 'InvoiceController@delete']);
Route::get('/stock/details', ['as'=>'stockdetails','uses' => 'InvoiceController@getStockDetails']);
Route::get('/stock/usage', ['as'=>'stockusage','uses' => 'StockController@stockUsage']);
Route::get('/stock/usageAjax', ['as'=>'remaining-stock','uses' => 'StockController@stockUsageAjax']);


Route::get('/party-details/{partyid}', ['as' => 'party-details','uses' => 'PartyController@partydetails']);
Route::get('/employee-details/{empid}', ['as' => 'employee-details','uses' => 'EmployeeController@employeedetails']);

Route::get('/finishing', ['as' => 'finishing','uses' => 'StockController@finishing']);
Route::get('/design_no/{id}', ['as' => 'finishing', 'uses' => 'StockController@design_no']);
Route::get('/stitching_return_data/{stockid}', ['as' => 'finishing', 'uses' => 'StockController@stitching_return_data']);
Route::post('/finishing/store', ['as' => 'finishing', 'uses' => 'StockController@finishing_store']);
Route::get('/finishing-return', ['as' => 'finishing-return','uses' => 'StockController@finishing_return']);
Route::get('/finishing_data/{stockid}', ['as' => 'finishing-return', 'uses' => 'StockController@finishing_data']);
Route::post('/finishing-return/store', ['as' => 'finishing-return', 'uses' => 'StockController@finish_return_store']);

Route::get('/stitchings', ['as' => 'stitchings','uses' => 'StitchingController@index']);
Route::get('/fixedsalary', ['as' => 'fixedsalary','uses' => 'StitchingController@getFixed']);
Route::post('/fixed/store', ['as' => 'fixed_store','uses' => 'StitchingController@fixed_store']);
Route::get('/getpayment/{empid}', ['as' => 'getpayment','uses' => 'StitchingController@getpayment']);


});