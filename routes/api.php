<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/employees', ['uses' => 'ApiEmployeeController@index']);
Route::post('/employees', ['uses' => 'ApiEmployeeController@store']);
Route::post('/stock/store', ['uses' => 'ApiStockController@stock_store']);
Route::get('/party', ['uses' => 'ApiPartyController@index']);
Route::post('/party', ['uses' => 'ApiPartyController@create']);
Route::get('/party/{id}', ['uses' => 'ApiPartyController@edit']);
Route::post('/party/{id}', ['uses' => 'ApiPartyController@update']);
Route::delete('/party/{id}', ['uses' => 'ApiPartyController@delete']);

